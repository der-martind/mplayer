# mplayer

## Screenshot
<div style="position: relative; width: 300px; height: 300px;">
  <img src="docs/images/Screenshot.png" alt="Hintergrundbild" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
  <img src="docs/images/ScreenshotDescription.png" alt="Überlagertes Bild" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; opacity: 0.7;">
</div>


## Features
* Browse your tune directory in a tree view
* Add them to the playlist and play them
* Show lyrics, guitar tabs and additional media for the played tunes
* Record the play charts and visualize them
* Search your tunes by given tags

## STARTUP #############################################################################
* IDE
	Customize the default run configuration with the following cmdline arguments:
		--spring.datasource.username=<dbuser>
		--spring.datasource.password=<dbpassword>
* local
	configure your environment in start-mplayer.bat and execute it
* javafx:run@run -f pom.xml OR javafx:run@debug -f pom.xml 
* Environment variables:
  * JAVA_HOME
  * SPRING_PROFILES_ACTIVE
  * LASTFM_APIKEY  
  * LASTFM_SHARED_SECRET

## Authentication ######################################################################
use participator + private access token (github->avatar->settings->dev settings)

## Known Errors
* org.h2.jdbc.JdbcSQLFeatureNotSupportedException: Dieses Feature wird nicht unterstützt: "Index on column: ... BINARY LARGE OBJECT"
    * -> H2 tries to create indices on blobs which is not supported

## Developer docu ################################################################################

### mvvmfx #############################################################################
mvvmfx is not modularized, so jlink/jpackage won't work

* Interaction btw. Viewmodels and ContextMenu
  ![ViewModels](docs/images/ViewModels.drawio.png)

* EventBus-Subscriptions are done by a special listener, not by the viewmodel.

### Logo
https://patorjk.com/software/taag/#p=display&f=Doom&t=mplayer%202023
-> Advanced download -> Advanced PNG Export -> Background: white

### Icons
https://pictogrammers.com/library/mdi/
-> Advanced Download -> Foreground Color: White -> Download PNG

### Logging
log.info: insert/update
log.warn: no reaction needed -> no event
log.error: dev reaction / user info needed -> NotificationEvent

### Event listeners
Only listeners can subscribe to events:
```
XXXListener::eventbus.register(this)
```

### Migration
1) Invent config-property in application-migration.properties: mplayer.tools.my-migration=on
2) Create Migration CommandLineRunner
   Template:
```
@Component
public class TagMigrator implements CommandLineRunner { }
```
3) Activate migration profile
4) Start mplayer

### Unused warning in annotated classes, methods
<Alt>+<Enter> and select Suppress for methods annotated by ...

### RELEASE #############################################################################
use git bash:
`mvn --batch-mode release:prepare release:perform -Darguments="-Dmaven.javadoc.skip=true"`


package de.cbb.mplayer.session.view;

import de.cbb.mplayer.common.ui.trees.TaggedTreeItem;
import de.cbb.mplayer.tunes.domain.Tune;
import javafx.scene.control.*;
import javafx.scene.layout.Region;
import javafx.util.Callback;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TuneCellFactory implements Callback<ListView<Tune>, ListCell<Tune>> {

    private final ContextMenu contextMenu;
    private final Region parent;

    @Override
    public ListCell<Tune> call(final ListView<Tune> param) {
        return new ListCell<>() {
            {
                prefWidthProperty().bind(parent.widthProperty().subtract(8)); // for scroll-bar
                setMaxWidth(Control.USE_PREF_SIZE);
            }

            @Override
            public void updateItem(final Tune tune, final boolean empty) {
                super.updateItem(tune, empty);
                setText(null);
                if (empty || tune == null) {
                    setGraphic(null);
                    setContextMenu(null);
                } else {
                    setGraphic(new TaggedTreeItem(tune, true));
                    if (contextMenu != null) {
                        setContextMenu(contextMenu);
                    }
                }
            }
        };
    }
}

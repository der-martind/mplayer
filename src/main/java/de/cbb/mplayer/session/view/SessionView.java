package de.cbb.mplayer.session.view;

import de.cbb.mplayer.common.ui.AbstractDefaultViewModel;
import de.cbb.mplayer.common.ui.animation.GaugeTimeline;
import de.cbb.mplayer.common.ui.ctxmenu.DefaultActionMapFactory;
import de.cbb.mplayer.common.ui.ctxmenu.TuneCellContextMenu;
import de.cbb.mplayer.tunes.domain.Tune;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import eu.hansolo.medusa.Gauge;
import eu.hansolo.medusa.GaugeBuilder;
import java.util.List;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Stop;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SessionView implements FxmlView<SessionViewModel> {

    @FXML
    private HBox pnGaugeSession;

    @FXML
    private HBox pnGaugePlayed;

    @FXML
    private ListView<Tune> listPlayed;

    @FXML
    private ListView<Tune> listPending;

    @FXML
    private Label lbPlayed;

    @FXML
    private Button pbClearBowl;

    @InjectViewModel
    private SessionViewModel viewModel;

    private final DefaultActionMapFactory actionMapFactory;

    public void initialize() {
        listPending.setItems(viewModel.getPendingList());
        listPending.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        listPending.setCellFactory(new TuneCellFactory(new PendingTuneContextMenu(viewModel), listPending));
        viewModel.getSelectedPendingItem().bind(listPending.getSelectionModel().selectedItemProperty());

        listPlayed.setItems(viewModel.getPlayedList());
        listPlayed.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        viewModel.getSelectedPlayedItem().bind(listPlayed.getSelectionModel().selectedItemProperty());
        listPlayed.setCellFactory(new TuneCellFactory(
                new TuneCellContextMenu(
                        new AbstractDefaultViewModel() {
                            @Override
                            public List<Tune> getSelectedTunes() {
                                return List.of(viewModel.getSelectedPlayedItem().get());
                            }
                        },
                        actionMapFactory.createActionMap()),
                listPlayed));
        viewModel.getPlayedList().addListener((ListChangeListener<Tune>)
                c -> listPlayed.scrollTo(listPlayed.getItems().size() - 1));

        lbPlayed.textProperty().bind(viewModel.getPlayedRateProperty());
        pbClearBowl.setOnAction(ev -> viewModel.clearBowl());

        final Gauge gaugePlayed = GaugeBuilder.create()
                .skinType(Gauge.SkinType.LEVEL)
                .barBackgroundColor(Color.TRANSPARENT)
                .foregroundBaseColor(Color.WHITE) // text value
                .foregroundPaint(Color.WHITE)
                .gradientBarEnabled(true)
                .gradientBarStops(
                        new Stop(0.0, Color.LIME),
                        new Stop(0.25, Color.YELLOWGREEN),
                        new Stop(0.5, Color.YELLOW),
                        new Stop(0.75, Color.ORANGE),
                        new Stop(1.0, Color.RED))
                .prefSize(70, 70)
                .padding(Insets.EMPTY)
                //                .animated(true) // doesnt work on faster value change events
                .decimals(0)
                .maxValue(100)
                .build();
        gaugePlayed.valueProperty().bind(viewModel.getPlayedPercentageProperty());
        pnGaugePlayed.getChildren().add(gaugePlayed);

        final Gauge gaugeSession = GaugeBuilder.create()
                .skinType(Gauge.SkinType.KPI)
                .foregroundBaseColor(Color.WHITE)
                .needleColor(Color.WHITE)
                .animated(true)
                .threshold(4)
                .maxValue(6)
                .prefSize(80, 80)
                .padding(Insets.EMPTY)
                .build();
        pnGaugeSession.getChildren().add(gaugeSession);
        final GaugeTimeline sessionTimeline = new GaugeTimeline(gaugeSession, 3600);
        sessionTimeline.play();
    }
}

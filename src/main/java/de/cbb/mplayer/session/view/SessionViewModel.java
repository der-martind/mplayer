package de.cbb.mplayer.session.view;

import de.cbb.mplayer.common.ui.PointOnScreen;
import de.cbb.mplayer.detail.view.DetailViewModel;
import de.cbb.mplayer.session.service.MediaBowl;
import de.cbb.mplayer.tunes.domain.Tune;
import de.saxsys.mvvmfx.ViewModel;
import java.util.Iterator;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.IntegerBinding;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class SessionViewModel implements ViewModel {

    @Getter
    private ObservableList<Tune> pendingList;

    @Getter
    private final ObservableList<Tune> playedList = FXCollections.observableArrayList();

    @Getter
    private IntegerBinding pendingSizeProperty;

    @Getter
    private final IntegerBinding playedSizeProperty = Bindings.size(playedList);

    @Getter
    private final IntegerProperty playedPercentageProperty = new SimpleIntegerProperty();

    @Getter
    private final StringProperty playedRateProperty = new SimpleStringProperty();

    @Getter
    private final ObjectProperty<Tune> selectedPendingItem = new SimpleObjectProperty<>();

    @Getter
    private final ObjectProperty<Tune> selectedPlayedItem = new SimpleObjectProperty<>();

    private final MediaBowl mediaBowl;
    private final DetailViewModel detailViewModel;

    public void initialize() {
        pendingList = mediaBowl.getAll();
        pendingSizeProperty = Bindings.size(pendingList);
        playedPercentageProperty.bind(Bindings.createIntegerBinding(
                () -> {
                    final int pendingSize = pendingList.size();
                    final int playedSize = playedList.size();
                    return playedSize == 0 ? 0 : Math.floorDiv((100 * playedSize), (playedSize + pendingSize));
                },
                pendingList,
                playedList));
        playedRateProperty.bind(Bindings.createStringBinding(
                () -> playedList.size() + " of " + (pendingList.size() + playedList.size()), pendingList, playedList));
    }

    public void update(final Tune tune) {
        updateList(playedList, tune);
        updateList(pendingList, tune);
    }

    public void addPlayedTune(final Tune tune) {
        playedList.add(tune);
    }

    public void clearBowl() {
        mediaBowl.clear();
    }

    public void advanceTune() {
        Tune pendingItem = selectedPendingItem.get();
        mediaBowl.getAll().remove(pendingItem);
        mediaBowl.getAll().add(0, pendingItem);
    }

    public void dequeueTune() {
        mediaBowl.remove(selectedPendingItem.get());
    }

    public void showPendingItemEditDialog(final PointOnScreen point) {
        detailViewModel.showDialog(getSelectedPendingItem().get(), point);
    }

    private void updateList(final ObservableList<Tune> list, final Tune tune) {
        for (final Iterator<Tune> it = list.iterator(); it.hasNext(); ) {
            final Tune t = it.next();
            if (t.equals(tune)) {
                final int i = list.indexOf(t);
                it.remove();
                list.set(i, tune);
            }
        }
    }
}

package de.cbb.mplayer.session.view;

import de.cbb.mplayer.common.ui.PointOnScreen;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;

public class PendingTuneContextMenu extends ContextMenu {

    private static final Node IMG_ADVANCE = new ImageView("/img/arrow-up-bold-custom.png");
    private static final Node IMG_EDIT = new ImageView("/img/pencil-outline-custom.png");
    private static final Node IMG_DEQUEUE = new ImageView("/img/close-circle-outline-custom.png");

    public PendingTuneContextMenu(final SessionViewModel viewModel) {
        super();

        final MenuItem miAdvance = new MenuItem("", IMG_ADVANCE);
        miAdvance.setOnAction((final ActionEvent t) -> viewModel.advanceTune());
        getItems().add(miAdvance);

        final MenuItem miEdit = new MenuItem("", IMG_EDIT);
        miEdit.setOnAction((final ActionEvent t) ->
                viewModel.showPendingItemEditDialog(new PointOnScreen(this.getX(), this.getY())));
        getItems().add(miEdit);

        final MenuItem miDequeue = new MenuItem("", IMG_DEQUEUE);
        miDequeue.setOnAction((final ActionEvent t) -> viewModel.dequeueTune());
        getItems().add(miDequeue);
    }
}

package de.cbb.mplayer.session.events;

import de.cbb.mplayer.tunes.domain.Tune;
import java.util.List;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class EnqueueTunesEvent {

    private final List<Tune> tunes;
}

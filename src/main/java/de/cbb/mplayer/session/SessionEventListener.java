package de.cbb.mplayer.session;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import de.cbb.mplayer.common.listeners.DefaultListener;
import de.cbb.mplayer.detail.events.TuneUpdatedEvent;
import de.cbb.mplayer.player.events.PlayTuneEvent;
import de.cbb.mplayer.session.events.EnqueueTunesEvent;
import de.cbb.mplayer.session.service.MediaBowl;
import de.cbb.mplayer.session.view.SessionViewModel;
import org.springframework.stereotype.Component;

@Component
public class SessionEventListener extends DefaultListener {

    private final MediaBowl mediaBowl;
    private final SessionViewModel viewModel;

    public SessionEventListener(
            EventBus eventBus,
            DefaultErrorHandler defaultErrorHandler,
            final MediaBowl mediaBowl,
            final SessionViewModel viewModel) {
        super(eventBus, defaultErrorHandler);
        this.mediaBowl = mediaBowl;
        this.viewModel = viewModel;
    }

    @Subscribe
    public void handleEnqueueTunesEvent(final EnqueueTunesEvent event) {
        try {
            event.getTunes().forEach(mediaBowl::enqueue);
        } catch (final Exception ex) {
            defaultErrorHandler.handle("handleEnqueueTunesEvent", ex);
        }
    }

    @Subscribe
    public void handleTuneUpdatedEvent(final TuneUpdatedEvent event) {
        try {
            viewModel.update(event.getTune());
        } catch (final Exception ex) {
            defaultErrorHandler.handle("handleTuneUpdatedEvent", ex);
        }
    }

    @Subscribe
    public void handlePlayTuneEvent(final PlayTuneEvent event) {
        try {
            viewModel.addPlayedTune(event.getTune());
        } catch (final Exception ex) {
            defaultErrorHandler.handle("handlePlayTuneEvent", ex);
        }
    }
}

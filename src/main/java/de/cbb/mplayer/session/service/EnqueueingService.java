package de.cbb.mplayer.session.service;

import com.google.common.eventbus.EventBus;
import de.cbb.mplayer.session.events.EnqueueTunesEvent;
import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tunes.domain.Tune;
import java.util.List;
import java.util.function.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EnqueueingService {

    private final EventBus eventBus;

    public void enqueue(final List<Tune> tunes) {
        final List<Tune> filteredList =
                tunes.stream().filter(Predicate.not(this::hasBlockedTag)).toList();
        eventBus.post(new EnqueueTunesEvent(filteredList));
    }

    private boolean hasBlockedTag(final Tune tune) {
        return tune.getTags().stream().anyMatch(Tag::isEnqueueBlocked);
    }
}

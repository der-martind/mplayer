package de.cbb.mplayer.session.service;

import de.cbb.mplayer.tunes.domain.Tune;
import java.util.Random;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MediaBowl {

    private final ObservableList<Tune> playQueue = FXCollections.observableArrayList();
    private final Random random;

    public MediaBowl() {
        random = new Random(System.currentTimeMillis());
    }

    public void enqueue(final Tune media) {
        log.debug("Tune enqueued: {}", media.toString());
        playQueue.add(media);
    }

    public Tune getNextRandom() {
        if (playQueue.isEmpty()) {
            return null;
        }
        final int index = random.nextInt(playQueue.size());
        final Tune m = playQueue.get(index);
        removeFromQueue(m);
        return m;
    }

    public Tune getNextStraight() {
        if (playQueue.isEmpty()) {
            return null;
        }
        final Tune m = playQueue.get(0);
        removeFromQueue(m);
        return m;
    }

    public ObservableList<Tune> getAll() {
        return playQueue;
    }

    public void remove(final Tune m) {
        if (!playQueue.contains(m)) {
            return;
        }
        removeFromQueue(m);
    }

    public void clear() {
        playQueue.clear();
    }

    private void removeFromQueue(final Tune m) {
        if (!playQueue.contains(m)) {
            return;
        }
        playQueue.remove(m);
    }
}

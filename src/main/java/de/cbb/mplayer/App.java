package de.cbb.mplayer;

import de.cbb.mplayer.common.ui.UIConstants;
import de.cbb.mplayer.main.view.MainView;
import de.cbb.mplayer.screen.service.ScreenService;
import de.cbb.mplayer.shutdown.ShutdownHandler;
import de.cbb.mplayer.userprefs.domain.UserPrefs;
import de.cbb.mplayer.userprefs.service.UserPrefsService;
import de.saxsys.mvvmfx.FluentViewLoader;
import de.saxsys.mvvmfx.spring.MvvmfxSpringApplication;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class App extends MvvmfxSpringApplication {

    private static final String IMG_LOGO = "/img/logo20.png";

    public static void main(final String[] args) {
        Application.launch(args);
    }

    @Value("${app.name}")
    private String appName;

    @Value("${app.version}")
    private String appVersion;

    @Autowired
    private ScreenService screenService;

    @Autowired
    private UserPrefsService userPrefsService;

    @Override
    public void startMvvmfx(final Stage stage) {
        final Parent root = FluentViewLoader.fxmlView(MainView.class).load().getView();

        final Scene scene = new Scene(root);
        scene.getStylesheets().add(UIConstants.STYLESHEET);
        stage.setScene(scene);
        stage.getIcons().add(new Image(IMG_LOGO));
        stage.setTitle(appName + " v" + appVersion);
        stage.setOnCloseRequest(new ShutdownHandler(screenService, userPrefsService));
        positionStage(stage);
        screenService.setScene(scene);
        stage.show();
    }

    private void positionStage(final Stage stage) {
        final UserPrefs userPrefs = userPrefsService.get();
        if (userPrefs.getXOnScreen() != null && userPrefs.getYOnScreen() != null) {
            stage.setX(userPrefs.getXOnScreen());
            stage.setY(userPrefs.getYOnScreen());
        }
    }
}

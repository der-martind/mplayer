package de.cbb.mplayer.detail.events;

import de.cbb.mplayer.perspectives.events.AbstractTuneEvent;
import de.cbb.mplayer.tunes.domain.Tune;

public class TuneUpdatedEvent extends AbstractTuneEvent {
    public TuneUpdatedEvent(final Tune tune) {
        super(tune);
    }
}

package de.cbb.mplayer.detail.view;

import com.google.common.eventbus.EventBus;
import de.cbb.mplayer.common.Constants;
import de.cbb.mplayer.common.ui.PointOnScreen;
import de.cbb.mplayer.common.ui.dialogs.FxmlInfoDialog;
import de.cbb.mplayer.detail.events.TuneUpdatedEvent;
import de.cbb.mplayer.detail.view.mapping.TunePropertyAdapter;
import de.cbb.mplayer.discogs.domain.Result;
import de.cbb.mplayer.discogs.domain.Root;
import de.cbb.mplayer.discogs.service.DiscogsService;
import de.cbb.mplayer.ranking.domain.Ranking;
import de.cbb.mplayer.ranking.service.RankingService;
import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tags.service.TagService;
import de.cbb.mplayer.tunes.domain.Tune;
import de.cbb.mplayer.tunes.service.TuneService;
import de.saxsys.mvvmfx.ViewModel;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class DetailViewModel implements ViewModel {

    private final EventBus eventBus;
    private final TuneService tuneService;

    @Getter
    private final TagService tagService;

    private final DiscogsService discogsService;
    private final RankingService rankingService;

    private FxmlInfoDialog dlg;

    @Getter
    private final ObjectProperty<Tune> selectedTuneProperty = new SimpleObjectProperty<>();

    @Getter
    private final TunePropertyAdapter tunePropertyAdapter = new TunePropertyAdapter();

    public void saveTune() {
        log.debug("saveTune");
        tunePropertyAdapter.map2Entity(selectedTuneProperty.get());
        final Tune persistedTune = tuneService.save(selectedTuneProperty.get());
        selectedTuneProperty.set(persistedTune);
        eventBus.post(new TuneUpdatedEvent(persistedTune));
        closeDialog();
    }

    public List<Result> getDiscogs() {
        final Root r = discogsService.getReleases(
                tunePropertyAdapter.getArtistProperty().get(),
                tunePropertyAdapter.getAlbumProperty().get());
        return r.getResults();
    }

    public Ranking getRanking() {
        return rankingService.getRanking(
                tunePropertyAdapter.getArtistProperty().get(),
                tunePropertyAdapter.getAlbumProperty().get(),
                tunePropertyAdapter.getTitleProperty().get());
    }

    public void showDialog(final Tune tune, final PointOnScreen point) {
        selectedTuneProperty.set(tune);
        tunePropertyAdapter.map2Adapter(tune);
        createDialog(point);
        dlg.show();
    }

    public void fillReleaseYear() {
        Integer releaseYear;
        if (tunePropertyAdapter.getAlbumProperty().get().equalsIgnoreCase(Constants.DEFAULT_ALBUM))
            releaseYear = discogsService.getTrackReleaseYear(
                    tunePropertyAdapter.getArtistProperty().get(),
                    tunePropertyAdapter.getTitleProperty().get());
        else
            releaseYear = discogsService.getReleaseYear(
                    tunePropertyAdapter.getArtistProperty().get(),
                    tunePropertyAdapter.getAlbumProperty().get());
        if (releaseYear > 0 && !releaseYear.equals(Constants.DEFAULT_YEAR))
            tunePropertyAdapter.getYearProperty().set(releaseYear);
    }

    public void updateDecateTag() {
        int year = tunePropertyAdapter.getYearProperty().get();
        if (year > 0 && year != Constants.DEFAULT_YEAR) {
            Tag tag = tagService.findDecadeTag(year);
            if (tag != null) tunePropertyAdapter.getTags().add(tag);
        }
    }

    private void createDialog(final PointOnScreen point) {
        if (dlg != null) {
            return;
        }
        dlg = new FxmlInfoDialog("Edit", point, DetailView.class);
        Scene scene = dlg.getDialogPane().getScene();
        scene.getWindow().setOnCloseRequest(event -> closeDialog());
        scene.addEventHandler(KeyEvent.KEY_PRESSED, t -> {
            if (t.getCode() == KeyCode.ESCAPE) {
                dlg.close();
            }
        });
    }

    private void closeDialog() {
        dlg.getDialogPane().getScene().getWindow().hide();
    }
}

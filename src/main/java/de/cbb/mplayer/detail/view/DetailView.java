package de.cbb.mplayer.detail.view;

import de.cbb.mplayer.common.converters.DurationConverter;
import de.cbb.mplayer.common.ui.PointOnScreen;
import de.cbb.mplayer.discogs.view.DiscogsDialog;
import de.cbb.mplayer.ranking.view.RankingDialog;
import de.cbb.mplayer.tags.view.TagSelectionDialog;
import de.cbb.mplayer.tags.view.chips.ChipsPane;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.beans.binding.Bindings;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.util.converter.NumberStringConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DetailView implements FxmlView<DetailViewModel> {

    @FXML
    private TextField tfArtist;

    @FXML
    private TextField tfAlbum;

    @FXML
    private TextField tfTitle;

    @FXML
    private TextField tfPath;

    @FXML
    private TextField tfYear;

    @FXML
    private TextField tfFilename2;

    @FXML
    private HBox ctnTags;

    @FXML
    private TextField tfAdded;

    @FXML
    private TextField tfDuration;

    @FXML
    private TextField tfLyrics;

    @FXML
    private TextField tfTabs;

    @FXML
    private TextField tfMedia;

    @FXML
    private Button pbShowDiscogs;

    @FXML
    private Button pbRanking;

    @FXML
    private Button pbSave;

    @FXML
    private Button pbReleaseYear;

    @InjectViewModel
    private DetailViewModel viewModel;

    private final TagSelectionDialog tagSelectionDialog;

    public void initialize() {
        bindTextField(tfArtist, viewModel.getTunePropertyAdapter().getArtistProperty());
        bindTextField(tfAlbum, viewModel.getTunePropertyAdapter().getAlbumProperty());
        bindTextField(tfTitle, viewModel.getTunePropertyAdapter().getTitleProperty());
        Bindings.bindBidirectional(
                tfYear.textProperty(),
                viewModel.getTunePropertyAdapter().getYearProperty(),
                new NumberStringConverter("0000"));
        tfYear.disableProperty().bind(viewModel.getSelectedTuneProperty().isNull());
        bindTextField(tfPath, viewModel.getTunePropertyAdapter().getPathProperty());
        bindTextField(tfFilename2, viewModel.getTunePropertyAdapter().getFilename2Property());
        bindTextField(tfAdded, viewModel.getTunePropertyAdapter().getAddedProperty());
        Bindings.bindBidirectional(
                tfDuration.textProperty(),
                viewModel.getTunePropertyAdapter().getDurationProperty(),
                new DurationConverter());
        bindTextField(tfLyrics, viewModel.getTunePropertyAdapter().getLyricsProperty());
        bindTextField(tfTabs, viewModel.getTunePropertyAdapter().getTabsProperty());
        bindTextField(tfMedia, viewModel.getTunePropertyAdapter().getMediaProperty());
        pbShowDiscogs.setOnAction(event -> showDiscogsDialog());
        pbRanking.setOnAction(event -> showRankingDialog());
        pbSave.setOnAction(event -> viewModel.saveTune());
        pbReleaseYear.setOnAction(event -> fillReleaseYear());
        final ChipsPane pnTags = new ChipsPane(
                viewModel.getTagService(), viewModel.getTunePropertyAdapter().getTags(), tagSelectionDialog);
        pnTags.disableProperty().bind(viewModel.getSelectedTuneProperty().isNull());
        ctnTags.getChildren().add(pnTags);
        tfYear.focusedProperty().addListener((observableValue, aBoolean, t1) -> {
            if (!t1) { // focus lost
                viewModel.updateDecateTag();
            }
        });
    }

    private void fillReleaseYear() {
        pbReleaseYear.getScene().setCursor(Cursor.WAIT);
        viewModel.fillReleaseYear();
        pbReleaseYear.getScene().setCursor(Cursor.DEFAULT);
        tfYear.requestFocus();
    }

    private void bindTextField(final TextField tf, final StringProperty property) {
        tf.textProperty().bindBidirectional(property);
        tf.disableProperty().bind(viewModel.getSelectedTuneProperty().isNull());
    }

    private void showDiscogsDialog() {
        DiscogsDialog.display(viewModel.getDiscogs(), getPointOnScreen(pbShowDiscogs));
    }

    private void showRankingDialog() {
        RankingDialog.display(viewModel.getRanking(), getPointOnScreen(pbRanking));
    }

    private PointOnScreen getPointOnScreen(Button pb) {
        return new PointOnScreen(
                pb.getScene().getWindow().getX() + (pb.getScene().getWindow().getWidth() / 3),
                pb.getScene().getWindow().getY() + (pb.getScene().getWindow().getHeight() / 3));
    }
}

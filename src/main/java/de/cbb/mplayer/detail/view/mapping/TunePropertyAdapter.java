package de.cbb.mplayer.detail.view.mapping;

import de.cbb.mplayer.common.Constants;
import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tunes.domain.Tune;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;
import lombok.Getter;

@Getter
public class TunePropertyAdapter {

    private final StringProperty artistProperty = new SimpleStringProperty();
    private final StringProperty albumProperty = new SimpleStringProperty();
    private final StringProperty titleProperty = new SimpleStringProperty();

    @Getter
    private final IntegerProperty yearProperty = new SimpleIntegerProperty();

    private final StringProperty pathProperty = new SimpleStringProperty();
    private final StringProperty filename2Property = new SimpleStringProperty();
    private final StringProperty addedProperty = new SimpleStringProperty();
    private final LongProperty durationProperty = new SimpleLongProperty();
    private final StringProperty lyricsProperty = new SimpleStringProperty();
    private final StringProperty tabsProperty = new SimpleStringProperty();
    private final StringProperty mediaProperty = new SimpleStringProperty();
    private final ObservableSet<Tag> tags = FXCollections.observableSet();

    public void map2Adapter(final Tune tune) {
        artistProperty.set(tune.getArtist());
        albumProperty.set(tune.getAlbum());
        titleProperty.set(tune.getTitle());
        yearProperty.set(tune.getYear2());
        pathProperty.set(tune.getPath());
        filename2Property.set(tune.getFilename2());
        addedProperty.set(Constants.DATE_FORMAT.format(tune.getAdded()));
        if (tune.getDuration() != null) { // when importing
            durationProperty.set(tune.getDuration());
        } else {
            durationProperty.set(0);
        }
        lyricsProperty.set(tune.getLyricsUrl());
        tabsProperty.set(tune.getTabsUrl());
        mediaProperty.set(tune.getMediaUrl());
        tags.clear();
        tags.addAll(tune.getTags());
    }

    public void map2Entity(final Tune tune) {
        tune.setArtist(artistProperty.get());
        tune.setAlbum(albumProperty.get());
        tune.setTitle(titleProperty.get());
        tune.setYear2(yearProperty.get());
        tune.setPath(pathProperty.get());
        tune.setFilename2(filename2Property.get());
        tune.setDuration(durationProperty.get());
        tune.setLyricsUrl(lyricsProperty.get());
        tune.setTabsUrl(tabsProperty.get());
        tune.setMediaUrl(mediaProperty.get());
        tune.setTags(getTags());
    }
}

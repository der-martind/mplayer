package de.cbb.mplayer.screen.service;

import de.cbb.mplayer.screen.domain.Coords;
import javafx.scene.Scene;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ScreenService {

    @Setter
    private Scene scene;

    public Coords getCoordsOnScreen() {
        return new Coords(
                (int) scene.getWindow().getX(), (int) scene.getWindow().getY());
    }
}

package de.cbb.mplayer.screen.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Coords {

    private int x;
    private int y;
}

package de.cbb.mplayer.discogs.config;

import de.cbb.mplayer.discogs.client.DiscogsClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SuppressWarnings("unused")
@Configuration
public class DiscogsConfig {

    @Value("${discogs.user-agent}")
    private String userAgent;

    @Value("${discogs.consumer-key}")
    private String consumerKey;

    @Value("${discogs.consumer-secret}")
    private String consumerSecret;

    @Value("${discogs.oauth-token}")
    private String oauthToken;

    @Value("${discogs.oauth-token-secret}")
    private String oauthTokenSecret;

    @Bean
    public DiscogsClient discogsClient() {
        return new DiscogsClient(consumerKey, consumerSecret, userAgent, oauthToken, oauthTokenSecret);
    }
}

package de.cbb.mplayer.discogs.domain;

import lombok.Data;

@Data
public class Community {
    public int want;
    public int have;
}

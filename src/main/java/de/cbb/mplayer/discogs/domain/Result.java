package de.cbb.mplayer.discogs.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import lombok.Data;

@Data
public class Result {
    public String country;
    public ArrayList<String> genre;
    public ArrayList<String> format;
    public ArrayList<String> style;
    public int id;
    public ArrayList<String> label;
    public String type;
    public ArrayList<String> barcode;
    public UserData user_data;
    public int master_id;
    public String master_url;
    public String uri;
    public String catno;
    public String title;
    public String thumb;
    public String cover_image;
    public String resource_url;
    public Community community;
    public String year;
    public String format_quantity;

    @JsonIgnore
    public ArrayList<Object> formats;
}

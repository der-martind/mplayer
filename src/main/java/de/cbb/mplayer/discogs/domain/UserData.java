package de.cbb.mplayer.discogs.domain;

import lombok.Data;

@Data
public class UserData {
    public boolean in_wantlist;
    public boolean in_collection;
}

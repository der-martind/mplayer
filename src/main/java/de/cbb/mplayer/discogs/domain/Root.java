package de.cbb.mplayer.discogs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;
import lombok.Data;

@Data
@JsonIgnoreProperties(value = {"pagination"})
public class Root {
    public ArrayList<Result> results;
}

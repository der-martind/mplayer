package de.cbb.mplayer.discogs.view;

import de.cbb.mplayer.common.ui.PointOnScreen;
import de.cbb.mplayer.common.ui.UIConstants;
import de.cbb.mplayer.discogs.domain.Result;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class DiscogsDialog {

    private static final String TITLE = "Discogs";

    public static void display(final List<Result> results, final PointOnScreen point) {

        final Stage window = initWindow(point);

        final ListView<Result> listView = new ListView<>(FXCollections.observableArrayList(results));
        listView.setId("text-selection-list");
        listView.setCellFactory(new ReleaseCellFactory());

        final Scene scene = new Scene(listView);
        scene.getStylesheets().add(UIConstants.STYLESHEET);
        scene.addEventHandler(KeyEvent.KEY_PRESSED, t -> {
            if (t.getCode() == KeyCode.ESCAPE) {
                window.close();
            }
        });
        window.setScene(scene);
        window.showAndWait();
    }

    private static Stage initWindow(final PointOnScreen point) {
        final Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(TITLE);
        window.setWidth(450);
        window.setHeight(200);
        window.initStyle(StageStyle.UTILITY);
        window.setX(point.getX());
        window.setY(point.getY());
        return window;
    }
}

package de.cbb.mplayer.discogs.view;

import de.cbb.mplayer.common.ui.controls.Chip;
import de.cbb.mplayer.common.ui.controls.FlagImageView;
import de.cbb.mplayer.discogs.domain.Result;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.util.Callback;

public class ReleaseCellFactory implements Callback<ListView<Result>, ListCell<Result>> {

    private static final String COLOR_YEAR = "#0000AA";

    @Override
    public ListCell<Result> call(final ListView<Result> param) {
        return new ListCell<>() {
            @Override
            public void updateItem(final Result result, final boolean empty) {
                super.updateItem(result, empty);
                if (empty) {
                    setText(null);
                    setGraphic(null);
                } else if (result != null) {
                    setText(null);
                    final HBox hb = new HBox();
                    hb.setAlignment(Pos.CENTER_LEFT);
                    hb.getChildren().add(new Chip(result.getYear(), COLOR_YEAR));
                    hb.getChildren().add(new Label(result.getTitle()));
                    hb.getChildren().add(new Label(" [" + String.join(",", result.getStyle()) + "]"));
                    hb.getChildren().add(new FlagImageView(result.getCountry()));
                    setGraphic(hb);
                } else {
                    setText("null");
                    setGraphic(null);
                }
            }
        };
    }
}

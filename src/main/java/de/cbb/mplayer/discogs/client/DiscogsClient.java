package de.cbb.mplayer.discogs.client;

import de.cbb.mplayer.discogs.util.HttpRequest;
import java.util.HashMap;
import java.util.Map;

public class DiscogsClient {

    // Authorization
    public static final String URL_REQUEST_TOKEN = "https://api.discogs.com/oauth/request_token";
    public static final String URL_AUTHORIZE = "https://discogs.com/oauth/authorize";
    public static final String URL_ACCESS_TOKEN = "https://api.discogs.com/oauth/access_token";

    // Database
    public static final String URL_RELEASE = "https://api.discogs.com/releases/{release_id}";
    public static final String URL_MASTER_RELEASE = "https://api.discogs.com/masters/{master_id}";
    public static final String URL_MASTER_RELEASE_VERSIONS = "https://api.discogs.com/masters/{master_id}/versions";
    public static final String URL_ARTIST = "https://api.discogs.com/artists/{artist_id}";
    public static final String URL_ARTIST_RELEASES = "https://api.discogs.com/artists/{artist_id}/releases";
    public static final String URL_LABEL = "https://api.discogs.com/labels/{label_id}";
    public static final String URL_LABEL_RELEASES = "https://api.discogs.com/labels/{label_id}/releases";
    public static final String URL_SEARCH = "https://api.discogs.com/database/search?q={query}";

    // User Identity
    public static final String URL_USER_IDENTITY = "https://api.discogs.com/oauth/identity";
    public static final String URL_USER_PROFILE = "https://api.discogs.com/users/{username}";

    // User Collection
    public static final String URL_COLLECTION = "https://api.discogs.com/users/{username}/collection/folders";
    public static final String URL_COLLECTION_FOLDER =
            "https://api.discogs.com/users/{username}/collection/folders/{folder_id}";
    public static final String URL_COLLECTION_RELEASES =
            "https://api.discogs.com/users/{username}/collection/folders/{folder_id}/releases";
    public static final String URL_ADD_RELEASE_TO_FOLDER =
            "https://api.discogs.com/users/{username}/collection/folders/{folder_id}/releases/{release_id}";
    public static final String URL_MODIFY_INSTANCE_IN_FOLDER =
            "https://api.discogs.com/users/{username}/collection/folders/{folder_id}/releases/{release_id}/instances/{instance_id}";

    // User Wantlist
    public static final String URL_WANTLIST = "https://api.discogs.com/users/{username}/wants";
    public static final String URL_MODIFY_WANTLIST_WITH_RELEASE =
            "https://api.discogs.com/users/{username}/wants/{release_id}";

    // Marketplace
    public static final String URL_INVENTORY = "https://api.discogs.com/users/{username}/inventory";
    public static final String URL_LISTING = "https://api.discogs.com/marketplace/listings/{listing_id}";

    private static final String OAUTH_CONSUMER_KEY = "oauth_consumer_key";
    private static final String OAUTH_NONCE = "oauth_nonce";
    private static final String OAUTH_SIGNATURE = "oauth_signature";
    private static final String OAUTH_SIGNATURE_METHOD = "oauth_signature_method";
    private static final String OAUTH_SIGNATURE_METHOD_VALUE = "PLAINTEXT";
    private static final String OAUTH_TIMESTAMP = "oauth_timestamp";
    private static final String OAUTH_ACCESS_TOKEN = "oauth_token";
    private static final String OAUTH_CALLBACK = "oauth_callback";
    private static final String OAUTH_VERIFIER = "oauth_verifier";
    private static final String HEADER_AUTHORIZATION = "Authorization";

    private String consumerKey = "";
    private String consumerSecret = "";
    private String userAgent;
    private String callbackUrl = "";

    private String requestToken = "";
    private String requestTokenSecret = "";
    private String accessVerifier = "";

    private String oauthToken = "";
    private String oauthTokenSecret = "";

    public DiscogsClient(
            final String consumer_key,
            final String consumer_secret,
            final String user_agent,
            final String callback_url) {
        consumerKey = consumer_key;
        consumerSecret = consumer_secret;
        userAgent = user_agent;
        callbackUrl = callback_url;
    }

    public DiscogsClient(
            final String consumer_key,
            final String consumer_secret,
            final String user_agent,
            final String oauth_token,
            final String oauth_token_secret) {
        consumerKey = consumer_key;
        consumerSecret = consumer_secret;
        userAgent = user_agent;
        oauthToken = oauth_token;
        oauthTokenSecret = oauth_token_secret;
    }

    public DiscogsClient(final String user_agent) {
        userAgent = user_agent;
    }

    public String genericGet(final String URL) {

        final HttpRequest request =
                HttpRequest.get(URL).authorization(authenticatedHeader()).userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    public String genericPost(final String URL, final Map<String, String> params) {
        final HttpRequest request = HttpRequest.post(URL, true)
                .authorization(authenticatedHeader())
                .userAgent(userAgent)
                .contentType(HttpRequest.CONTENT_TYPE_JSON)
                .send(mapToJson(params));
        System.out.println(request.toString());

        return request.body();
    }

    public String genericDelete(final String URL) {
        final HttpRequest request = HttpRequest.delete(URL, true)
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        if (request.noContent()) {
            System.out.println(Integer.toString(request.code()));
            return Integer.toString(request.code()) + " No Content";
        }

        return request.body();
    }

    /**
     * ---------------------------------------------- method: GET URL :
     * https://api.discogs.com/oauth/identity params: none
     */
    public String identity() {

        final HttpRequest request = HttpRequest.get(URL_USER_IDENTITY)
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    /**
     * ---------------------------------------------- method: GET URL :
     * https://api.discogs.com/users/{username} params: username
     */
    public String profile(final String username) {

        final Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        final HttpRequest request = HttpRequest.get(replaceURLParams(URL_USER_PROFILE, params))
                .header(HEADER_AUTHORIZATION, authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    public String updateProfile(final String username, final Map<String, String> extraParams) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        final HttpRequest request = HttpRequest.post(replaceURLParams(URL_USER_PROFILE, params), true)
                .authorization(authenticatedHeader())
                .userAgent(userAgent)
                .contentType(HttpRequest.CONTENT_TYPE_JSON)
                .send(mapToJson(extraParams));
        System.out.println(request.toString());

        return request.body();
    }

    /**
     * ---------------------------------------------- method: GET URL :
     * https://api.discogs.com/database/search?q={query} params: query
     */
    public String search(final String query) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("query", query);
        final HttpRequest request = HttpRequest.get(replaceURLParams(URL_SEARCH, params), true)
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    public String advancedSearch(final String query, final Map<String, String> extraParams) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("query", query);
        final HttpRequest request = HttpRequest.get(replaceURLParams(URL_SEARCH, params), extraParams, true)
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    /**
     * ---------------------------------------------- method: GET URL :
     * https://api.discogs.com/releases/{release_id} params: release_id
     */
    public String release(final String release_id) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("release_id", release_id);
        final HttpRequest request = HttpRequest.get(replaceURLParams(URL_RELEASE, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    /**
     * ---------------------------------------------- method: GET URL :
     * https://api.discogs.com/masters/{master_id} params: master_id
     */
    public String masterRelease(final String master_id) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("master_id", master_id);
        final HttpRequest request = HttpRequest.get(replaceURLParams(URL_MASTER_RELEASE, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    /**
     * ---------------------------------------------- method: GET URL :
     * https://api.discogs.com/masters/{master_id}/versions params: master_id
     */
    public String masterReleaseVersions(final String master_id, final Map<String, String> extraParams) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("master_id", master_id);
        final HttpRequest request = HttpRequest.get(
                        replaceURLParams(URL_MASTER_RELEASE_VERSIONS, params), extraParams, true)
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    public String masterReleaseVersions(final String master_id) {
        return masterReleaseVersions(master_id, null);
    }

    /**
     * ---------------------------------------------- method: GET URL :
     * https://api.discogs.com/artists/{artist_id} params: artist_id
     */
    public String artist(final String artist_id) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("artist_id", artist_id);
        final HttpRequest request = HttpRequest.get(replaceURLParams(URL_ARTIST, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    /**
     * ---------------------------------------------- method: GET URL :
     * https://api.discogs.com/artists/{artist_id}/releases params: artist_id
     */
    public String artistReleases(final String artist_id, final Map<String, String> extraParams) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("artist_id", artist_id);
        final HttpRequest request = HttpRequest.get(replaceURLParams(URL_ARTIST_RELEASES, params), extraParams, true)
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    public String artistReleases(final String artist_id) {
        return artistReleases(artist_id, null);
    }

    /**
     * ---------------------------------------------- method: GET URL :
     * https://api.discogs.com/labels/{label_id} params: label_id
     */
    public String label(final String label_id) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("label_id", label_id);
        final HttpRequest request = HttpRequest.get(replaceURLParams(URL_LABEL, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    /**
     * ---------------------------------------------- method: GET URL :
     * https://api.discogs.com/labels/{label_id}/releases params: label_id
     */
    public String labelReleases(final String label_id, final Map<String, String> extraParams) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("label_id", label_id);
        final HttpRequest request = HttpRequest.get(replaceURLParams(URL_LABEL_RELEASES, params), extraParams, true)
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    public String labelReleases(final String label_id) {
        return labelReleases(label_id, null);
    }

    /**
     * ---------------------------------------------- method: GET URL :
     * https://api.discogs.com/users/{username}/collection/folders params: username
     */
    public String collection(final String username) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        final HttpRequest request = HttpRequest.get(replaceURLParams(URL_COLLECTION, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    public String addCollectionFolder(final String username, final String folderName) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        final Map<String, String> extraParams = new HashMap<String, String>();
        extraParams.put("name", folderName);
        final HttpRequest request = HttpRequest.post(replaceURLParams(URL_COLLECTION, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent)
                .contentType(HttpRequest.CONTENT_TYPE_JSON)
                .send(mapToJson(extraParams));
        System.out.println(request.toString());

        return request.body();
    }

    /**
     * ---------------------------------------------- method: GET URL :
     * https://api.discogs.com/users/{username}/collection/folders/{folder_id} params: username,
     * folder_id
     */
    public String collectionFolder(final String username, final String folder_id) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("folder_id", folder_id);
        final HttpRequest request = HttpRequest.get(replaceURLParams(URL_COLLECTION_FOLDER, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    public String updateCollectionFolder(
            final String username, final String folder_id, final Map<String, String> extraParams) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("folder_id", folder_id);
        final HttpRequest request = HttpRequest.post(replaceURLParams(URL_COLLECTION_FOLDER, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent)
                .contentType(HttpRequest.CONTENT_TYPE_JSON)
                .send(mapToJson(extraParams));

        return request.body();
    }

    /**
     * ---------------------------------------------- method: DELETE URL :
     * https://api.discogs.com/users/{username}/collection/folders/{folder_id} params: username,
     * folder_id
     */
    public String deleteCollectionFolder(final String username, final String folder_id) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("folder_id", folder_id);
        final HttpRequest request = HttpRequest.delete(replaceURLParams(URL_COLLECTION_FOLDER, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        if (request.noContent()) {
            System.out.println(Integer.toString(request.code()));
            return Integer.toString(request.code()) + " No Content";
        }

        return request.body();
    }

    /**
     * ---------------------------------------------- method: GET URL :
     * https://api.discogs.com/users/{username}/collection/folders/{folder_id}/releases params:
     * username, folder_id
     */
    public String collectionReleases(
            final String username, final String folder_id, final Map<String, String> extraParams) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("folder_id", folder_id);
        final HttpRequest request = HttpRequest.get(
                        replaceURLParams(URL_COLLECTION_RELEASES, params), extraParams, true)
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    public String collectionReleases(final String username, final String folder_id) {
        return collectionReleases(username, folder_id, null);
    }

    /**
     * ---------------------------------------------- method: POST URL :
     * https://api.discogs.com/users/{username}/collection/folders/{folder_id}/releases/{release_id}
     * params: username, folder_id, release_id NOTE : use folder_id = 1 for uncategorized
     */
    public String addReleaseToFolder(final String username, final String folder_id, final String release_id) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("folder_id", folder_id);
        params.put("release_id", release_id);
        final HttpRequest request = HttpRequest.post(replaceURLParams(URL_ADD_RELEASE_TO_FOLDER, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent)
                .send("");
        System.out.println(request.toString());
        System.out.println(request.code());

        return request.body();
    }

    /**
     * ---------------------------------------------- method: POST URL :
     * https://api.discogs.com/users/{username}/collection/folders/{folder_id}/releases/{release_id}/instances/{instance_id}
     * params: username, folder_id, release_id, instance_id
     */
    public String updateInstanceInFolder(
            final String username,
            final String folder_id,
            final String release_id,
            final String instance_id,
            final Map<String, String> extraParams) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("folder_id", folder_id);
        params.put("release_id", release_id);
        params.put("instance_id", instance_id);
        final HttpRequest request = HttpRequest.post(replaceURLParams(URL_MODIFY_INSTANCE_IN_FOLDER, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent)
                .contentType(HttpRequest.CONTENT_TYPE_JSON)
                .send(mapToJson(extraParams));
        System.out.println(request.toString());

        if (request.noContent()) {
            System.out.println(Integer.toString(request.code()));
            return Integer.toString(request.code()) + " No Content";
        }

        return request.body();
    }

    /**
     * ---------------------------------------------- method: DELETE URL :
     * https://api.discogs.com/users/{username}/collection/folders/{folder_id}/releases/{release_id}/instances/{instance_id}
     * params: username, folder_id, release_id, instance_id
     */
    public String deleteInstanceFromFolder(
            final String username, final String folder_id, final String release_id, final String instance_id) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("folder_id", folder_id);
        params.put("release_id", release_id);
        params.put("instance_id", instance_id);

        final HttpRequest request = HttpRequest.delete(replaceURLParams(URL_MODIFY_INSTANCE_IN_FOLDER, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent);

        if (request.noContent()) {
            System.out.println(Integer.toString(request.code()));
            return Integer.toString(request.code()) + " No Content";
        }

        return request.body();
    }

    /**
     * ---------------------------------------------- method: GET URL :
     * https://api.discogs.com/users/{username}/wants params: username
     */
    public String wantlist(final String username, final Map<String, String> extraParams) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        final HttpRequest request = HttpRequest.get(replaceURLParams(URL_WANTLIST, params), extraParams, true)
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    public String wantlist(final String username) {
        return wantlist(username, null);
    }

    /**
     * ---------------------------------------------- method: PUT URL :
     * https://api.discogs.com/users/{username}/wants/{release_id} params: username, release_id
     */
    public String addToWantlist(final String username, final String release_id) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("release_id", release_id);
        final HttpRequest request = HttpRequest.put(replaceURLParams(URL_MODIFY_WANTLIST_WITH_RELEASE, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());
        System.out.println(request.code());

        return request.body();
    }

    /**
     * ---------------------------------------------- method: DELETE URL :
     * https://api.discogs.com/users/{username}/wants/{release_id} params: username, release_id
     */
    public String deleteFromWantlist(final String username, final String release_id) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("release_id", release_id);
        final HttpRequest request = HttpRequest.delete(replaceURLParams(URL_MODIFY_WANTLIST_WITH_RELEASE, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());
        if (request.noContent()) {
            System.out.println(Integer.toString(request.code()));
            return Integer.toString(request.code()) + " No Content";
        }

        return request.body();
    }

    public String updateInWantlist(
            final String username, final String release_id, final Map<String, String> extraParams) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("release_id", release_id);
        final HttpRequest request = HttpRequest.post(replaceURLParams(URL_MODIFY_WANTLIST_WITH_RELEASE, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent)
                .contentType(HttpRequest.CONTENT_TYPE_JSON)
                .send(mapToJson(extraParams));
        System.out.println(request.toString());

        if (request.noContent()) {
            System.out.println(Integer.toString(request.code()));
            return Integer.toString(request.code()) + " No Content";
        }

        return request.body();
    }

    /**
     * ---------------------------------------------- method: GET URL :
     * https://api.discogs.com/users/{username}/inventory params: username
     */
    public String inventory(final String username) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        final HttpRequest request = HttpRequest.get(replaceURLParams(URL_INVENTORY, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    /**
     * ---------------------------------------------- method: GET URL :
     * https://api.discogs.com/marketplace/listings/{listing_id} params: listing_id
     */
    public String listing(final String listing_id) {
        final Map<String, String> params = new HashMap<String, String>();
        params.put("listing_id", listing_id);
        final HttpRequest request = HttpRequest.get(replaceURLParams(URL_LISTING, params))
                .authorization(authenticatedHeader())
                .userAgent(userAgent);
        System.out.println(request.toString());

        return request.body();
    }

    public String replaceURLParams(final String start, final Map<String, String> keysAndValues) {
        String endString = start;

        for (final String key : keysAndValues.keySet()) {
            endString = endString.replace("{" + key + "}", keysAndValues.get(key));
        }

        return endString;
    }

    public String mapToJson(final Map<String, String> map) {
        String mapAsJson = "";
        int index = 0;

        if (map == null || map.isEmpty()) {
            return "";
        }

        mapAsJson += "{";
        for (final String key : map.keySet()) {
            mapAsJson += "\"" + key + "\"" + ":" + "\"" + map.get(key) + "\"";
            index++;
            if (index < map.size()) {
                mapAsJson += ",";
            }
        }
        mapAsJson += "}";

        return mapAsJson;
    }

    public Map<String, String> parseParams(final String responseString) {
        final Map<String, String> responseMap = new HashMap<String, String>();
        System.out.println(responseString);
        final String[] keysAndValues = responseString.split("&");

        for (int i = 0; i < keysAndValues.length; i++) {
            final String[] keyAndValue = keysAndValues[i].split("=");

            responseMap.put(keyAndValue[0], keyAndValue[1]);
        }
        return responseMap;
    }

    public Map<String, String> optionalParamsToMap(final String... params) {
        final Map<String, String> map = new HashMap<String, String>();
        String key = "";
        for (int i = 0; i < params.length; i++) {
            if (i % 2 == 0) {
                key = params[i];
            } else {
                map.put(key, params[i]);
            }
        }
        return map;
    }

    public void getRequestToken() {

        final HttpRequest request = HttpRequest.get(HttpRequest.append(URL_REQUEST_TOKEN))
                .userAgent(userAgent)
                .authorization(requestAuthorizationHeader());

        System.out.println(request.toString());
        System.out.println(request.code());
        final Map<String, String> r = parseParams(request.body());
        final String token = r.get("oauth_token");
        final String token_secret = r.get("oauth_token_secret");
        System.out.println(token);
        System.out.println(token_secret);

        requestToken = token;
        requestTokenSecret = token_secret;
    }

    public String getAuthorizationURL() {

        return HttpRequest.append(URL_AUTHORIZE, "oauth_token", requestToken);
    }

    public void getAccessToken(final String verifier) {
        accessVerifier = verifier;
        final HttpRequest request = HttpRequest.post(URL_ACCESS_TOKEN)
                .userAgent(userAgent)
                .authorization(accessAuthorizationHeader())
                .send("");
        System.out.println(request.toString());
        System.out.println(request.code());
        final Map<String, String> r = parseParams(request.body());
        final String token = r.get("oauth_token");
        final String token_secret = r.get("oauth_token_secret");
        System.out.println(token);
        System.out.println(token_secret);

        oauthToken = token;
        oauthTokenSecret = token_secret;
    }

    public String authenticatedHeader() {
        final java.util.Date date = new java.util.Date();

        final String authorization = "OAuth "
                + OAUTH_CONSUMER_KEY
                + "=\""
                + consumerKey
                + "\", "
                + OAUTH_NONCE
                + "=\""
                + String.valueOf(date.getTime())
                + "\", "
                + OAUTH_SIGNATURE
                + "=\""
                + consumerSecret
                + "&"
                + oauthTokenSecret
                + "\", "
                + OAUTH_SIGNATURE_METHOD
                + "=\""
                + OAUTH_SIGNATURE_METHOD_VALUE
                + "\", "
                + OAUTH_TIMESTAMP
                + "=\""
                + String.valueOf(date.getTime())
                + "\", "
                + OAUTH_ACCESS_TOKEN
                + "=\""
                + oauthToken
                + "\"";

        return authorization;
    }

    public String accessAuthorizationHeader() {
        final java.util.Date date = new java.util.Date();

        final String authorization = "OAuth "
                + OAUTH_CONSUMER_KEY
                + "=\""
                + consumerKey
                + "\", "
                + OAUTH_NONCE
                + "=\""
                + String.valueOf(date.getTime())
                + "\", "
                + OAUTH_SIGNATURE
                + "=\""
                + consumerSecret
                + "&"
                + requestTokenSecret
                + "\", "
                + OAUTH_SIGNATURE_METHOD
                + "=\""
                + OAUTH_SIGNATURE_METHOD_VALUE
                + "\", "
                + OAUTH_TIMESTAMP
                + "=\""
                + String.valueOf(date.getTime())
                + "\", "
                + OAUTH_VERIFIER
                + "=\""
                + accessVerifier
                + "\", "
                + OAUTH_ACCESS_TOKEN
                + "=\""
                + requestToken
                + "\"";

        return authorization;
    }

    public String requestAuthorizationHeader() {
        final java.util.Date date = new java.util.Date();

        final String authorization = "OAuth "
                + OAUTH_CONSUMER_KEY
                + "=\""
                + consumerKey
                + "\", "
                + OAUTH_NONCE
                + "=\""
                + String.valueOf(date.getTime())
                + "\", "
                + OAUTH_SIGNATURE
                + "=\""
                + consumerSecret
                + "&"
                + "\", "
                + OAUTH_SIGNATURE_METHOD
                + "=\""
                + OAUTH_SIGNATURE_METHOD_VALUE
                + "\", "
                + OAUTH_TIMESTAMP
                + "=\""
                + String.valueOf(date.getTime())
                + "\", "
                + OAUTH_CALLBACK
                + "=\""
                + callbackUrl
                + "\"";

        return authorization;
    }

    public String getConsumerKey() {
        return consumerKey;
    }

    public void setConsumerKey(final String consumerKey) {
        this.consumerKey = consumerKey;
    }

    public String getConsumerSecret() {
        return consumerSecret;
    }

    public void setConsumerSecret(final String consumerSecret) {
        this.consumerSecret = consumerSecret;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(final String userAgent) {
        this.userAgent = userAgent;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(final String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public void setRequestToken(final String requestToken) {
        this.requestToken = requestToken;
    }

    public String getRequestTokenSecret() {
        return requestTokenSecret;
    }

    public void setRequestTokenSecret(final String requestTokenSecret) {
        this.requestTokenSecret = requestTokenSecret;
    }

    public String getAccessVerifier() {
        return accessVerifier;
    }

    public void setAccessVerifier(final String accessVerifier) {
        this.accessVerifier = accessVerifier;
    }

    public String getOauthToken() {
        return oauthToken;
    }

    public void setOauthToken(final String oauthToken) {
        this.oauthToken = oauthToken;
    }

    public String getOauthTokenSecret() {
        return oauthTokenSecret;
    }

    public void setOauthTokenSecret(final String oauthTokenSecret) {
        this.oauthTokenSecret = oauthTokenSecret;
    }
}

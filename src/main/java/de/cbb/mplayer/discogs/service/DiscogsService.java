package de.cbb.mplayer.discogs.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import de.cbb.mplayer.discogs.client.DiscogsClient;
import de.cbb.mplayer.discogs.domain.Result;
import de.cbb.mplayer.discogs.domain.Root;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class DiscogsService {

    private static final String RELEASE_TYPE = "master";

    private final DiscogsClient client;

    @Cacheable("discogs-releases")
    public Root getReleases(final String artist, final String album) {
        final Map<String, String> map = new HashMap<>();
        map.put("artist", artist);
        map.put("release_title", album);
        map.put("type", RELEASE_TYPE);
        final String result2 = client.advancedSearch("", map);
        log.debug(result2);
        final ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(result2, Root.class);
        } catch (final JsonProcessingException e) {
            log.warn("Mapping Discogs result failed: {}", e.toString());
        }
        return null;
    }

    public String getAlbumCover(final String artist, final String album, final int year) {
        final Root root = getReleases(artist, album);
        if (root == null) {
            return null;
        }
        final List<String> covers =
                root.getResults().stream().map(Result::getCover_image).toList();
        if (covers.isEmpty()) {
            return null;
        }
        if (covers.size() > 1) {
            final Optional<String> cover = root.getResults().stream()
                    .filter(result -> !Strings.isNullOrEmpty(result.getYear()))
                    .filter(result -> year == Integer.parseInt(result.getYear()))
                    .map(Result::getCover_image)
                    .findFirst();
            if (cover.isPresent()) {
                return cover.get();
            }
        }
        return covers.get(0);
    }

    @Cacheable("discogs-releaseyears")
    public Integer getReleaseYear(final String artist, final String releaseName) {
        return getReleaseYear(artist, "release_title", releaseName);
    }

    public Integer getTrackReleaseYear(final String artist, final String trackName) {
        return getReleaseYear(artist, "track", trackName);
    }

    public Integer getReleaseYear(final String artist, final String key, final String value) {
        final Map<String, String> map = new HashMap<>();
        map.put("artist", artist);
        map.put(key, value);
        map.put("type", RELEASE_TYPE);
        final String result2 = client.advancedSearch("", map);
        log.debug(result2);
        final ObjectMapper mapper = new ObjectMapper();
        Root root;
        try {
            root = mapper.readValue(result2, Root.class);
        } catch (final JsonProcessingException e) {
            log.warn("Mapping Discogs result failed: {}", e.toString());
            return 0;
        }
        int oldest = Integer.MAX_VALUE;
        for (Result r : root.getResults()) {
            if (!Strings.isNullOrEmpty(r.getYear())) {
                int y = Integer.parseInt(r.getYear());
                if (y < oldest) oldest = y;
            }
        }
        if (oldest == Integer.MAX_VALUE) oldest = 0;
        log.debug("Release-Year: " + oldest);
        return oldest;
    }
}

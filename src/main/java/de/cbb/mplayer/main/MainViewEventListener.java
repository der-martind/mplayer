package de.cbb.mplayer.main;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import de.cbb.mplayer.common.listeners.DefaultListener;
import de.cbb.mplayer.main.events.NotificationEvent;
import de.cbb.mplayer.main.view.MainViewModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MainViewEventListener extends DefaultListener {

    private final MainViewModel mainViewModel;

    public MainViewEventListener(
            final EventBus eventBus, final DefaultErrorHandler defaultErrorHandler, final MainViewModel mainViewModel) {
        super(eventBus, defaultErrorHandler);
        this.mainViewModel = mainViewModel;
    }

    @Subscribe
    public void handleNotificationEvent(final NotificationEvent ev) {
        try {
            mainViewModel.setMessage(ev.getMessage());
        } catch (final Exception ex) {
            log.error("handleNotificationEvent failed", ex);
        }
    }
}

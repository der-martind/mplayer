package de.cbb.mplayer.main.events;

public class NotificationEvent {

    private final String message;

    public String getMessage() {
        return message;
    }

    public NotificationEvent(final String message) {
        this.message = message;
    }
}

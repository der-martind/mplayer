package de.cbb.mplayer.main;

import de.cbb.mplayer.charts.view.ChartViewModel;
import de.cbb.mplayer.perspectives.artists.view.ArtistViewModel;
import de.cbb.mplayer.perspectives.files.view.FileViewModel;
import de.cbb.mplayer.reports.view.ReportViewModel;
import de.saxsys.mvvmfx.ViewModel;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ViewModelMap {

    private final ArtistViewModel artistViewModel;
    private final FileViewModel fileViewModel;
    private final ChartViewModel chartViewModel;
    private final ReportViewModel reportViewModel;
    private final Map<String, ViewModel> map = new HashMap<>();

    @PostConstruct
    public void init() {
        map.put("Artists", artistViewModel);
        map.put("Files", fileViewModel);
        map.put("Charts", chartViewModel);
        map.put("Reports", reportViewModel);
    }

    public ViewModel getViewModel(final String name) {
        return map.get(name);
    }
}

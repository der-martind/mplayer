package de.cbb.mplayer.main.view;

import com.google.common.eventbus.EventBus;
import de.cbb.mplayer.common.ui.PositioningHelper;
import de.cbb.mplayer.common.ui.trees.AbstractTreeViewModel;
import de.cbb.mplayer.detail.view.DetailViewModel;
import de.cbb.mplayer.main.ViewModelMap;
import de.cbb.mplayer.tunes.domain.Tune;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import de.saxsys.mvvmfx.ViewModel;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Popup;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MainView implements FxmlView<MainViewModel> {

    @FXML
    private Button pbImportFolder;

    @FXML
    private Button pbImportFile;

    @FXML
    private Button pbAddTag;

    @FXML
    private TextField tfSearch;

    @FXML
    private Label message;

    @FXML
    private Button pbClearMessages;

    @FXML
    private TabPane pnPerspective;

    @FXML
    private Button pbClearSearch;

    @InjectViewModel
    private MainViewModel viewModel;

    private final ViewModelMap viewModelMap;
    private final DetailViewModel detailViewModel;
    private final EventBus eventBus;

    private Popup searchResultsPopup;

    public void initialize() {
        pbImportFolder.setOnAction(ev -> viewModel.onImportFolder(pnPerspective));
        pbImportFile.setOnAction(ev -> viewModel.onImportFile(pnPerspective));
        pbAddTag.setOnAction(ev ->
                viewModel.showAddTagDialog(PositioningHelper.getPointOnParent(pnPerspective.getParent(), 30, 100)));
        message.textProperty().bind(viewModel.getMessageProperty());
        viewModel.getSearchTextProperty().bind(tfSearch.textProperty());

        viewModel.getSearchResults().addListener((ListChangeListener<Tune>) c -> {
            if (viewModel.getSearchResults().isEmpty()) {
                searchResultsPopup.hide();
            } else {
                showSearchResults();
            }
        });

        pbClearMessages.setOnAction(ev -> viewModel.setMessage(""));
        pbClearSearch.setOnAction(ev -> tfSearch.setText(""));

        final SingleSelectionModel<Tab> selectionModel = pnPerspective.getSelectionModel();
        selectionModel.select(0);

        pnPerspective.getSelectionModel().selectedItemProperty().addListener((ov, oldTab, newTab) -> {
            ViewModel myViewModel = viewModelMap.getViewModel(newTab.getText());
            if (myViewModel instanceof AbstractTreeViewModel myAbstractViewModel) {
                myAbstractViewModel.onTabSelected();
            }
        });
    }

    private void showSearchResults() {
        searchResultsPopup = new SearchResultPopup(
                viewModel.getSearchResults(),
                PositioningHelper.getPointBelowParent(tfSearch),
                detailViewModel,
                eventBus);
        searchResultsPopup.show(tfSearch.getScene().getWindow());
    }
}

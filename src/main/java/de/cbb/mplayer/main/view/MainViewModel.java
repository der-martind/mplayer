package de.cbb.mplayer.main.view;

import com.google.common.base.Strings;
import de.cbb.mplayer.common.ui.PointOnScreen;
import de.cbb.mplayer.common.ui.PositioningHelper;
import de.cbb.mplayer.config.ConfigProperties;
import de.cbb.mplayer.importing.view.ImportingViewModel;
import de.cbb.mplayer.tags.view.adding.AddingTagViewModel;
import de.cbb.mplayer.tunes.domain.Tune;
import de.cbb.mplayer.tunes.service.TuneService;
import de.cbb.mplayer.userprefs.service.UserPrefsService;
import de.saxsys.mvvmfx.ViewModel;
import java.io.File;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class MainViewModel implements ViewModel {

    public static final int MIN_SEARCHTEXT = 3;
    public static final int MAX_SEARCHRESULTS = 15;

    @Getter
    private final StringProperty messageProperty = new SimpleStringProperty("");

    @Getter
    private final StringProperty searchTextProperty = new SimpleStringProperty("");

    @Getter
    private final ObservableList<Tune> searchResults = FXCollections.observableArrayList();

    private final ConfigProperties properties;
    private final ImportingViewModel importingViewModel;
    private final AddingTagViewModel addingTagViewModel;
    private final TuneService tuneService;
    private final UserPrefsService userPrefsService;

    public void initialize() {
        searchTextProperty.addListener((observable, oldValue, newValue) -> {
            if (newValue != null && newValue.length() >= MIN_SEARCHTEXT) {
                updateSearchResultItems();
            } else {
                searchResults.clear();
            }
        });
    }

    public void onImportFile(final Node parent) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser
                .getExtensionFilters()
                .addAll(
                        new FileChooser.ExtensionFilter(
                                "Audio Files", properties.getImporting().getSupportedSuffices()),
                        new FileChooser.ExtensionFilter("All Files", "*.*"));
        fileChooser.setInitialDirectory(getInitialFileImportFolder());
        final File file = fileChooser.showOpenDialog(parent.getScene().getWindow());
        if (file == null) {
            return;
        }
        if (!file.getAbsolutePath().startsWith(properties.getMediaRoot())) {
            log.warn("Files outside media root {} can't be imported", properties.getMediaRoot());
            messageProperty.set("Files outside media root " + properties.getMediaRoot() + " can't be imported");
            return;
        }
        updateUserPrefs(file);
        showImportDialog(file, PositioningHelper.getPointOnParent(parent, 15, 45));
    }

    public void onImportFolder(final Node parent) {
        final DirectoryChooser fileChooser = new DirectoryChooser();
        fileChooser.setInitialDirectory(new File(properties.getMediaRoot()));
        final File dir = fileChooser.showDialog(parent.getScene().getWindow());
        if (dir == null) {
            return;
        }
        showImportDialog(dir, PositioningHelper.getPointOnParent(parent, 15, 45));
    }

    public void setMessage(final String message) {
        messageProperty.set(message);
    }

    public void showAddTagDialog(final PointOnScreen point) {
        addingTagViewModel.showAddTagDialog(point);
    }

    private File getInitialFileImportFolder() {
        final String latestFileImportFolder = userPrefsService.get().getLatestFileImportFolder();
        if (!Strings.isNullOrEmpty(latestFileImportFolder)) {
            final File folder = new File(latestFileImportFolder);
            if (folder.isDirectory()) {
                return folder;
            }
        }
        return new File(properties.getMediaRoot());
    }

    private void updateUserPrefs(final File file) {
        userPrefsService.updateLatestFileImportFolder(file.getParent());
    }

    private void showImportDialog(final File file, final PointOnScreen point) {
        importingViewModel.showImportDialog(file, point);
    }

    private void updateSearchResultItems() {
        searchResults.clear();
        searchResults.addAll(tuneService.getByContains(searchTextProperty.get(), MAX_SEARCHRESULTS));
    }
}

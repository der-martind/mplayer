package de.cbb.mplayer.main.view;

import com.google.common.eventbus.EventBus;
import de.cbb.mplayer.common.ui.PointOnScreen;
import de.cbb.mplayer.common.ui.PositioningHelper;
import de.cbb.mplayer.detail.view.DetailViewModel;
import de.cbb.mplayer.session.events.EnqueueTunesEvent;
import de.cbb.mplayer.tunes.domain.Tune;
import java.util.List;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.stage.Popup;
import javafx.util.Callback;

public class SearchResultPopup extends Popup {

    public SearchResultPopup(
            final ObservableList<Tune> searchResults,
            final PointOnScreen point,
            final DetailViewModel detailViewModel,
            final EventBus eventBus) {
        super();
        setAnchorX(point.getX());
        setAnchorY(point.getY() + 2);
        ListView<Tune> listview = new ListView<>(searchResults);
        listview.setPrefHeight(searchResults.size() * 24);
        listview.setPrefWidth(266);
        listview.setOnMouseClicked(mouseEvent -> {
            // double-click
            if (mouseEvent.getClickCount() == 2) {
                Tune tune = listview.getSelectionModel().getSelectedItem();
                hide();
                eventBus.post(new EnqueueTunesEvent(List.of(tune)));
            }
        });

        listview.setCellFactory(new Callback<>() {
            @Override
            public ListCell<Tune> call(ListView<Tune> listView) {
                return new ListCell<>() {
                    @Override
                    protected void updateItem(Tune item, boolean empty) {
                        super.updateItem(item, empty);

                        if (empty) {
                            setText(null);
                        } else {
                            setText(item != null ? item.toString() : "No value");
                            ContextMenu contextMenu = createSearchResultContextMenu(item, detailViewModel, listview);
                            setContextMenu(contextMenu);
                        }
                    }
                };
            }
        });

        getContent().add(listview);
        setHideOnEscape(true);
        setAutoHide(true);
    }

    private ContextMenu createSearchResultContextMenu(Tune item, final DetailViewModel detailViewModel, Node listview) {
        ContextMenu contextMenu = new ContextMenu();

        MenuItem miEnqueue = new MenuItem("Edit");
        miEnqueue.setOnAction(event -> {
            detailViewModel.showDialog(item, PositioningHelper.getPointBesidesParent(listview));
            hide();
        });
        contextMenu.getItems().addAll(miEnqueue);
        return contextMenu;
    }
}

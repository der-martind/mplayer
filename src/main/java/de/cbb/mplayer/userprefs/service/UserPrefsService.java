package de.cbb.mplayer.userprefs.service;

import de.cbb.mplayer.screen.domain.Coords;
import de.cbb.mplayer.userprefs.domain.UserPrefs;
import de.cbb.mplayer.userprefs.repo.UserPrefsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserPrefsService {

    private final UserPrefsRepository userPrefsRepository;

    public UserPrefs get() {
        final UserPrefs userPrefs = userPrefsRepository.findFirstByOrderById();
        return userPrefs == null ? new UserPrefs() : userPrefs;
    }

    public void updateCoordsOnScreen(final Coords coords) {
        final UserPrefs userPrefs = initUserPrefs();
        userPrefs.setXOnScreen(coords.getX());
        userPrefs.setYOnScreen(coords.getY());
        save(userPrefs);
    }

    public void updateLatestFileImportFolder(final String folder) {
        final UserPrefs userPrefs = initUserPrefs();
        userPrefs.setLatestFileImportFolder(folder);
        save(userPrefs);
    }

    private UserPrefs initUserPrefs() {
        UserPrefs userPrefs = userPrefsRepository.findFirstByOrderById();
        if (userPrefs == null) {
            userPrefs = new UserPrefs();
        }
        return userPrefs;
    }

    private void save(final UserPrefs userPrefs) {
        userPrefsRepository.save(userPrefs);
        log.debug("UserPrefs saved: " + userPrefs);
    }
}

package de.cbb.mplayer.userprefs.repo;

import de.cbb.mplayer.userprefs.domain.UserPrefs;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPrefsRepository extends JpaRepository<UserPrefs, Integer> {

    UserPrefs findFirstByOrderById();
}

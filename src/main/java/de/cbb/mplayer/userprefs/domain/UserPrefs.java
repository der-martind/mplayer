package de.cbb.mplayer.userprefs.domain;

import javax.persistence.*;
import lombok.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class UserPrefs {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer xOnScreen;
    private Integer yOnScreen;
    private String latestFileImportFolder;
}

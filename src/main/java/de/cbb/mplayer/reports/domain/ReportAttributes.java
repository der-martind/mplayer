package de.cbb.mplayer.reports.domain;

import lombok.Getter;

public enum ReportAttributes {
    ARTIST("Artist"),
    ALBUM("Album"),
    TITLE("Title"),
    GENRES("Genres");

    @Getter
    private final String name;

    ReportAttributes(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

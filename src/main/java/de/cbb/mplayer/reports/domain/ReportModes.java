package de.cbb.mplayer.reports.domain;

import lombok.Getter;

public enum ReportModes {
    TOP10("Top10"),
    TOP_PER_MONTH("Top/Month");

    @Getter
    private final String name;

    ReportModes(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

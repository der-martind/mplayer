package de.cbb.mplayer.reports.service;

import java.time.LocalDate;
import java.util.List;
import javafx.scene.chart.XYChart;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class ChartReportBuilder {

    private final ReportService reportService;

    public XYChart.Series<String, Long> createTop10GenresChart(final LocalDate periodFrom, final LocalDate periodTo) {
        final XYChart.Series<String, Long> series = new XYChart.Series<>();
        final List<Object[]> list = reportService.getTopGenres(periodFrom, periodTo, 10);
        for (final Object[] o : list) {
            series.getData().add(new XYChart.Data(o[0], o[1]));
        }
        return series;
    }

    public XYChart.Series<String, Long> createTop10AlbumsChart(final LocalDate periodFrom, final LocalDate periodTo) {
        final XYChart.Series<String, Long> series = new XYChart.Series<>();
        final List<Object[]> list = reportService.getTopAlbums(periodFrom, periodTo, 10);
        for (final Object[] o : list) {
            series.getData().add(new XYChart.Data(o[0], o[1]));
        }
        return series;
    }

    public XYChart.Series<String, Long> createTop10ArtistsChart(final LocalDate periodFrom, final LocalDate periodTo) {
        final XYChart.Series<String, Long> series = new XYChart.Series<>();
        final List<Object[]> list = reportService.getTopArtists(periodFrom, periodTo, 10);
        for (final Object[] o : list) {
            series.getData().add(new XYChart.Data(o[0], o[1]));
        }
        return series;
    }

    public XYChart.Series<String, Long> createTop10TitlesChart(final LocalDate periodFrom, final LocalDate periodTo) {
        final XYChart.Series<String, Long> series = new XYChart.Series<>();
        final List<Object[]> list = reportService.getTopTitles(periodFrom, periodTo, 10);
        for (final Object[] o : list) {
            series.getData().add(new XYChart.Data(o[0], o[1]));
        }
        return series;
    }

    public XYChart.Series<String, Long> createTopPerMonthTitlesChart(final LocalDate periodFrom) {
        final XYChart.Series<String, Long> series = new XYChart.Series<>();
        for (int iMonth = 0; iMonth < 10; iMonth++) {
            final LocalDate from = periodFrom.plusMonths(iMonth);
            final LocalDate to = periodFrom.plusMonths((iMonth + 1));
            final List<Object[]> list = reportService.getTopTitles(from, to, 1);
            if (list.isEmpty()) {
                break;
            }
            log.debug("Top of month " + from.getMonth().toString() + " = " + list.get(0)[1]);
            series.getData().add(0, new XYChart.Data(list.get(0)[0], list.get(0)[1]));
        }
        return series;
    }

    public XYChart.Series<String, Long> createTopPerMonthArtistsChart(final LocalDate periodFrom) {
        final XYChart.Series<String, Long> series = new XYChart.Series<>();
        for (int iMonth = 0; iMonth < 10; iMonth++) {
            final LocalDate from = periodFrom.plusMonths(iMonth);
            final LocalDate to = periodFrom.plusMonths((iMonth + 1));
            final List<Object[]> list = reportService.getTopArtists(from, to, 1);
            if (list.isEmpty()) {
                break;
            }
            log.debug("Top of month " + from.getMonth().toString() + " = " + list.get(0)[1]);
            series.getData().add(0, new XYChart.Data(list.get(0)[0], list.get(0)[1]));
        }
        return series;
    }

    public XYChart.Series<String, Long> createTopPerMonthAlbumsChart(final LocalDate periodFrom) {
        final XYChart.Series<String, Long> series = new XYChart.Series<>();
        for (int iMonth = 0; iMonth < 10; iMonth++) {
            final LocalDate from = periodFrom.plusMonths(iMonth);
            final LocalDate to = periodFrom.plusMonths((iMonth + 1));
            final List<Object[]> list = reportService.getTopAlbums(from, to, 1);
            if (list.isEmpty()) {
                break;
            }
            log.debug("Top of month " + from.getMonth().toString() + " = " + list.get(0)[1]);
            series.getData().add(0, new XYChart.Data(list.get(0)[0], list.get(0)[1]));
        }
        return series;
    }

    public XYChart.Series<String, Long> createTopPerMonthGenresChart(final LocalDate periodFrom) {
        final XYChart.Series<String, Long> series = new XYChart.Series<>();
        for (int iMonth = 0; iMonth < 10; iMonth++) {
            final LocalDate from = periodFrom.plusMonths(iMonth);
            final LocalDate to = periodFrom.plusMonths((iMonth + 1));
            final List<Object[]> list = reportService.getTopGenres(from, to, 1);
            if (list.isEmpty()) {
                break;
            }
            log.debug("Top of month " + from.getMonth().toString() + " = " + list.get(0)[1]);
            series.getData().add(0, new XYChart.Data(list.get(0)[0], list.get(0)[1]));
        }
        return series;
    }
}

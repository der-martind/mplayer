package de.cbb.mplayer.reports.service;

import de.cbb.mplayer.charts.repo.ChartRepository;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ReportService {

    private final ChartRepository chartRepo;

    public List<Object[]> getTopTitles(final LocalDate from, final LocalDate to, final int limit) {
        final List<Object[]> list = chartRepo.findTopTitles(from, to, Pageable.ofSize(limit));
        Collections.reverse(list);
        return list;
    }

    public List<Object[]> getTopArtists(final LocalDate from, final LocalDate to, final int limit) {
        final List<Object[]> list = chartRepo.findTopArtists(from, to, Pageable.ofSize(limit));
        Collections.reverse(list);
        return list;
    }

    public List<Object[]> getTopAlbums(final LocalDate from, final LocalDate to, final int limit) {
        final List<Object[]> list = chartRepo.findTopAlbums(from, to, Pageable.ofSize(limit));
        Collections.reverse(list);
        return list;
    }

    public List<Object[]> getTopGenres(final LocalDate from, final LocalDate to, final int limit) {
        final List<Object[]> list = chartRepo.findTopGenres(from, to, limit);
        Collections.reverse(list);
        return list;
    }
}

package de.cbb.mplayer.reports.view;

import de.cbb.mplayer.reports.domain.ReportAttributes;
import de.cbb.mplayer.reports.domain.ReportModes;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ReportView implements FxmlView<ReportViewModel> {

    @FXML
    private ComboBox<ReportAttributes> cbAttribute;

    @FXML
    private DatePicker tfPeriodFrom;

    @FXML
    private DatePicker tfPeriodTo;

    @FXML
    private ChoiceBox<ReportModes> cbMode;
    //    @FXML TODO
    //    private ComboBox<String> cbFilters;
    @FXML
    private BarChart<String, Long> barChart;

    @InjectViewModel
    private ReportViewModel viewModel;

    public void initialize() {
        cbAttribute.getItems().addAll(viewModel.getAttributes());
        cbMode.getItems().addAll(viewModel.getModes());
        //        cbFilters.getItems().addAll(viewModel.getFilters());

        cbAttribute.valueProperty().bindBidirectional(viewModel.getAttributeProperty());
        tfPeriodFrom.valueProperty().bindBidirectional(viewModel.getPeriodFromProperty());
        tfPeriodTo.valueProperty().bindBidirectional(viewModel.getPeriodToProperty());
        cbMode.valueProperty().bindBidirectional(viewModel.getModeProperty());
        //        cbFilters.valueProperty().bindBidirectional(viewModel.getFilterProperty());

        barChart.getYAxis()
                .labelProperty()
                .bind(viewModel.getAttributeProperty().asString());

        viewModel.getSeriesProperty().addListener(observable -> {
            if (viewModel.getSeriesProperty().get().getData().size() > 0) {
                barChart.setData(FXCollections.observableArrayList(
                        viewModel.getSeriesProperty().get()));
                for (final XYChart.Data<String, Long> data :
                        viewModel.getSeriesProperty().get().getData()) {
                    displayInsideDataLabel(
                            data, ((XYChart.Data<?, ?>) data).getYValue().toString());
                }
            }
        });
    }

    private void displayInsideDataLabel(final XYChart.Data<String, Long> data, final String label) {
        final Node node = data.getNode();
        final Text dataText = new Text(label);
        dataText.getStyleClass().add("report-label");
        final Group parent = (Group) node.getParent();
        parent.getChildren().add(dataText);

        node.boundsInParentProperty().addListener((ov, oldBounds, bounds) -> {
            dataText.setLayoutX(10);
            dataText.setLayoutY(bounds.getMaxY() - 3);
        });
    }
}

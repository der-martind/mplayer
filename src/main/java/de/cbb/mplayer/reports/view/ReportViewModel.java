package de.cbb.mplayer.reports.view;

import de.cbb.mplayer.reports.domain.ReportAttributes;
import de.cbb.mplayer.reports.domain.ReportModes;
import de.cbb.mplayer.reports.service.ChartReportBuilder;
import de.saxsys.mvvmfx.ViewModel;
import java.time.LocalDate;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class ReportViewModel implements ViewModel {

    @Getter
    private final ObjectProperty<ReportAttributes> attributeProperty = new SimpleObjectProperty<>();

    @Getter
    private final ObjectProperty<LocalDate> periodFromProperty =
            new SimpleObjectProperty<>(LocalDate.of(LocalDate.now().getYear(), 1, 1));

    @Getter
    private final ObjectProperty<LocalDate> periodToProperty = new SimpleObjectProperty<>(LocalDate.now());

    @Getter
    private final ObjectProperty<ReportModes> modeProperty = new SimpleObjectProperty<>(ReportModes.TOP10);

    @Getter
    private final StringProperty filterProperty = new SimpleStringProperty();

    @Getter
    private final ObservableList<ReportAttributes> attributes =
            FXCollections.observableArrayList(ReportAttributes.values());

    @Getter
    private final ObservableList<ReportModes> modes = FXCollections.observableArrayList(ReportModes.values());

    @Getter
    private final ObservableList<String> filters =
            FXCollections.observableArrayList("EXHIBITIONS", "Genre:rock", "Genre:alternative"); // TODO

    @Getter
    private final ObjectProperty<XYChart.Series<String, Long>> seriesProperty = new SimpleObjectProperty<>();

    private final ChartReportBuilder chartReportBuilder;

    public void initialize() {
        attributeProperty.addListener((observable, oldValue, newValue) -> updateReport());
        periodFromProperty.addListener((observable, oldValue, newValue) -> updateReport());
        periodToProperty.addListener((observable, oldValue, newValue) -> updateReport());
        modeProperty.addListener((observable, oldValue, newValue) -> updateReport());
        filterProperty.addListener((observable, oldValue, newValue) -> updateReport());
    }

    private void updateReport() {
        final XYChart.Series<String, Long> series = getChartSeries();
        seriesProperty.set(series);
    }

    private XYChart.Series<String, Long> getChartSeries() {
        if (periodFromProperty.get() == null || periodToProperty.get() == null || attributeProperty.get() == null) {
            return null;
        }
        if (attributeProperty.get().equals(ReportAttributes.TITLE)) {
            if (modeProperty.get().equals(ReportModes.TOP10)) {
                return chartReportBuilder.createTop10TitlesChart(periodFromProperty.get(), periodToProperty.get());
            } else if (modeProperty.get().equals(ReportModes.TOP_PER_MONTH)) {
                return chartReportBuilder.createTopPerMonthTitlesChart(periodFromProperty.get());
            }
        }
        if (attributeProperty.get().equals(ReportAttributes.ARTIST)) {
            if (modeProperty.get().equals(ReportModes.TOP10)) {
                return chartReportBuilder.createTop10ArtistsChart(periodFromProperty.get(), periodToProperty.get());
            } else if (modeProperty.get().equals(ReportModes.TOP_PER_MONTH)) {
                return chartReportBuilder.createTopPerMonthArtistsChart(periodFromProperty.get());
            }
        }
        if (attributeProperty.get().equals(ReportAttributes.ALBUM)) {
            if (modeProperty.get().equals(ReportModes.TOP10)) {
                return chartReportBuilder.createTop10AlbumsChart(periodFromProperty.get(), periodToProperty.get());
            } else if (modeProperty.get().equals(ReportModes.TOP_PER_MONTH)) {
                return chartReportBuilder.createTopPerMonthAlbumsChart(periodFromProperty.get());
            }
        }
        if (attributeProperty.get().equals(ReportAttributes.GENRES)) {
            if (modeProperty.get().equals(ReportModes.TOP10)) {
                return chartReportBuilder.createTop10GenresChart(periodFromProperty.get(), periodToProperty.get());
            } else if (modeProperty.get().equals(ReportModes.TOP_PER_MONTH)) {
                return chartReportBuilder.createTopPerMonthGenresChart(periodFromProperty.get());
            }
        }
        return null;
    }
}

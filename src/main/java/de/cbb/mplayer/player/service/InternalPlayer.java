package de.cbb.mplayer.player.service;

import com.google.common.eventbus.EventBus;
import de.cbb.mplayer.main.events.NotificationEvent;
import de.cbb.mplayer.player.events.MediaStopEvent;
import java.io.File;
import javafx.application.Platform;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaPlayer.Status;
import javafx.util.Duration;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class InternalPlayer {

    private final EventBus eventBus;

    private MediaPlayer mediaPlayer;

    public void prepareMedia(final File file) {
        if (!file.exists()) {
            log.error("File not exists: {}", file.getAbsolutePath());
            eventBus.post(new NotificationEvent("startPlaying failed: File not exists: " + file.getAbsolutePath()));
            return;
        }
        stopPlaying();
        final Media currentMedia = new Media(file.toURI().toString());
        mediaPlayer = new MediaPlayer(currentMedia);
    }

    public void continuePlaying() {
        if (mediaPlayer != null
                && (mediaPlayer.getStatus().equals(Status.PAUSED)
                        || mediaPlayer.getStatus().equals(Status.READY))) {
            playAndRelease();
        }
    }

    public void pausePlaying() {
        if (mediaPlayer != null && mediaPlayer.getStatus().equals(Status.PLAYING)) {
            mediaPlayer.pause();
        }
    }

    public void rewind() {
        if (mediaPlayer != null && mediaPlayer.getStatus().equals(Status.PLAYING)) {
            mediaPlayer.stop();
        }
        playAndRelease();
    }

    public void stopPlaying() {
        if (mediaPlayer != null && mediaPlayer.getStatus().equals(Status.PLAYING)) {
            mediaPlayer.stop();
            mediaPlayer.dispose();
            Platform.runLater(() -> eventBus.post(new MediaStopEvent()));
        }
    }

    public void playAndRelease() {
        mediaPlayer.play();
        mediaPlayer.setOnEndOfMedia(() -> {
            mediaPlayer.dispose();
            Platform.runLater(() -> eventBus.post(new MediaStopEvent()));
        });
    }

    public Duration getCursor() {
        if (mediaPlayer != null && mediaPlayer.getStatus().equals(Status.PLAYING)) {
            return mediaPlayer.getCurrentTime();
        }
        return Duration.ZERO;
    }
}

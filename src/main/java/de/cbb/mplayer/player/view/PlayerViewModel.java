package de.cbb.mplayer.player.view;

import com.google.common.base.Strings;
import com.google.common.eventbus.EventBus;
import de.cbb.mplayer.common.Constants;
import de.cbb.mplayer.common.converters.DurationConverter;
import de.cbb.mplayer.config.ConfigProperties;
import de.cbb.mplayer.discogs.service.DiscogsService;
import de.cbb.mplayer.external.ExternalResourceService;
import de.cbb.mplayer.player.events.PlayTuneEvent;
import de.cbb.mplayer.player.service.InternalPlayer;
import de.cbb.mplayer.session.service.MediaBowl;
import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tunes.domain.Tune;
import de.saxsys.mvvmfx.ViewModel;
import java.io.File;
import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.util.Duration;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class PlayerViewModel implements ViewModel {

    private static final Image IMG_UNKNOWN_COVER = new Image("/img/unknown-cover.png");
    private static final Image IMG_PLAY = new Image("/img/play-custom.png");
    private static final Image IMG_PAUSE = new Image("/img/pause-custom.png");

    private final EventBus eventBus;
    private final InternalPlayer player;
    private final MediaBowl mediaBowl;
    private final ExternalResourceService externalResourceService;
    private final ConfigProperties properties;
    private final DiscogsService discogsService;

    private Tune currentTune = null;

    @Getter
    private final ObjectProperty<Image> playPauseProperty = new SimpleObjectProperty<>(IMG_PLAY);

    @Getter
    private final ObservableList<Tag> shownTags = FXCollections.observableArrayList();

    @Getter
    private final ObjectProperty<PlayerState> playerStateProperty = new SimpleObjectProperty<>(PlayerState.INIT);

    @Getter
    private final Property<Boolean> shuffleProperty = new SimpleBooleanProperty(false);

    @Getter
    private final Property<Boolean> cueProperty = new SimpleBooleanProperty(false);

    @Getter
    private final StringProperty artistProperty = new SimpleStringProperty();

    @Getter
    private final StringProperty titleProperty = new SimpleStringProperty();

    @Getter
    private final DoubleProperty cursorProperty = new SimpleDoubleProperty(0);

    @Getter
    private final DoubleProperty durationProperty = new SimpleDoubleProperty(0);

    @Getter
    private final StringProperty cursorTextProperty = new SimpleStringProperty();

    @Getter
    private final StringProperty durationTextProperty = new SimpleStringProperty();

    @Getter
    private final ObjectProperty<Image> coverProperty = new SimpleObjectProperty<>(IMG_UNKNOWN_COVER);

    @Getter
    private final BooleanProperty tabsDisabledProperty = new SimpleBooleanProperty(true);

    public void initialize() {}

    public void playNextTune() {
        log.debug("playTune");
        final Tune tune;
        if (shuffleProperty.getValue() == Boolean.FALSE) {
            tune = mediaBowl.getNextStraight();
        } else {
            tune = mediaBowl.getNextRandom();
        }

        resetControls();
        if (tune == null) {
            currentTune = null;
            artistProperty.set("End of playlist");
            return;
        }
        currentTune = tune;
        bindTunetoProperties();
        player.prepareMedia(new File(buildPath(tune)));
        if (cueProperty.getValue()) {
            setState(PlayerState.PAUSED);
        } else {
            player.playAndRelease();
            eventBus.post(new PlayTuneEvent(tune));
            setState(PlayerState.PLAYING);
        }
        cueProperty.setValue(false);
        showAlbumCover(tune);
    }

    public void playPauseTune() {
        if (playerStateProperty.get() == PlayerState.PAUSED) {
            continueTune();
        } else if (playerStateProperty.get() == PlayerState.INIT) {
            playNextTune();
        } else if (playerStateProperty.get() == PlayerState.PLAYING) {
            pauseTune();
        }
    }

    public void rewindTune() {
        log.debug("rewindTune");
        player.rewind();
    }

    public void skipTune() {
        player.stopPlaying();
        resetControls();
    }

    public void showLyrics() {
        externalResourceService.openLyrics(currentTune);
    }

    public void showTabs() {
        externalResourceService.openTabs(currentTune);
    }

    public void showMedia() {
        externalResourceService.openMedia(currentTune);
    }

    public void updateCursor() {
        final Duration duration = player.getCursor();
        cursorTextProperty.set(new DurationConverter()
                .toString(Double.valueOf(duration.toSeconds()).longValue()));
        cursorProperty.set(duration.toSeconds());
    }

    private void continueTune() {
        log.debug("continueTune");
        player.continuePlaying();
        setState(PlayerState.PLAYING);
    }

    private void bindTunetoProperties() {
        if (currentTune.getDuration() != null) {
            durationProperty.set(currentTune.getDuration());
            durationTextProperty.set(new DurationConverter().toString(Double.valueOf(currentTune.getDuration())));
        }
        artistProperty.set(currentTune.getArtist());
        titleProperty.set(currentTune.getTitle());
        tabsDisabledProperty.set(Strings.isNullOrEmpty(currentTune.getTabsUrl()));
        for (Tag tag : currentTune.getTags()) {
            if (tag.isShowInPlayer()) shownTags.add(tag);
        }
    }

    private void showAlbumCover(final Tune tune) {
        Platform.runLater(() -> {
            final String cover = discogsService.getAlbumCover(tune.getArtist(), tune.getAlbum(), tune.getYear2());
            if (Strings.isNullOrEmpty(cover)) {
                coverProperty.set(IMG_UNKNOWN_COVER);
            } else {
                final Image img = new Image(cover);
                coverProperty.set(img);
            }
        });
    }

    private void setState(final PlayerState state) {
        playerStateProperty.set(state);
        switch (state) {
            case INIT, PAUSED -> playPauseProperty.set(IMG_PLAY);
            case PLAYING -> playPauseProperty.set(IMG_PAUSE);
        }
    }

    private void pauseTune() {
        log.debug("pauseTune");
        player.pausePlaying();
        setState(PlayerState.PAUSED);
    }

    private String buildPath(final Tune tune) {
        return properties.getMediaRoot()
                + Constants.FILE_SEPARATOR
                + tune.getPath()
                + Constants.FILE_SEPARATOR
                + tune.getFilename2();
    }

    private void resetControls() {
        setState(PlayerState.INIT);
        cursorTextProperty.set("0:00");
        artistProperty.set("");
        titleProperty.set("");
        cursorProperty.set(0);
        durationProperty.set(0);
        coverProperty.set(IMG_UNKNOWN_COVER);
        shownTags.clear();
    }
}

package de.cbb.mplayer.player.view;

public enum PlayerState {
    INIT,
    PAUSED,
    PLAYING
}

package de.cbb.mplayer.player.view;

import de.cbb.mplayer.common.ui.controls.Chip;
import de.cbb.mplayer.tags.domain.Tag;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.Duration;
import org.springframework.stereotype.Component;

@Component
public class PlayerView implements FxmlView<PlayerViewModel> {

    @FXML
    private Button pbPlayPause;

    @FXML
    private ImageView imgPlayPause;

    @FXML
    private Button pbRewind;

    @FXML
    private Button pbNext;

    @FXML
    private ToggleButton toggleShuffle;

    @FXML
    private ToggleButton toggleCue;

    @FXML
    private Label lbArtist;

    @FXML
    private Label lbTitle;

    @FXML
    private Button pbOpenLyrics;

    @FXML
    private Button pbOpenTabs;

    @FXML
    private Button pbOpenMedia;

    @FXML
    private Slider slTime;

    @FXML
    private Label lbCursor;

    @FXML
    private Label lbDuration;

    @FXML
    private ImageView imgCover;

    @FXML
    private HBox pnTags;

    @InjectViewModel
    private PlayerViewModel viewModel;

    public void initialize() {
        imgPlayPause.imageProperty().bind(viewModel.getPlayPauseProperty());
        pbPlayPause.setOnAction(event -> viewModel.playPauseTune());
        pbRewind.setOnAction(event -> viewModel.rewindTune());
        pbNext.setOnAction(event -> viewModel.skipTune());
        toggleShuffle.selectedProperty().bindBidirectional(viewModel.getShuffleProperty());
        toggleCue.selectedProperty().bindBidirectional(viewModel.getCueProperty());
        lbArtist.textProperty().bind(viewModel.getArtistProperty());
        lbTitle.textProperty().bind(viewModel.getTitleProperty());
        pbOpenLyrics.setOnAction(event -> viewModel.showLyrics());
        pbOpenTabs.disableProperty().bind(viewModel.getTabsDisabledProperty());
        pbOpenTabs.setOnAction(event -> viewModel.showTabs());
        pbOpenMedia.setOnAction(event -> viewModel.showMedia());
        imgCover.imageProperty().bind(viewModel.getCoverProperty());

        slTime.setMin(0);
        slTime.setBlockIncrement(10);
        slTime.maxProperty().bind(viewModel.getDurationProperty());
        slTime.valueProperty().bind(viewModel.getCursorProperty());

        final Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1.0), e -> viewModel.updateCursor()));
        timeline.setCycleCount(Timeline.INDEFINITE);

        viewModel.getPlayerStateProperty().addListener((observableValue, playerState, t1) -> {
            if (t1.equals(PlayerState.PLAYING)) {
                timeline.play();
            } else {
                timeline.pause();
            }
        });

        lbCursor.textProperty().bind(viewModel.getCursorTextProperty());
        lbDuration.textProperty().bind(viewModel.getDurationTextProperty());
        updateShownTags();
        viewModel.getShownTags().addListener((ListChangeListener<Tag>) change -> updateShownTags());
    }

    private void updateShownTags() {
        pnTags.getChildren().clear();
        viewModel.getShownTags().forEach(tag -> addChip(pnTags, tag));
    }

    private void addChip(HBox pnTags, Tag tag) {
        Chip chip = tag.getIcon() != null
                ? new Chip(tag.getIcon(), tag.getColor())
                : new Chip(tag.getValue(), tag.getColor());
        pnTags.getChildren().add(chip);
    }
}

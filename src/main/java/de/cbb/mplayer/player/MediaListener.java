package de.cbb.mplayer.player;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import de.cbb.mplayer.common.listeners.DefaultListener;
import de.cbb.mplayer.player.events.MediaStopEvent;
import de.cbb.mplayer.player.view.PlayerViewModel;
import org.springframework.stereotype.Component;

@Component
public class MediaListener extends DefaultListener {

    private final PlayerViewModel viewModel;

    public MediaListener(EventBus eventBus, DefaultErrorHandler defaultErrorHandler, final PlayerViewModel viewModel) {
        super(eventBus, defaultErrorHandler);
        this.viewModel = viewModel;
    }

    @Subscribe
    public void handleMediaStopEvent(final MediaStopEvent event) {
        try {
            viewModel.playNextTune();
        } catch (final Exception ex) {
            defaultErrorHandler.handle("handleMediaStopEvent", ex);
        }
    }
}

package de.cbb.mplayer.player.events;

import de.cbb.mplayer.perspectives.events.AbstractTuneEvent;
import de.cbb.mplayer.tunes.domain.Tune;

public class PlayTuneEvent extends AbstractTuneEvent {

    public PlayTuneEvent(Tune tune) {
        super(tune);
    }
}

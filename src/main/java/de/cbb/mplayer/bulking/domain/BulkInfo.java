package de.cbb.mplayer.bulking.domain;

import de.cbb.mplayer.tags.domain.Tag;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BulkInfo {

    private Optional<String> path = Optional.empty();
    private Optional<String> artist = Optional.empty();
    private Optional<String> album = Optional.empty();
    private Optional<Integer> year = Optional.empty();
    private List<Tag> tags = new ArrayList<>();
    private List<Tag> untags = new ArrayList<>();
}

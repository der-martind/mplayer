package de.cbb.mplayer.bulking.view;

import de.cbb.mplayer.tags.view.TagSelectionDialog;
import de.cbb.mplayer.tags.view.chips.ChipsPane;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.WindowEvent;
import javafx.util.converter.NumberStringConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BulkView implements FxmlView<BulkViewModel> {

    @FXML
    private CheckBox cbBulkPath;

    @FXML
    private TextField tfPath;

    @FXML
    private CheckBox cbBulkArtist;

    @FXML
    private TextField tfArtist;

    @FXML
    private CheckBox cbBulkAlbum;

    @FXML
    private TextField tfAlbum;

    @FXML
    private CheckBox cbBulkYear;

    @FXML
    private TextField tfYear;

    @FXML
    private HBox ctnTags;

    @FXML
    private HBox ctnUntags;

    @FXML
    private Button pbSave;

    @InjectViewModel
    private BulkViewModel viewModel;

    private final TagSelectionDialog tagSelectionDialog;

    public void initialize() {
        cbBulkPath.selectedProperty().bindBidirectional(viewModel.getBulkPath());
        bindTextField(tfPath, viewModel.getPathProperty(), viewModel.getBulkPath());
        cbBulkArtist.selectedProperty().bindBidirectional(viewModel.getBulkArtist());
        bindTextField(tfArtist, viewModel.getArtistProperty(), viewModel.getBulkArtist());
        cbBulkAlbum.selectedProperty().bindBidirectional(viewModel.getBulkAlbum());
        bindTextField(tfAlbum, viewModel.getAlbumProperty(), viewModel.getBulkAlbum());
        cbBulkYear.selectedProperty().bindBidirectional(viewModel.getBulkYear());
        Bindings.bindBidirectional(
                tfYear.textProperty(), viewModel.getYearProperty(), new NumberStringConverter("0000"));
        tfYear.disableProperty().bind(viewModel.getBulkYear().not());
        pbSave.setOnAction(event -> pbSave.getScene()
                .getWindow()
                .fireEvent(new WindowEvent(pbSave.getScene().getWindow(), WindowEvent.WINDOW_CLOSE_REQUEST)));
        final ChipsPane pnTags = new ChipsPane(viewModel.getTagService(), viewModel.getTags(), tagSelectionDialog);
        ctnTags.getChildren().add(pnTags);
        final ChipsPane pnUntags = new ChipsPane(viewModel.getTagService(), viewModel.getUntags(), tagSelectionDialog);
        ctnUntags.getChildren().add(pnUntags);
    }

    private void bindTextField(final TextField tf, final StringProperty property, final BooleanProperty b) {
        property.bindBidirectional(tf.textProperty());
        tf.disableProperty().bind(b.not());
    }
}

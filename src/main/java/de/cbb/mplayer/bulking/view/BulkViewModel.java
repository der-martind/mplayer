package de.cbb.mplayer.bulking.view;

import de.cbb.mplayer.bulking.domain.BulkInfo;
import de.cbb.mplayer.common.ui.dialogs.DialogViewModel;
import de.cbb.mplayer.discogs.domain.Result;
import de.cbb.mplayer.discogs.domain.Root;
import de.cbb.mplayer.discogs.service.DiscogsService;
import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tags.service.TagService;
import de.saxsys.mvvmfx.ViewModel;
import java.util.List;
import java.util.Optional;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class BulkViewModel implements DialogViewModel<BulkInfo>, ViewModel {

    @Getter
    private final TagService tagService;

    private final DiscogsService discogsService;

    @Getter
    private final BooleanProperty bulkPath = new SimpleBooleanProperty();

    @Getter
    private final StringProperty pathProperty = new SimpleStringProperty();

    @Getter
    private final BooleanProperty bulkArtist = new SimpleBooleanProperty();

    @Getter
    private final StringProperty artistProperty = new SimpleStringProperty();

    @Getter
    private final BooleanProperty bulkAlbum = new SimpleBooleanProperty();

    @Getter
    private final StringProperty albumProperty = new SimpleStringProperty();

    @Getter
    private final BooleanProperty bulkYear = new SimpleBooleanProperty();

    @Getter
    private final IntegerProperty yearProperty = new SimpleIntegerProperty();

    @Getter
    private final ObservableSet<Tag> tags = FXCollections.observableSet();

    @Getter
    private final ObservableSet<Tag> untags = FXCollections.observableSet();

    public List<Result> getDiscogs() {
        final Root r = discogsService.getReleases(artistProperty.get(), albumProperty.get());
        return r.getResults();
    }

    @Override
    public BulkInfo getResult() {
        final BulkInfo bulkInfo = new BulkInfo();
        if (bulkPath.get()) {
            bulkInfo.setPath(Optional.of(pathProperty.get()));
        }
        if (bulkArtist.get()) {
            bulkInfo.setArtist(Optional.of(artistProperty.get()));
        }
        if (bulkAlbum.get()) {
            bulkInfo.setAlbum(Optional.of(albumProperty.get()));
        }
        if (bulkYear.get()) {
            bulkInfo.setYear(Optional.of(yearProperty.get()));
        }
        if (!tags.isEmpty()) {
            bulkInfo.getTags().addAll(tags);
        }
        if (!untags.isEmpty()) {
            bulkInfo.getUntags().addAll(untags);
        }
        return bulkInfo;
    }

    @Override
    public void reset() {
        pathProperty.set("");
        artistProperty.set("");
        albumProperty.set("");
        yearProperty.set(0);
        bulkPath.set(false);
        bulkArtist.set(false);
        bulkAlbum.set(false);
        bulkYear.set(false);
        tags.clear();
        untags.clear();
    }
}

package de.cbb.mplayer.ranking.view;

import de.cbb.mplayer.common.ui.PointOnScreen;
import de.cbb.mplayer.common.ui.UIConstants;
import de.cbb.mplayer.ranking.domain.Ranking;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class RankingDialog {

    private static final String TITLE = "Ranking";

    public static void display(final Ranking ranking, final PointOnScreen point) {

        final Stage window = initWindow(point);

        final HBox hbContent = new HBox();
        hbContent.getStyleClass().add("component");

        addRanking(hbContent, "Artist", ranking.getArtistRanking());
        addRanking(hbContent, "Album", ranking.getAlbumRanking());
        addRanking(hbContent, "Title", ranking.getTitleRanking());

        hbContent.addEventHandler(KeyEvent.KEY_PRESSED, t -> {
            if (t.getCode() == KeyCode.ESCAPE) {
                window.close();
            }
        });

        final Scene scene = new Scene(hbContent);
        scene.getStylesheets().add(UIConstants.STYLESHEET);
        scene.addEventHandler(KeyEvent.KEY_PRESSED, t -> {
            if (t.getCode() == KeyCode.ESCAPE) {
                window.close();
            }
        });
        window.setScene(scene);
        window.showAndWait();
    }

    private static void addRanking(final Pane parent, final String header, final Integer ranking) {
        final VBox vb = new VBox();
        vb.setAlignment(Pos.CENTER);
        final Label lb = new Label(header);
        lb.getStyleClass().add("ranking-header");
        vb.getChildren().add(lb);
        final StackPane pn = new StackPane();
        pn.setAlignment(Pos.CENTER);
        final ImageView imgRanking;
        if (ranking > 0 && ranking <= 10) {
            imgRanking = new ImageView("/img/laurel-gold.png");
        } else {
            imgRanking = new ImageView("/img/laurel-white.png");
        }
        pn.getChildren().add(imgRanking);
        pn.getChildren().add(new Label(ranking.toString()));
        vb.getChildren().add(pn);
        parent.getChildren().add(vb);
    }

    private static Stage initWindow(final PointOnScreen point) {
        final Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(TITLE);
        window.initStyle(StageStyle.UTILITY);
        window.setX(point.getX());
        window.setY(point.getY());
        return window;
    }
}

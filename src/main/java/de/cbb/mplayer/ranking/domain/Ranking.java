package de.cbb.mplayer.ranking.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Ranking {

    private Integer artistRanking;
    private Integer albumRanking;
    private Integer titleRanking;
}

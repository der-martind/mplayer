package de.cbb.mplayer.ranking.service;

import de.cbb.mplayer.charts.repo.ChartRepository;
import de.cbb.mplayer.ranking.domain.Ranking;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class RankingService {

    private final ChartRepository chartRepo;

    public Integer findArtistRanking(final String artist) {
        return getRanking(chartRepo.findArtistsOrderByCount(), artist);
    }

    public Integer findAlbumRanking(final String album) {
        return getRanking(chartRepo.findAlbumsOrderByCount(), album);
    }

    public Integer findTitleRanking(final String title) {
        return getRanking(chartRepo.findTitlesOrderByCount(), title);
    }

    public Ranking getRanking(final String artist, final String album, final String title) {
        final Integer artistRanking = findArtistRanking(artist);
        final Integer albumRanking = findAlbumRanking(album);
        final Integer titleRanking = findTitleRanking(title);
        return new Ranking(artistRanking, albumRanking, titleRanking);
    }

    private Integer getRanking(final List<Object[]> entries, final String name) {
        Long lastPlayed = Long.MAX_VALUE;
        int ranking = 0;
        for (final Object[] entry : entries) {
            if ((Long) entry[0] < lastPlayed) {
                ranking++;
                lastPlayed = (Long) entry[0];
            }
            if (name.equalsIgnoreCase((String) entry[1])) {
                return ranking;
            }
        }
        return 0;
    }
}

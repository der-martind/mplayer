package de.cbb.mplayer.tunes.domain;

import de.cbb.mplayer.tags.domain.Tag;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Tune {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer tid;

    @NotNull
    private String artist;

    @NotNull
    private String album;

    @NotNull
    private String title;

    @Min(0)
    @Max(9999)
    private Integer year2;

    @NotNull
    private String path;

    @Deprecated
    private String filename;

    @NotNull
    private String filename2;

    @NotNull
    private LocalDate added = LocalDate.now();

    @NotNull
    private LocalDate updated = LocalDate.now();

    private String tuning;

    @Column(name = "tabsurl")
    private String tabsUrl;

    @Column(name = "lyricsurl")
    private String lyricsUrl;

    @Column(name = "mediaurl")
    private String mediaUrl;

    @Deprecated
    @Column(nullable = false, columnDefinition = "BOOLEAN default false")
    private boolean liked = false;

    @Deprecated
    @Column(nullable = false, columnDefinition = "BOOLEAN default false")
    private boolean disliked = false;

    private Long duration;

    @ManyToMany(
            cascade = {CascadeType.MERGE},
            fetch = FetchType.LAZY)
    @JoinTable(
            name = "tune_tag",
            joinColumns = @JoinColumn(name = "tune_tid", referencedColumnName = "tid"),
            inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "id"))
    private Set<Tag> tags = new HashSet<>();

    @Deprecated
    @Column(nullable = false, columnDefinition = "BOOLEAN default true")
    private boolean discogs = true;

    public Tune(final String artist, final String album, final String title) {
        this.artist = artist;
        this.album = album;
        this.title = title;
    }

    @Override
    public String toString() {
        return artist + " - " + title;
    }

    public boolean containsTag(final Tag tag) { // set.contains doesnt work
        return tags.stream().anyMatch(t -> t.equals(tag));
    }

    public void removeTag(final Tag tag) { // set.remove doesnt work
        getTags().removeIf(tag1 -> tag1.equals(tag));
    }
}

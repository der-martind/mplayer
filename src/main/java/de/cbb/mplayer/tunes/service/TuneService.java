package de.cbb.mplayer.tunes.service;

import com.google.common.base.Strings;
import de.cbb.mplayer.charts.service.ChartService;
import de.cbb.mplayer.common.Constants;
import de.cbb.mplayer.search.domain.SearchFilter;
import de.cbb.mplayer.tunes.domain.Tune;
import de.cbb.mplayer.tunes.repo.TuneRepository;
import java.time.LocalDate;
import java.util.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class TuneService {

    private final TuneRepository tuneRepository;
    private final ChartService chartService;

    public Tune save(final Tune tune) {
        if (tune.getTid() == null) { // new tune
            tune.setAdded(LocalDate.now());
        }
        tune.setUpdated(LocalDate.now());
        if (Strings.isNullOrEmpty(tune.getAlbum())) {
            tune.setAlbum(Constants.DEFAULT_ALBUM);
        }
        if (tune.getYear2() == null) {
            tune.setYear2(Constants.DEFAULT_YEAR);
        }
        final Tune persistedTune = tuneRepository.saveAndFlush(tune);

        log.info("Tune saved: " + tune);
        return persistedTune;
    }

    public List<String> getDistinctArtistsByFilterOrderByArtist(final Set<SearchFilter<?>> searchFilters) {
        return tuneRepository.findDistinctArtistsByFilterOrderByArtist(searchFilters);
    }

    public List<String> getDistinctArtistsByFilterOrderByAge(final Set<SearchFilter<?>> searchFilters) {
        return tuneRepository.findDistinctArtistsByFilterOrderByAdded(searchFilters);
    }

    public List<String> getDistinctArtistsByFilterOrderByPlayed(final Set<SearchFilter<?>> searchFilters) {
        List<String> artists = tuneRepository.findDistinctArtistsByFilterOrderByArtist(searchFilters);
        return chartService.sortArtistsByPlayed(artists);
    }

    public Optional<Tune> getByPathAndFilename2(final String path, final String name) {
        return tuneRepository.findByPathAndFilename2(path, name);
    }

    public List<Tune> getByPathStartingWith(final String path) {
        return tuneRepository.findByPathStartingWith(path);
    }

    public Optional<Tune> getById(final Integer tid) {
        return tuneRepository.findById(tid);
    }

    public List<Tune> getByArtistAndFilters(final String artist, final Set<SearchFilter<?>> searchFilters) {
        return tuneRepository.findByArtistAndFilters(artist, searchFilters);
    }

    public List<Tune> getByArtistAndAlbumAndFilters(
            final String artist, final String album, final Set<SearchFilter<?>> searchFilters) {
        return tuneRepository.findByArtistAndAlbumAndFilters(artist, album, searchFilters);
    }

    public List<Tune> getByArtistAndAlbumAndFiltersOrderByTitle(
            final String artist, final String album, final Set<SearchFilter<?>> searchFilters) {
        return tuneRepository.findByArtistAndAlbumAndFiltersOrderByTitle(artist, album, searchFilters);
    }

    public List<Tune> getByArtistAndAlbumAndFiltersOrderByAge(
            final String artist, final String album, final Set<SearchFilter<?>> searchFilters) {
        return tuneRepository.findByArtistAndAlbumAndFiltersOrderByAdded(artist, album, searchFilters);
    }

    public List<Tune> getByArtistAndAlbumAndFiltersOrderByPlayed(
            final String artist, final String album, final Set<SearchFilter<?>> searchFilters) {
        List<Tune> tunes = tuneRepository.findByArtistAndAlbumAndFilters(artist, album, searchFilters);
        return chartService.sortTunesByPlayed(tunes);
    }

    public List<Tune> getAll() {
        return tuneRepository.findAll(Sort.by("artist", "album", "title"));
    }

    public List<String> getDistinctAlbumsByArtistAndFiltersOrderByAlbum(
            final String artist, final Set<SearchFilter<?>> searchFilters) {
        return tuneRepository.findDistinctAlbumsByArtistAndFiltersOrderByAlbum(artist, searchFilters);
    }

    public List<String> getDistinctAlbumsByArtistAndFiltersOrderByAge(
            final String artist, final Set<SearchFilter<?>> searchFilters) {
        return tuneRepository.findDistinctAlbumsByArtistAndFiltersOrderByAdded(artist, searchFilters);
    }

    public List<String> getDistinctAlbumsByArtistAndFiltersOrderByPlayed(
            final String artist, final Set<SearchFilter<?>> searchFilters) {
        List<String> list = tuneRepository.findDistinctAlbumsByArtistAndFiltersOrderByAlbum(artist, searchFilters);
        return chartService.sortAlbumsByPlayed(artist, list);
    }

    public List<Tune> getByFilters(final Set<SearchFilter<?>> filters) {
        return tuneRepository.findByFilters(filters);
    }

    public List<Tune> getByContains(final String s, final int maxSize) {
        return tuneRepository.findByArtistContainingIgnoreCaseOrTitleContainingIgnoreCase(
                s, s, Pageable.ofSize(maxSize));
    }
}

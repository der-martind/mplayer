package de.cbb.mplayer.tunes.service.sorting;

import de.cbb.mplayer.search.domain.SearchFilter;
import de.cbb.mplayer.tunes.domain.Tune;
import de.cbb.mplayer.tunes.service.TuneService;
import java.util.List;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ByPlayedOrderStrategy implements TuneReadingOrderStrategy {

    private final TuneService tuneService;

    public List<Tune> getByArtistAndAlbumAndFilters(
            final String artist, final String album, Set<SearchFilter<?>> searchFilters) {
        return tuneService.getByArtistAndAlbumAndFiltersOrderByPlayed(artist, album, searchFilters);
    }

    @Override
    public List<String> getDistinctArtistsByFilter(Set<SearchFilter<?>> searchFilters) {
        return tuneService.getDistinctArtistsByFilterOrderByPlayed(searchFilters);
    }

    @Override
    public List<String> getDistinctAlbumsByArtistAndFilters(final String artist, Set<SearchFilter<?>> searchFilters) {
        return tuneService.getDistinctAlbumsByArtistAndFiltersOrderByPlayed(artist, searchFilters);
    }
}

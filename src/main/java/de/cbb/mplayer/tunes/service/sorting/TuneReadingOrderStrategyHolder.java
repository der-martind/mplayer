package de.cbb.mplayer.tunes.service.sorting;

import de.cbb.mplayer.perspectives.artists.Sorting;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TuneReadingOrderStrategyHolder {

    private final ByAlphaOrderStrategy byAlphaOrderStrategy;
    private final ByAgeOrderStrategy byAgeOrderStrategy;
    private final ByPlayedOrderStrategy byPlayedOrderStrategy;

    public TuneReadingOrderStrategy getStrategy(final Sorting sorting) {
        if (Sorting.AGE.equals(sorting)) return byAgeOrderStrategy;
        else if (Sorting.PLAYED.equals(sorting)) return byPlayedOrderStrategy;
        else return byAlphaOrderStrategy;
    }
}

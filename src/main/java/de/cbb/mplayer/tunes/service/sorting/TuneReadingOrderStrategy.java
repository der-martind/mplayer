package de.cbb.mplayer.tunes.service.sorting;

import de.cbb.mplayer.search.domain.SearchFilter;
import de.cbb.mplayer.tunes.domain.Tune;
import java.util.List;
import java.util.Set;

public interface TuneReadingOrderStrategy {

    List<Tune> getByArtistAndAlbumAndFilters(
            final String artist, final String album, Set<SearchFilter<?>> searchFilters);

    List<String> getDistinctArtistsByFilter(Set<SearchFilter<?>> searchFilters);

    List<String> getDistinctAlbumsByArtistAndFilters(final String artist, Set<SearchFilter<?>> searchFilters);
}

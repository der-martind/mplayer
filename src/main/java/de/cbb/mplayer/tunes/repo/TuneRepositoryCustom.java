package de.cbb.mplayer.tunes.repo;

import de.cbb.mplayer.search.domain.SearchFilter;
import de.cbb.mplayer.tunes.domain.Tune;
import java.util.List;
import java.util.Set;

public interface TuneRepositoryCustom {

    List<String> findDistinctArtistsByFilterOrderByArtist(Set<SearchFilter<?>> searchFilters);

    List<String> findDistinctAlbumsByArtistAndFiltersOrderByAlbum(String artist, Set<SearchFilter<?>> searchFilters);

    List<String> findDistinctAlbumsByArtistAndFiltersOrderByAdded(String artist, Set<SearchFilter<?>> searchFilters);

    List<String> findDistinctArtistsByFilterOrderByAdded(Set<SearchFilter<?>> searchFilters);

    List<Tune> findByArtistAndFilters(String artist, Set<SearchFilter<?>> filters);

    List<Tune> findByArtistAndAlbumAndFilters(String artist, String album, Set<SearchFilter<?>> filters);

    List<Tune> findByArtistAndAlbumAndFiltersOrderByTitle(String artist, String album, Set<SearchFilter<?>> filters);

    List<Tune> findByArtistAndAlbumAndFiltersOrderByAdded(String artist, String album, Set<SearchFilter<?>> filters);

    List<Tune> findByFilters(Set<SearchFilter<?>> filters);
}

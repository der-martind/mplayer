package de.cbb.mplayer.tunes.repo;

import de.cbb.mplayer.search.domain.SearchFilter;
import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tags.domain.Tag_;
import de.cbb.mplayer.tunes.domain.Tune;
import de.cbb.mplayer.tunes.domain.Tune_;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;

@SuppressWarnings("unused")
public class TuneRepositoryCustomImpl implements TuneRepositoryCustom {

    @SuppressWarnings({"UnusedDeclaration"})
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<String> findDistinctArtistsByFilterOrderByArtist(final Set<SearchFilter<?>> searchFilters) {
        final CriteriaBuilder cbuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<String> cquery = cbuilder.createQuery(String.class);
        final Root<Tune> root = cquery.from(Tune.class);

        final List<Predicate> predicates = buildPredicates(searchFilters, root, cbuilder, cquery);
        cquery.select(root.get(Tune_.artist))
                .distinct(true)
                .where(cbuilder.and(predicates.toArray(new Predicate[0])))
                .orderBy(cbuilder.asc(root.get(Tune_.artist)));

        return entityManager.createQuery(cquery).getResultList();
    }

    @Override
    public List<String> findDistinctAlbumsByArtistAndFiltersOrderByAlbum(
            final String artist, final Set<SearchFilter<?>> searchFilters) {
        final CriteriaBuilder cbuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<String> cquery = cbuilder.createQuery(String.class);
        final Root<Tune> root = cquery.from(Tune.class);

        final List<Predicate> predicates = buildPredicates(searchFilters, root, cbuilder, cquery);
        predicates.add(cbuilder.equal(root.get(Tune_.artist), artist));
        cquery.select(root.get(Tune_.album))
                .distinct(true)
                .where(cbuilder.and(predicates.toArray(new Predicate[0])))
                .orderBy(cbuilder.asc(root.get(Tune_.album)));

        return entityManager.createQuery(cquery).getResultList();
    }

    @Override
    public List<String> findDistinctArtistsByFilterOrderByAdded(final Set<SearchFilter<?>> searchFilters) {
        final CriteriaBuilder cbuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Object[]> cquery = cbuilder.createQuery(Object[].class);
        final Root<Tune> root = cquery.from(Tune.class);

        final List<Predicate> predicates = buildPredicates(searchFilters, root, cbuilder, cquery);

        Path<String> artistPath = root.get(Tune_.artist);
        Expression<LocalDate> maxAdded = cbuilder.greatest(root.get(Tune_.added));

        cquery.multiselect(artistPath, maxAdded)
                .where(cbuilder.and(predicates.toArray(new Predicate[0])))
                .groupBy(artistPath)
                .orderBy(cbuilder.desc(maxAdded));

        List<Object[]> results = entityManager.createQuery(cquery).getResultList();

        // Extract artist names from the result
        return results.stream().map(result -> (String) result[0]).distinct().collect(Collectors.toList());
    }

    @Override
    public List<String> findDistinctAlbumsByArtistAndFiltersOrderByAdded(
            final String artist, final Set<SearchFilter<?>> searchFilters) {
        final CriteriaBuilder cbuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Object[]> cquery = cbuilder.createQuery(Object[].class);
        final Root<Tune> root = cquery.from(Tune.class);

        final List<Predicate> predicates = buildPredicates(searchFilters, root, cbuilder, cquery);
        predicates.add(cbuilder.equal(root.get(Tune_.artist), artist));

        Path<String> albumPath = root.get(Tune_.album);
        Expression<LocalDate> maxAdded = cbuilder.greatest(root.get(Tune_.added));

        cquery.multiselect(albumPath, maxAdded)
                .where(cbuilder.and(predicates.toArray(new Predicate[0])))
                .groupBy(albumPath)
                .orderBy(cbuilder.desc(maxAdded));

        List<Object[]> results = entityManager.createQuery(cquery).getResultList();

        // Extract album names from the result
        return results.stream().map(result -> (String) result[0]).toList();
    }

    @Override
    public List<Tune> findByArtistAndFilters(final String artist, final Set<SearchFilter<?>> searchFilters) {
        final CriteriaBuilder cbuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Tune> cquery = cbuilder.createQuery(Tune.class);
        final Root<Tune> root = cquery.from(Tune.class);

        final List<Predicate> predicates = buildPredicates(searchFilters, root, cbuilder, cquery);
        predicates.add(cbuilder.equal(root.get(Tune_.artist), artist));
        cquery.select(root).distinct(true).where(cbuilder.and(predicates.toArray(new Predicate[0])));

        return entityManager.createQuery(cquery).getResultList();
    }

    @Override
    public List<Tune> findByArtistAndAlbumAndFilters(
            final String artist, final String album, final Set<SearchFilter<?>> searchFilters) {
        final CriteriaBuilder cbuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Tune> cquery = cbuilder.createQuery(Tune.class);
        final Root<Tune> root = cquery.from(Tune.class);

        final List<Predicate> predicates = buildPredicates(searchFilters, root, cbuilder, cquery);
        predicates.add(cbuilder.equal(root.get(Tune_.artist), artist));
        predicates.add(cbuilder.equal(root.get(Tune_.album), album));
        cquery.select(root).distinct(true).where(cbuilder.and(predicates.toArray(new Predicate[0])));

        return entityManager.createQuery(cquery).getResultList();
    }

    @Override
    public List<Tune> findByArtistAndAlbumAndFiltersOrderByTitle(
            final String artist, final String album, final Set<SearchFilter<?>> searchFilters) {
        final CriteriaBuilder cbuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Tune> cquery = cbuilder.createQuery(Tune.class);
        final Root<Tune> root = cquery.from(Tune.class);

        final List<Predicate> predicates = buildPredicates(searchFilters, root, cbuilder, cquery);
        predicates.add(cbuilder.equal(root.get(Tune_.artist), artist));
        predicates.add(cbuilder.equal(root.get(Tune_.album), album));
        cquery.select(root)
                .distinct(true)
                .where(cbuilder.and(predicates.toArray(new Predicate[0])))
                .orderBy(cbuilder.asc(root.get(Tune_.title)));

        return entityManager.createQuery(cquery).getResultList();
    }

    @Override
    public List<Tune> findByArtistAndAlbumAndFiltersOrderByAdded(
            final String artist, final String album, final Set<SearchFilter<?>> searchFilters) {
        final CriteriaBuilder cbuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Tune> cquery = cbuilder.createQuery(Tune.class);
        final Root<Tune> root = cquery.from(Tune.class);

        final List<Predicate> predicates = buildPredicates(searchFilters, root, cbuilder, cquery);
        predicates.add(cbuilder.equal(root.get(Tune_.artist), artist));
        predicates.add(cbuilder.equal(root.get(Tune_.album), album));
        cquery.select(root)
                .distinct(true)
                .where(cbuilder.and(predicates.toArray(new Predicate[0])))
                .orderBy(cbuilder.desc(root.get(Tune_.added)));

        return entityManager.createQuery(cquery).getResultList();
    }

    @Override
    public List<Tune> findByFilters(final Set<SearchFilter<?>> searchFilters) {
        final CriteriaBuilder cbuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Tune> cquery = cbuilder.createQuery(Tune.class);
        final Root<Tune> root = cquery.from(Tune.class);

        final List<Predicate> predicates = buildPredicates(searchFilters, root, cbuilder, cquery);
        cquery.select(root).distinct(true).where(cbuilder.and(predicates.toArray(new Predicate[0])));

        return entityManager.createQuery(cquery).getResultList();
    }

    private Predicate createTagPredicate(
            final String tag, final Root<Tune> root, final CriteriaBuilder cbuilder, final CriteriaQuery<?> cq) {
        // Initialize the subquery
        final Subquery<Integer> subquery = cq.subquery(Integer.class);
        final Root<Tune> subqueryTune = subquery.from(Tune.class);
        final SetJoin<Tune, Tag> subqueryTag = subqueryTune.join(Tune_.tags);

        // Select the Tune IDs where one of their tags matches
        subquery.select(subqueryTune.get(Tune_.tid)).where(cbuilder.equal(subqueryTag.get(Tag_.value), tag));

        // Filter by tunes that match one of the tunes found in the subquery
        return cbuilder.in(root.get(Tune_.tid)).value(subquery);
    }

    private List<Predicate> buildPredicates(
            final Set<SearchFilter<?>> searchFilters,
            final Root<Tune> root,
            final CriteriaBuilder cbuilder,
            final CriteriaQuery<?> cquery) {
        final List<Predicate> predicates = new ArrayList<>();
        for (final SearchFilter<?> searchFilter : searchFilters) {
            final Object value = searchFilter.getValue();
            if (value instanceof Tag tag) {
                predicates.add(createTagPredicate(tag.getValue(), root, cbuilder, cquery));
            } else throw new RuntimeException("TODO: implement buildPredicates for non-tags");
        }
        return predicates;
    }
}

package de.cbb.mplayer.tunes.repo;

import de.cbb.mplayer.tunes.domain.Tune;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TuneRepository
        extends JpaRepository<Tune, Integer>, JpaSpecificationExecutor<Tune>, TuneRepositoryCustom {

    @Override
    List<Tune> findAll(Sort sort);

    Optional<Tune> findByPathAndFilename2(String path, String filename2);

    List<Tune> findByPathStartingWith(String path);

    List<Tune> findByArtistContainingIgnoreCaseOrTitleContainingIgnoreCase(
            String artist, String title, Pageable pageable);
}

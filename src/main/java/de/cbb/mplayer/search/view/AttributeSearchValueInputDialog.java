package de.cbb.mplayer.search.view;

import com.google.common.eventbus.EventBus;
import de.cbb.mplayer.common.ui.PointOnScreen;
import de.cbb.mplayer.common.ui.UIConstants;
import de.cbb.mplayer.search.domain.SearchFilter;
import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tags.events.TagSelectedEvent;
import de.cbb.mplayer.tags.view.TagCellFactory;
import java.util.List;
import java.util.Optional;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.transformation.FilteredList;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AttributeSearchValueInputDialog {

    private static final String TITLE = "Enter attribute value";

    private final EventBus eventBus;

    public SearchFilter<?> display(final List<Tag> suggestions, final PointOnScreen point) {

        final Stage window = initWindow(point);

        final FilteredList<Tag> filteredData =
                new FilteredList<>(FXCollections.observableArrayList(suggestions), p -> true);

        final GridPane grid = new GridPane();
        grid.setId("text-selection-grid");

        final TextField tfText = new TextField();

        addTextListener(tfText, filteredData);

        addEnterKeyHandler(tfText, window);

        grid.add(tfText, 1, 0);

        final ListView<Tag> listView = new ListView<>(filteredData);
        listView.setId("text-selection-list");
        listView.setCellFactory(new TagCellFactory());

        addSelectListener(listView, tfText, window);

        final ScrollPane scrollPane = new ScrollPane(listView);
        scrollPane.setContent(listView);
        scrollPane.hbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.vbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        grid.add(scrollPane, 1, 1);

        Platform.runLater(tfText::requestFocus);

        final Scene scene = new Scene(grid);
        scene.getStylesheets().add(UIConstants.STYLESHEET);
        scene.addEventHandler(KeyEvent.KEY_PRESSED, t -> {
            if (t.getCode() == KeyCode.ESCAPE) {
                window.close();
            }
        });
        window.setScene(scene);
        window.showAndWait();

        final Optional<Tag> selectedTag = suggestions.stream()
                .filter(tag -> tag.getValue().equals(tfText.getText()))
                .findFirst();
        if (selectedTag.isPresent()) {
            return new SearchFilter<>(selectedTag.get());
        } else {
            throw new RuntimeException("Unimplemented");
        }
    }

    private void addSelectListener(final ListView<Tag> listView, final TextField tfText, final Stage window) {
        listView.getSelectionModel()
                .selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> Platform.runLater(() -> {
                    if (newValue != null) {
                        tfText.setText(newValue.getValue());
                        eventBus.post(new TagSelectedEvent(newValue));
                    }
                    window.close();
                }));
    }

    private void addEnterKeyHandler(final TextField tfText, final Stage window) {
        tfText.addEventHandler(KeyEvent.KEY_RELEASED, (event) -> {
            if (event.getCode().equals(KeyCode.ENTER)) {
                window.close();
            }
        });
    }

    private void addTextListener(final TextField tfText, final FilteredList<Tag> filteredData) {
        tfText.textProperty().addListener(obs -> {
            final String filter = tfText.getText();
            if (filter == null || filter.length() == 0) {
                filteredData.setPredicate(s -> true);
            } else {
                filteredData.setPredicate(s -> s.getValue().toLowerCase().contains(filter.toLowerCase()));
            }
        });
    }

    private Stage initWindow(final PointOnScreen point) {
        final Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(TITLE);
        window.setWidth(200);
        window.setHeight(300);
        window.initStyle(StageStyle.UTILITY);
        window.setX(point.getX());
        window.setY(point.getY());
        return window;
    }
}

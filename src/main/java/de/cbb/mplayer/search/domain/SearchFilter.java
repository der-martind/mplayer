package de.cbb.mplayer.search.domain;

import lombok.Data;

@Data
public class SearchFilter<T> {

    private final T value;
}

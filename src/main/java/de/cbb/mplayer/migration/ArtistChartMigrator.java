package de.cbb.mplayer.migration;

import de.cbb.mplayer.charts.domain.ArtistChart;
import de.cbb.mplayer.charts.domain.Chart;
import de.cbb.mplayer.charts.repo.ArtistChartRepository;
import de.cbb.mplayer.charts.repo.ChartRepository;
import de.cbb.mplayer.tunes.domain.Tune;
import de.cbb.mplayer.tunes.service.TuneService;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty("mplayer.tools.artistchart-migration")
@RequiredArgsConstructor
@Slf4j
public class ArtistChartMigrator implements CommandLineRunner {

    private final TuneService tuneService;
    private final ChartRepository chartRepository;
    private final ArtistChartRepository artistChartRepository;

    @Override
    public void run(final String... args) {
        log.info("START artist_chart migration");
        List<Chart> list = chartRepository.findAll();
        for (final Chart chart : list) {
            Optional<Tune> tune = tuneService.getById(chart.getTid());
            if (tune.isEmpty()) {
                log.warn("Tune.tid not found: {}", chart.getTid());
                continue;
            }
            ArtistChart artistChart =
                    artistChartRepository.findByArtistIgnoreCase(tune.get().getArtist());
            if (artistChart == null) {
                artistChart = new ArtistChart(tune.get().getArtist());
            }
            artistChart.setPlayed(artistChart.getPlayed() + 1);
            artistChartRepository.save(artistChart);
            log.info("ArtistChart saved: {} ({})", artistChart.getArtist(), artistChart.getPlayed());
        }
        log.info("END artist_chart migration");
    }
}

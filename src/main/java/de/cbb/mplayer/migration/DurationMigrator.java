package de.cbb.mplayer.migration;

import de.cbb.mplayer.common.Constants;
import de.cbb.mplayer.config.ConfigProperties;
import de.cbb.mplayer.importing.util.EncodingUtil;
import de.cbb.mplayer.tunes.domain.Tune;
import de.cbb.mplayer.tunes.service.TuneService;
import java.io.File;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty("mplayer.tools.duration-migration")
@RequiredArgsConstructor
@Slf4j
public class DurationMigrator implements CommandLineRunner {

    private final TuneService tuneService;
    private final ConfigProperties properties;

    @Override
    public void run(final String... args) {
        log.info("Starting duration migration");
        final List<Tune> list = tuneService.getAll();
        for (final Tune tune : list) {
            if (tune.getDuration() == null) {
                final File file = new File(properties.getMediaRoot()
                        + Constants.FILE_SEPARATOR
                        + tune.getPath()
                        + Constants.FILE_SEPARATOR
                        + tune.getFilename2());
                if (!file.exists()) {
                    log.error("File not exists: {}", file.getAbsolutePath());
                    continue;
                }
                final Long duration = EncodingUtil.getDurationInSecs(file);
                tune.setDuration(duration);
                tuneService.save(tune);
            }
        }
    }
}

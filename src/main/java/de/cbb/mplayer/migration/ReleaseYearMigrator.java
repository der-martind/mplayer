package de.cbb.mplayer.migration;

import de.cbb.mplayer.common.Constants;
import de.cbb.mplayer.discogs.service.DiscogsService;
import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tags.service.TagService;
import de.cbb.mplayer.tunes.domain.Tune;
import de.cbb.mplayer.tunes.service.TuneService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty("mplayer.tools.releaseyear-migration")
@RequiredArgsConstructor
@Slf4j
public class ReleaseYearMigrator implements CommandLineRunner {

    private final TuneService tuneService;
    private final TagService tagService;
    private final DiscogsService discogsService;

    @Override
    public void run(final String... args) {
        log.info("START release_year migration");
        List<Tune> list = tuneService.getAll();
        for (final Tune tune : list) {
            log.debug("Processing tune: {}", tune);
            if (!tune.isDiscogs()) continue;
            if (tune.getYear2() == 0 || tune.getYear2().equals(Constants.DEFAULT_YEAR)) {
                Integer year;
                try {
                    if (tune.getAlbum().equalsIgnoreCase(Constants.DEFAULT_ALBUM)) {
                        year = discogsService.getTrackReleaseYear(tune.getArtist(), tune.getTitle());
                    } else {
                        year = discogsService.getReleaseYear(tune.getArtist(), tune.getAlbum());
                    }
                } catch (Exception e) {
                    return;
                }
                if (year != 0) {
                    tune.setYear2(year);
                    updateDecateTag(tune);
                    tuneService.save(tune);
                } else {
                    tune.setDiscogs(false);
                    tuneService.save(tune);
                }
                try {
                    Thread.sleep(2000); // discogs rate limit
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        log.info("END release_year migration");
    }

    private void updateDecateTag(final Tune tune) {
        int year = tune.getYear2();
        if (year > 0 && year != Constants.DEFAULT_YEAR) {
            Tag tag = tagService.findDecadeTag(year);
            if (tag != null) {
                boolean present =
                        tune.getTags().stream().anyMatch(t -> t.getValue().equalsIgnoreCase(tag.getValue()));
                if (!present) tune.getTags().add(tag);
            }
        }
    }
}

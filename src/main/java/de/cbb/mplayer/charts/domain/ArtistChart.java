package de.cbb.mplayer.charts.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@Table
@RequiredArgsConstructor
@Data
public class ArtistChart implements Comparable<ArtistChart>, java.io.Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer cid;

    @NotNull
    @Column(unique = true)
    private String artist;

    @Column(columnDefinition = "INTEGER default 0")
    private Integer played = 0;

    public ArtistChart(final String artist) {
        this.artist = artist;
    }

    public ArtistChart(final String artist, final Integer played) {
        this(artist);
        this.played = played;
    }

    @Override
    public int compareTo(ArtistChart o) {
        if (o == null) {
            return -1;
        }
        return played.compareTo(o.getPlayed());
    }
}

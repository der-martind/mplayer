package de.cbb.mplayer.charts.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Entity
@Table(indexes = @Index(columnList = "played"))
@Data
public class Chart implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer cid;

    @NotNull
    private Integer tid;

    @NotNull
    private String label;

    @NotNull
    private LocalDate played;
}

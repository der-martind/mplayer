package de.cbb.mplayer.charts.repo;

import de.cbb.mplayer.charts.domain.ArtistChart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArtistChartRepository extends JpaRepository<ArtistChart, Long> {

    ArtistChart findByArtistIgnoreCase(final String artist);
}

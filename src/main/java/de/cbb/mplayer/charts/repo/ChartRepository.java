package de.cbb.mplayer.charts.repo;

import de.cbb.mplayer.charts.domain.Chart;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ChartRepository extends JpaRepository<Chart, Long> {

    @Query("select count(*),concat(t.artist, ' - ', t.title) from Chart c, Tune t where c.tid = t.tid"
            + " and c.played >= ?1 and c.played <= ?2 group by t.tid order by count(*) desc")
    List<Object[]> findTopTitles(LocalDate from, LocalDate to, Pageable pageable);

    @Query("select count(*),t.artist from Chart c, Tune t where c.tid = t.tid and c.played >= ?1 and"
            + " c.played <= ?2 group by t.artist order by count(*) desc")
    List<Object[]> findTopArtists(LocalDate from, LocalDate to, Pageable pageable);

    @Query("select count(*),concat(t.artist, ' - ', t.album) from Chart c, Tune t where c.tid = t.tid"
            + " and c.played >= ?1 and c.played <= ?2 and album != 'unknown' group by t.album order"
            + " by count(*) desc")
    List<Object[]> findTopAlbums(LocalDate from, LocalDate to, Pageable pageable);

    List<Chart> findByPlayed(LocalDate played);

    List<Chart> findByPlayedAndTid(LocalDate played, Integer tid);

    @Query("select DISTINCT(c.played) from Chart c ORDER BY played desc")
    List<LocalDate> findDistinctPlayedOrderByPlayed();

    @Query(
            value = "select artist from chart c join tune t on t.tid = c.tid where c.played in (select played"
                    + " from chart where tid in (select tid from tune where artist = ?1)  and artist !="
                    + " ?1 order by played) group by artist order by count(artist) desc LIMIT ?2",
            nativeQuery = true)
    List<String> findDistinctArtistsByPlayed(String artist, int limit);

    @Query(
            value = "SELECT COUNT(tag.value),tag.value FROM tune t JOIN tune_tag ON tune_tid = t.tid JOIN tag"
                    + " tag ON tag.id = tag_id JOIN chart c ON c.tid = t.tid WHERE tag.value LIKE"
                    + " 'Genre:%' AND c.played >= ?1 AND c.played <= ?2 GROUP BY tag.value ORDER BY"
                    + " count(tag.value) DESC LIMIT ?3",
            nativeQuery = true)
    List<Object[]> findTopGenres(LocalDate begin, LocalDate end, int limit);

    @Query("select count(*),t.artist from Chart c, Tune t where c.tid = t.tid group by t.artist order by"
            + " count(*) desc")
    List<Object[]> findArtistsOrderByCount();

    @Query("select count(*),t.album from Chart c, Tune t where c.tid = t.tid group by t.album order by"
            + " count(*) desc")
    List<Object[]> findAlbumsOrderByCount();

    @Query("select count(*),t.title from Chart c, Tune t where c.tid = t.tid group by t.title order by"
            + " count(*) desc")
    List<Object[]> findTitlesOrderByCount();

    @Query("select count(*) from Chart c where c.tid = ?1")
    Integer countByTid(Integer tid);

    @Query("select count(*) from Chart c join Tune t on t.tid = c.tid where t.album = ?2 and t.artist =" + " ?1")
    Integer countByArtistAndAlbum(String artist, String album);
}

package de.cbb.mplayer.charts.service;

import de.cbb.mplayer.charts.domain.ArtistChart;
import de.cbb.mplayer.charts.domain.Chart;
import de.cbb.mplayer.charts.repo.ArtistChartRepository;
import de.cbb.mplayer.charts.repo.ChartRepository;
import de.cbb.mplayer.common.Constants;
import de.cbb.mplayer.tunes.domain.Tune;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ChartService {

    private final ChartRepository chartRepo;
    private final ArtistChartRepository artistChartRepo;

    public void insertChart(final Tune tune) {
        final Chart chart = new Chart();
        chart.setTid(tune.getTid());
        chart.setPlayed(LocalDate.now());
        chart.setLabel(tune.toString());
        chartRepo.save(chart);

        insertArtistChart(tune);
        log.info("chart inserted: " + tune);
    }

    private void insertArtistChart(Tune tune) {
        if (tune.getArtist().equalsIgnoreCase(Constants.DEFAULT_ARTIST)) return;
        ArtistChart artistChart = artistChartRepo.findByArtistIgnoreCase(tune.getArtist());
        if (artistChart == null) {
            artistChart = new ArtistChart(tune.getArtist());
        }
        artistChart.setPlayed(artistChart.getPlayed() + 1);
        artistChartRepo.save(artistChart);
        log.debug("Artist played: {}", artistChart.getPlayed());
    }

    // TODO
    public void deleteChart(final LocalDate played, final Integer tid) {
        final List<Chart> charts = chartRepo.findByPlayedAndTid(played, tid);
        if (charts.isEmpty()) {
            log.error("deleteChart failed. No chart found with tid={} and played={}.", tid, played);
        } else {
            chartRepo.delete(charts.get(0)); // Delete first of n charts
            log.info("Chart deleted: {}", charts.get(0));
        }
    }

    public List<LocalDate> getDistinctDates() {
        return chartRepo.findDistinctPlayedOrderByPlayed();
    }

    public List<Chart> findByPlayed(final LocalDate played) {
        return chartRepo.findByPlayed(played);
    }

    public Integer findArtistPlayed(final String artist) {
        ArtistChart chart = artistChartRepo.findByArtistIgnoreCase(artist);
        return (chart == null) ? 0 : chart.getPlayed();
    }

    public Integer findAlbumPlayed(final String artist, final String album) {
        return chartRepo.countByArtistAndAlbum(artist, album);
    }

    public List<String> sortArtistsByPlayed(List<String> artists) {
        List<ArtistChart> ranked = new ArrayList<>();
        for (String artist : artists) {
            final Integer artistPlayed = findArtistPlayed(artist);
            ranked.add(new ArtistChart(artist, artistPlayed));
        }
        return ranked.stream()
                .sorted(Comparator.reverseOrder())
                .map(ArtistChart::getArtist)
                .toList();
    }

    public List<String> sortAlbumsByPlayed(final String artist, final List<String> albums) {
        return albums.stream()
                .sorted((a1, a2) -> findAlbumPlayed(artist, a1).compareTo(findAlbumPlayed(artist, a2)))
                .toList();
    }

    public List<Tune> sortTunesByPlayed(List<Tune> tunes) {
        return tunes.stream()
                .sorted((t1, t2) -> chartRepo.countByTid(t1.getTid()).compareTo(chartRepo.countByTid(t2.getTid())))
                .toList();
    }
}

package de.cbb.mplayer.charts.view;

import de.cbb.mplayer.common.ui.ctxmenu.DefaultActionMapFactory;
import de.cbb.mplayer.common.ui.trees.TreeCellImpl;
import de.cbb.mplayer.common.ui.trees.TreeItemTuneValue;
import de.cbb.mplayer.common.ui.trees.TreeItemValue;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.fxml.FXML;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TreeView;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class ChartView implements FxmlView<ChartViewModel> {

    @FXML
    private TreeView<TreeItemValue> treeView;

    @InjectViewModel
    private ChartViewModel viewModel;

    private final DefaultActionMapFactory actionMapFactory;

    public void initialize() {
        treeView.setRoot(viewModel.rootProperty().get());

        // bind viewmodel-root to treeview-root
        viewModel.rootProperty().addListener((observable, oldValue, newValue) -> treeView.setRoot(newValue));

        treeView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE); // TODO: multi?

        treeView.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> {
            log.debug("Tune selected: {}", newValue);
            if (newValue.getValue() instanceof TreeItemTuneValue tuneValue) {
                viewModel.selectTune(tuneValue.getTune());
            }
        });

        viewModel.selectedItemProperty().bind(treeView.getSelectionModel().selectedItemProperty());
        treeView.setCellFactory(p -> new TreeCellImpl(viewModel, actionMapFactory.createActionMap()));

        treeView.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() == 2) {
                viewModel.enqueueSelectedTunes();
            }
        });
    }
}

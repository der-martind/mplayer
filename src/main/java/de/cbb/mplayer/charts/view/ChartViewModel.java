package de.cbb.mplayer.charts.view;

import com.google.common.eventbus.EventBus;
import de.cbb.mplayer.charts.domain.Chart;
import de.cbb.mplayer.charts.service.ChartService;
import de.cbb.mplayer.common.Constants;
import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import de.cbb.mplayer.common.ui.trees.*;
import de.cbb.mplayer.perspectives.events.TuneSelectedEvent;
import de.cbb.mplayer.session.service.EnqueueingService;
import de.cbb.mplayer.tags.service.TagService;
import de.cbb.mplayer.tunes.domain.Tune;
import de.cbb.mplayer.tunes.service.TuneService;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.TreeItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ChartViewModel extends AbstractTreeViewModel {

    private static final String ROOT_NAME = "<root>";

    private final ObjectProperty<TreeItem<TreeItemValue>> rootProperty = new SimpleObjectProperty<>();
    private final ObjectProperty<TreeItem<TreeItemValue>> selectedItemProperty = new SimpleObjectProperty<>();

    private final ChartService chartService;

    public ChartViewModel(
            final TagService tagService,
            final TuneService tuneService,
            final EventBus eventBus,
            final ChartService chartService,
            final EnqueueingService enqueueingService,
            final DefaultErrorHandler defaultErrorHandler) {
        super(tagService, tuneService, eventBus, enqueueingService, defaultErrorHandler);
        this.chartService = chartService;
    }

    public void initialize() {
        final TreeItem<TreeItemValue> parent = new ExpandableParentTreeItem(ROOT_NAME);
        parent.expandedProperty().addListener((observable, wasExpanded, isExpanded) -> {
            if (parent.getChildren().isEmpty()) {
                populateParentTreeItem(parent);
            }
        });
        rootProperty.set(parent);
    }

    ObjectProperty<TreeItem<TreeItemValue>> rootProperty() {
        return rootProperty;
    }

    ObjectProperty<TreeItem<TreeItemValue>> selectedItemProperty() {
        return selectedItemProperty;
    }

    @Override
    public List<Tune> getSelectedTunes() {
        final TreeItem<TreeItemValue> selectedItem = selectedItemProperty.get();
        if (selectedItem.getValue() instanceof TreeItemTuneValue tuneValue) {
            return Collections.singletonList(tuneValue.getTune());
        } else {
            return Collections.emptyList(); // TODO
        }
    }

    @Override
    protected TreeItem<TreeItemValue> getRoot() {
        return rootProperty.get();
    }

    public void selectTune(final Tune newValue) {
        eventBus.post(new TuneSelectedEvent(newValue));
    }

    public void expandNode(final TreeItem<TreeItemValue> treeItem) {
        if (treeItem.getChildren().isEmpty()) {
            populateParentTreeItem(treeItem);
        }
    }

    public void appendChart(final Tune tune) {
        if (rootProperty.get().getChildren().isEmpty()) {
            return;
        }
        final TreeItem<TreeItemValue> todayItem = getTodayItem();
        todayItem.getChildren().add(new TreeItem<>(new TreeItemTuneValue(tune)));
    }

    private void populateParentTreeItem(final TreeItem<TreeItemValue> item) {
        final List<LocalDate> chartDates = chartService.getDistinctDates();
        item.getChildren()
                .addAll(chartDates.stream()
                        .map(date -> new ExpandableParentTreeItem(Constants.DATE_FORMAT.format(date)))
                        .peek(this::addExpandListener)
                        .toList());
    }

    private void addExpandListener(final TreeItem<TreeItemValue> item1) {
        item1.expandedProperty().addListener((observable, wasExpanded, isExpanded) -> {
            if (item1.getChildren().isEmpty()) {
                populateParentDateItem(item1);
            }
        });
    }

    private void populateParentDateItem(final TreeItem<TreeItemValue> item) {
        final List<Chart> charts;
        charts = chartService.findByPlayed(
                LocalDate.parse(((TreeItemTitleValue) item.getValue()).getTitle(), Constants.DATE_FORMAT));
        item.getChildren()
                .addAll(charts.stream()
                        .map(chart -> findTune(chart.getTid()))
                        .filter(Optional::isPresent)
                        .map(tune -> new TreeItem<TreeItemValue>(new TreeItemTuneValue(tune.get())))
                        .toList());
    }

    private Optional<Tune> findTune(final Integer tid) {
        return tuneService.getById(tid);
    }

    private TreeItem<TreeItemValue> getTodayItem() {
        final String today = Constants.DATE_FORMAT.format(LocalDate.now());
        for (final TreeItem<TreeItemValue> dateItem : rootProperty.getValue().getChildren()) {
            if (((TreeItemTitleValue) dateItem.getValue()).getTitle().equals(today)) {
                return dateItem;
            }
        }
        final TreeItem<TreeItemValue> todayItem = new ExpandableParentTreeItem(today);
        rootProperty.get().getChildren().add(0, todayItem);
        return todayItem;
    }
}

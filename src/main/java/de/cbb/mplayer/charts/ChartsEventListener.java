package de.cbb.mplayer.charts;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import de.cbb.mplayer.charts.service.ChartService;
import de.cbb.mplayer.charts.view.ChartViewModel;
import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import de.cbb.mplayer.common.listeners.DefaultListener;
import de.cbb.mplayer.detail.events.TuneUpdatedEvent;
import de.cbb.mplayer.perspectives.events.ExpandNodeEvent;
import de.cbb.mplayer.player.events.PlayTuneEvent;
import org.springframework.stereotype.Component;

@Component
public class ChartsEventListener extends DefaultListener {

    private final ChartService chartService;
    private final ChartViewModel viewModel;

    public ChartsEventListener(
            final EventBus eventBus,
            final DefaultErrorHandler defaultErrorHandler,
            final ChartService chartService,
            final ChartViewModel viewModel) {
        super(eventBus, defaultErrorHandler);
        this.chartService = chartService;
        this.viewModel = viewModel;
        registerForDefaultEvents();
    }

    @Subscribe
    public void handlePlayTuneEvent(final PlayTuneEvent ev) {
        try {
            chartService.insertChart(ev.getTune());
            viewModel.appendChart(ev.getTune());
        } catch (final Exception ex) {
            defaultErrorHandler.handle("handlePlayTuneEvent", ex);
        }
    }

    @Subscribe
    public void handleExpandNodeEvent(final ExpandNodeEvent event) {
        try {
            viewModel.expandNode(event.getTreeItem());
        } catch (final Exception ex) {
            defaultErrorHandler.handle("handleExpandNodeEvent", ex);
        }
    }

    @Subscribe
    public void handleTuneUpdatedEvent(final TuneUpdatedEvent event) {
        try {
            viewModel.update(event.getTune());
        } catch (final Exception ex) {
            defaultErrorHandler.handle("handleTuneUpdatedEvent", ex);
        }
    }
}

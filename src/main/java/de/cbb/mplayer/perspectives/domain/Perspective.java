package de.cbb.mplayer.perspectives.domain;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Perspective {

    private final String name;
    private final byte[] image;
}

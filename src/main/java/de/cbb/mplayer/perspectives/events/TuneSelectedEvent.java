package de.cbb.mplayer.perspectives.events;

import de.cbb.mplayer.tunes.domain.Tune;

public class TuneSelectedEvent extends AbstractTuneEvent {

    public TuneSelectedEvent(final Tune tune) {
        super(tune);
    }
}

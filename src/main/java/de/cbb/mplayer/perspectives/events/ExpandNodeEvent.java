package de.cbb.mplayer.perspectives.events;

import de.cbb.mplayer.common.ui.trees.TreeItemValue;
import javafx.scene.control.TreeItem;
import lombok.Data;

@Data
public class ExpandNodeEvent {

    private final TreeItem<TreeItemValue> treeItem;

    public ExpandNodeEvent(final TreeItem<TreeItemValue> treeItem) {
        this.treeItem = treeItem;
    }
}

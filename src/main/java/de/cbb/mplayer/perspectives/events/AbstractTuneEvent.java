package de.cbb.mplayer.perspectives.events;

import de.cbb.mplayer.tunes.domain.Tune;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public abstract class AbstractTuneEvent {

    protected final Tune tune;
}

package de.cbb.mplayer.perspectives.artists.view;

import de.cbb.mplayer.common.ui.PositioningHelper;
import de.cbb.mplayer.common.ui.controls.Chip;
import de.cbb.mplayer.common.ui.controls.JumperBar;
import de.cbb.mplayer.common.ui.ctxmenu.DefaultActionMapFactory;
import de.cbb.mplayer.common.ui.trees.TreeCellImpl;
import de.cbb.mplayer.common.ui.trees.TreeItemTuneValue;
import de.cbb.mplayer.common.ui.trees.TreeItemValue;
import de.cbb.mplayer.perspectives.artists.Sorting;
import de.cbb.mplayer.search.domain.SearchFilter;
import de.cbb.mplayer.search.view.AttributeSearchValueInputDialog;
import de.cbb.mplayer.tags.domain.Tag;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import java.util.function.Consumer;
import javafx.beans.value.ObservableValue;
import javafx.collections.SetChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class ArtistView implements FxmlView<ArtistViewModel> {

    @FXML
    private TreeView<TreeItemValue> treeView;

    @FXML
    private HBox hbFilters;

    @FXML
    private ToggleButton tgSortAlpha;

    @FXML
    private ToggleButton tgSortAge;

    @FXML
    private ToggleButton tgSortPlayed;

    @FXML
    private Pane pnJumper;

    @InjectViewModel
    private ArtistViewModel viewModel;

    private final AttributeSearchValueInputDialog attributeSearchValueInputDialog;
    private final DefaultActionMapFactory actionMapFactory;

    public void initialize() {

        final JumperBar jumpers = new JumperBar(this::onJump);
        pnJumper.getChildren().add(jumpers);

        treeView.setRoot(viewModel.getRootProperty().get());

        // bind viewmodel-root to treeview-root
        viewModel.getRootProperty().addListener((observable, oldValue, newValue) -> treeView.setRoot(newValue));

        treeView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        treeView.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> {
            log.debug("Node selected: {}", newValue);
            if (newValue != null && newValue.getValue() instanceof TreeItemTuneValue tuneValue) {
                viewModel.selectTune(tuneValue.getTune());
            }
        });

        viewModel.getSelectedItemProperty().bind(treeView.getSelectionModel().selectedItemProperty());
        treeView.setCellFactory(p -> new TreeCellImpl(viewModel, actionMapFactory.createActionMap()));

        treeView.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() == 2) {
                viewModel.enqueueSelectedTunes();
            }
        });

        treeView.getRoot().setExpanded(true);

        hbFilters.setOnMouseClicked(mouseEvent -> {
            final SearchFilter<?> filter = attributeSearchValueInputDialog.display(
                    viewModel.getTags(), PositioningHelper.getPointBelowParent(hbFilters, 15, 25));
            if (filter.getValue() != null) {
                viewModel.getSearchFilters().add(new SearchFilter<>(filter.getValue()));
            }
        });

        viewModel.getSearchFilters().addListener((SetChangeListener<SearchFilter<?>>) change -> populateFilterPane());

        final ToggleGroup tgSorting = new ToggleGroup();
        tgSortAlpha.setToggleGroup(tgSorting);
        tgSortAge.setToggleGroup(tgSorting);
        tgSortPlayed.setToggleGroup(tgSorting);
        if (viewModel.getSortingProperty().get().equals(Sorting.ALPHABETICAL)) tgSorting.selectToggle(tgSortAlpha);
        else if (viewModel.getSortingProperty().get().equals(Sorting.AGE)) tgSorting.selectToggle(tgSortAge);
        else tgSorting.selectToggle(tgSortPlayed);

        tgSorting
                .selectedToggleProperty()
                .addListener((final ObservableValue<? extends Toggle> ov, final Toggle t, final Toggle t1) -> {
                    if (t1 == null) { // selected toggle selected again
                        return;
                    }
                    final ToggleButton chk = (ToggleButton) t1.getToggleGroup().getSelectedToggle();
                    if (chk == tgSortAlpha) {
                        viewModel.getSortingProperty().set(Sorting.ALPHABETICAL);
                    }
                    if (chk == tgSortAge) {
                        viewModel.getSortingProperty().set(Sorting.AGE);
                    }
                    if (chk == tgSortPlayed) {
                        viewModel.getSortingProperty().set(Sorting.PLAYED);
                    }
                });
    }

    private void onJump(final ActionEvent event) {
        final Consumer<String> scrollTo = this::scrollTo;
        scrollTo.accept(((Button) event.getSource()).getText());
    }

    private void scrollTo(final String letter) {
        final TreeItem<TreeItemValue> root = treeView.getRoot();
        for (final TreeItem<TreeItemValue> item : root.getChildren()) {
            if ((item.getValue().toString()).toLowerCase().startsWith(letter.toLowerCase())) {
                final int row = treeView.getRow(item);
                treeView.scrollTo(row);
                treeView.getSelectionModel().select(row);
                return;
            }
        }
    }

    private void populateFilterPane() {
        hbFilters.getChildren().clear();
        for (final SearchFilter<?> filter : viewModel.getSearchFilters()) {
            final Object value = filter.getValue();
            Chip chip = null;
            if (value instanceof Tag tag) {
                chip = new Chip(tag.getValue(), tag.getColor(), actionEvent -> viewModel
                        .getSearchFilters()
                        .remove(filter));
            }
            hbFilters.getChildren().add(chip);
        }
    }
}

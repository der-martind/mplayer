package de.cbb.mplayer.perspectives.artists.view;

import com.google.common.eventbus.EventBus;
import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import de.cbb.mplayer.common.ui.trees.*;
import de.cbb.mplayer.perspectives.artists.Sorting;
import de.cbb.mplayer.perspectives.events.TuneSelectedEvent;
import de.cbb.mplayer.search.domain.SearchFilter;
import de.cbb.mplayer.session.service.EnqueueingService;
import de.cbb.mplayer.tags.service.TagService;
import de.cbb.mplayer.tunes.domain.Tune;
import de.cbb.mplayer.tunes.service.TuneService;
import de.cbb.mplayer.tunes.service.sorting.TuneReadingOrderStrategyHolder;
import java.util.Collections;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;
import javafx.collections.SetChangeListener;
import javafx.scene.control.TreeItem;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ArtistViewModel extends AbstractTreeViewModel {

    private static final String ROOT_NAME = "<root>";

    protected final TuneReadingOrderStrategyHolder tuneReadingOrderStrategyHolder;

    @Getter
    private final ObjectProperty<TreeItem<TreeItemValue>> rootProperty = new SimpleObjectProperty<>();

    @Getter
    private final ObjectProperty<TreeItem<TreeItemValue>> selectedItemProperty = new SimpleObjectProperty<>();

    @Getter
    private final ObservableSet<SearchFilter<?>> searchFilters = FXCollections.observableSet();

    @Getter
    private final ObjectProperty<Sorting> sortingProperty = new SimpleObjectProperty<>(Sorting.ALPHABETICAL);

    public ArtistViewModel(
            final TagService tagService,
            final EventBus eventBus,
            final TuneService tuneService,
            final EnqueueingService enqueueingService,
            final DefaultErrorHandler defaultErrorHandler,
            TuneReadingOrderStrategyHolder tuneReadingOrderStrategyHolder) {
        super(tagService, tuneService, eventBus, enqueueingService, defaultErrorHandler);
        this.tuneReadingOrderStrategyHolder = tuneReadingOrderStrategyHolder;
    }

    public void initialize() {
        updateRoot();
        searchFilters.addListener((SetChangeListener<? super SearchFilter<?>>) change -> updateRoot());
        sortingProperty.addListener((observableValue, sorting, newSorting) -> updateRoot());
    }

    @Override
    public List<Tune> getSelectedTunes() {
        final TreeItem<TreeItemValue> selectedItem = selectedItemProperty.get();
        if (selectedItem.getValue() instanceof TreeItemTuneValue tuneValue) {
            return Collections.singletonList(tuneValue.getTune());
        } else if (selectedItem.getParent() == null) { // root
            return tuneService.getByFilters(searchFilters);
        } else if (selectedItem.getParent().getParent() == null) { // artist
            return tuneService.getByArtistAndFilters(
                    ((TreeItemTitleValue) selectedItem.getValue()).getTitle(), searchFilters);
        } else { // album
            return tuneService.getByArtistAndAlbumAndFilters(
                    ((TreeItemTitleValue) selectedItem.getParent().getValue()).getTitle(),
                    ((TreeItemTitleValue) selectedItem.getValue()).getTitle(),
                    searchFilters);
        }
    }

    @Override
    protected TreeItem<TreeItemValue> getRoot() {
        return rootProperty.get();
    }

    public void selectTune(final Tune newValue) {
        eventBus.post(new TuneSelectedEvent(newValue));
    }

    private void updateRoot() {
        if (rootProperty.get() != null) {
            rootProperty.get().getChildren().clear();
        }
        final var parent = new ExpandableParentTreeItem(ROOT_NAME);
        parent.expandedProperty().addListener((observable, wasExpanded, isExpanded) -> {
            if (parent.getChildren().isEmpty()) {
                populateRootItem(parent);
            }
        });
        rootProperty.set(parent);
        parent.setExpanded(true);
    }

    private void populateRootItem(final ExpandableParentTreeItem parent) {
        List<String> artists = tuneReadingOrderStrategyHolder
                .getStrategy(sortingProperty.get())
                .getDistinctArtistsByFilter(searchFilters);
        for (final String artist : artists) {
            final var item = new ExpandableParentTreeItem(artist);
            item.expandedProperty().addListener(createArtistExpandListener(item));
            parent.getChildren().add(item);
        }
    }

    private ChangeListener<? super Boolean> createArtistExpandListener(final TreeItem<TreeItemValue> item) {
        return (ChangeListener<Boolean>) (observable, oldValue, newValue) -> {
            if (item.getChildren().isEmpty()) {
                List<String> albums = tuneReadingOrderStrategyHolder
                        .getStrategy(sortingProperty.get())
                        .getDistinctAlbumsByArtistAndFilters(
                                ((TreeItemTitleValue) item.getValue()).getTitle(), searchFilters);
                if (!albums.isEmpty()) {
                    for (final String album : albums) {
                        final TreeItem<TreeItemValue> albumItem = new ExpandableParentTreeItem(album);
                        albumItem.expandedProperty().addListener(createAlbumExpandListener(albumItem));
                        item.getChildren().add(albumItem);
                    }
                }
            }
        };
    }

    private ChangeListener<? super Boolean> createAlbumExpandListener(final TreeItem<TreeItemValue> item) {
        return (ChangeListener<Boolean>) (observable, oldValue, newValue) -> {
            if (item.getChildren().isEmpty()) {
                List<Tune> tunes = tuneReadingOrderStrategyHolder
                        .getStrategy(sortingProperty.get())
                        .getByArtistAndAlbumAndFilters(
                                ((TreeItemTitleValue) item.getParent().getValue()).getTitle(),
                                ((TreeItemTitleValue) item.getValue()).getTitle(),
                                searchFilters);
                if (!tunes.isEmpty()) {
                    for (final Tune tune : tunes) {
                        final TreeItem<TreeItemValue> tuneItem = new TreeItem<>(new TreeItemTuneValue(tune));
                        item.getChildren().add(tuneItem);
                    }
                }
            }
        };
    }
}

package de.cbb.mplayer.perspectives.artists;

public enum Sorting {
    ALPHABETICAL,
    AGE,
    PLAYED
}

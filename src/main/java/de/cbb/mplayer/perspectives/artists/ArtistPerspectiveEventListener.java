package de.cbb.mplayer.perspectives.artists;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import de.cbb.mplayer.common.listeners.DefaultListener;
import de.cbb.mplayer.detail.events.TuneUpdatedEvent;
import de.cbb.mplayer.perspectives.artists.view.ArtistViewModel;
import org.springframework.stereotype.Component;

@Component
public class ArtistPerspectiveEventListener extends DefaultListener {

    private final ArtistViewModel viewModel;

    public ArtistPerspectiveEventListener(
            final EventBus eventBus, final DefaultErrorHandler defaultErrorHandler, final ArtistViewModel viewModel) {
        super(eventBus, defaultErrorHandler);
        this.viewModel = viewModel;
        registerForDefaultEvents();
    }

    @Subscribe
    public void handleTuneUpdatedEvent(final TuneUpdatedEvent event) {
        try {
            viewModel.update(event.getTune());
        } catch (final Exception ex) {
            defaultErrorHandler.handle("handleTuneUpdatedEvent", ex);
        }
    }
}

package de.cbb.mplayer.perspectives.files.view;

import de.cbb.mplayer.common.ui.controls.JumperBar;
import de.cbb.mplayer.common.ui.ctxmenu.DefaultActionMapFactory;
import de.cbb.mplayer.common.ui.trees.TreeCellImpl;
import de.cbb.mplayer.common.ui.trees.TreeItemTuneValue;
import de.cbb.mplayer.common.ui.trees.TreeItemValue;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import java.util.function.Consumer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class FileView implements FxmlView<FileViewModel> {

    @FXML
    private TreeView<TreeItemValue> treeView;

    @FXML
    private Pane pnJumper;

    @InjectViewModel
    private FileViewModel viewModel;

    private final DefaultActionMapFactory actionMapFactory;

    public void initialize() {

        final JumperBar jumpers = new JumperBar(this::onJump);
        pnJumper.getChildren().add(jumpers);

        treeView.setRoot(viewModel.rootProperty().get());

        // bind viewmodel-root to treeview-root
        viewModel.rootProperty().addListener((observable, oldValue, newValue) -> treeView.setRoot(newValue));

        treeView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        treeView.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> {
            log.debug("Tune selected: {}", newValue);
            if (newValue.getValue() instanceof TreeItemTuneValue tuneValue) {
                viewModel.selectTune(tuneValue.getTune());
            }
        });

        viewModel.selectedItemProperty().bind(treeView.getSelectionModel().selectedItemProperty());
        treeView.setCellFactory(p -> new TreeCellImpl(viewModel, actionMapFactory.createActionMap()));

        treeView.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() == 2) {
                viewModel.enqueueSelectedTunes();
            }
        });
    }

    private void onJump(final ActionEvent event) {
        final Consumer<String> scrollTo = this::scrollTo;
        scrollTo.accept(((Button) event.getSource()).getText());
    }

    private void scrollTo(final String letter) {
        final TreeItem<TreeItemValue> root = treeView.getRoot();
        for (final TreeItem<TreeItemValue> item : root.getChildren()) {
            if (item.getValue().toString().toLowerCase().startsWith(letter.toLowerCase())) {
                final int row = treeView.getRow(item);
                treeView.scrollTo(row);
                treeView.getSelectionModel().select(row);
                return;
            }
        }
    }
}

package de.cbb.mplayer.perspectives.files;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import de.cbb.mplayer.common.listeners.DefaultListener;
import de.cbb.mplayer.detail.events.TuneUpdatedEvent;
import de.cbb.mplayer.perspectives.files.view.FileViewModel;
import org.springframework.stereotype.Component;

@Component
public class FilePerspectiveEventsListener extends DefaultListener {

    private final FileViewModel viewModel;

    public FilePerspectiveEventsListener(
            EventBus eventBus, DefaultErrorHandler defaultErrorHandler, final FileViewModel viewModel) {
        super(eventBus, defaultErrorHandler);
        this.viewModel = viewModel;
        registerForDefaultEvents();
    }

    @Subscribe
    public void handleTuneUpdatedEvent(final TuneUpdatedEvent event) {
        try {
            viewModel.update(event.getTune());
        } catch (final Exception ex) {
            defaultErrorHandler.handle("handleTuneUpdatedEvent", ex);
        }
    }
}

package de.cbb.mplayer.perspectives.files.view;

import com.google.common.eventbus.EventBus;
import de.cbb.mplayer.common.Constants;
import de.cbb.mplayer.common.converters.PathConverter;
import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import de.cbb.mplayer.common.ui.trees.*;
import de.cbb.mplayer.config.ConfigProperties;
import de.cbb.mplayer.perspectives.events.TuneSelectedEvent;
import de.cbb.mplayer.session.service.EnqueueingService;
import de.cbb.mplayer.tags.service.TagService;
import de.cbb.mplayer.tunes.domain.Tune;
import de.cbb.mplayer.tunes.service.TuneService;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.TreeItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FileViewModel extends AbstractTreeViewModel {

    private static final String ROOT_NAME = "<root>";

    private final ObjectProperty<TreeItem<TreeItemValue>> rootProperty = new SimpleObjectProperty<>();
    private final ObjectProperty<TreeItem<TreeItemValue>> selectedItemProperty = new SimpleObjectProperty<>();

    private final ConfigProperties properties;
    private final PathConverter pathConverter;

    public FileViewModel(
            final TagService tagService,
            final EventBus eventBus,
            final TuneService tuneService,
            final ConfigProperties properties,
            final PathConverter pathConverter,
            final EnqueueingService enqueueingService,
            final DefaultErrorHandler defaultErrorHandler) {
        super(tagService, tuneService, eventBus, enqueueingService, defaultErrorHandler);
        this.properties = properties;
        this.pathConverter = pathConverter;
    }

    public void initialize() {
        final TreeItem<TreeItemValue> parent = new ExpandableParentTreeItem(ROOT_NAME);
        parent.expandedProperty().addListener((observable, wasExpanded, isExpanded) -> {
            if (parent.getChildren().isEmpty()) {
                populateParentTreeItem(parent, properties.getMediaRoot());
            }
        });
        rootProperty.set(parent);
    }

    ObjectProperty<TreeItem<TreeItemValue>> rootProperty() {
        return rootProperty;
    }

    ObjectProperty<TreeItem<TreeItemValue>> selectedItemProperty() {
        return selectedItemProperty;
    }

    @Override
    public List<Tune> getSelectedTunes() {
        final TreeItem<TreeItemValue> selectedItem = selectedItemProperty.get();
        if (selectedItem.getValue() instanceof TreeItemTuneValue tuneValue) {
            return Collections.singletonList(tuneValue.getTune());
        } else {
            final String relativeParentPath = pathConverter.toRelativePath(buildParentPath(selectedItem));
            return tuneService.getByPathStartingWith(relativeParentPath);
        }
    }

    @Override
    protected TreeItem<TreeItemValue> getRoot() {
        return rootProperty.get();
    }

    public void selectTune(final Tune newValue) {
        eventBus.post(new TuneSelectedEvent(newValue));
    }

    private void populateParentTreeItem(final TreeItem<TreeItemValue> parent, final String parentPath) {
        final File[] files = new File(parentPath).listFiles();
        if (files == null) {
            return;
        }
        Arrays.sort(files);
        for (final File file : files) {
            final TreeItem<TreeItemValue> item;
            if (file.isDirectory()) {
                item = createParentItem(file);
            } else {
                item = createLeafItem(file);
            }
            if (item != null) {
                parent.getChildren().add(item);
            }
        }
    }

    private TreeItem<TreeItemValue> createLeafItem(final File file) {
        final String relativeParentPath = pathConverter.toRelativeParentPath(file.getAbsolutePath());
        final Optional<Tune> tune = tuneService.getByPathAndFilename2(relativeParentPath, file.getName());
        if (tune.isPresent()) {
            return new TreeItem<>(new TreeItemTuneValue(tune.get()));
        } else {
            log.warn("No file found with path: " + relativeParentPath + Constants.FILE_SEPARATOR + file.getName());
            return null;
        }
    }

    private TreeItem<TreeItemValue> createParentItem(final File file) {
        final TreeItem<TreeItemValue> item = new ExpandableParentTreeItem(file.getName());
        addExpandListener(item);
        return item;
    }

    private void addExpandListener(final TreeItem<TreeItemValue> item) {
        item.expandedProperty().addListener((observable, wasExpanded, isExpanded) -> {
            if (item.getChildren().isEmpty()) {
                final String path = buildParentPath(item);
                populateParentTreeItem(item, path);
            }
        });
    }

    private String buildParentPath(final TreeItem<TreeItemValue> parent) {
        if (parent.getParent().getParent() == null) { // above root-item
            return properties.getMediaRoot() + Constants.FILE_SEPARATOR + parent.getValue();
        } else {
            return buildParentPath(parent.getParent()) + Constants.FILE_SEPARATOR + parent.getValue();
        }
    }
}

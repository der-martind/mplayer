package de.cbb.mplayer.external;

import com.google.common.base.Strings;
import de.cbb.mplayer.external.urls.InfoUrlProvider;
import de.cbb.mplayer.external.urls.LyricsUrlProvider;
import de.cbb.mplayer.external.urls.VideoUrlProvider;
import de.cbb.mplayer.tunes.domain.Tune;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ExternalResourceService {

    private final BrowserService browserService;
    private final LyricsUrlProvider lyricsUrlProvider;
    private final VideoUrlProvider videoUrlProvider;
    private final InfoUrlProvider infoUrlProvider;

    public void openLyrics(final Tune tune) {
        if (!Strings.isNullOrEmpty(tune.getLyricsUrl())) {
            browserService.open(tune.getLyricsUrl());
        } else {
            final String url = lyricsUrlProvider.getUrl(tune);
            browserService.open(url);
        }
    }

    public void openTabs(final Tune tune) {
        if (!Strings.isNullOrEmpty(tune.getTabsUrl())) {
            browserService.open(tune.getTabsUrl());
        }
    }

    public void openMedia(final Tune tune) {
        if (!Strings.isNullOrEmpty(tune.getMediaUrl())) {
            browserService.open(tune.getMediaUrl());
        } else {
            browserService.open(videoUrlProvider.getUrl(tune));
        }
    }

    public void searchMedia(String query) {
        browserService.open(videoUrlProvider.getUrl(query));
    }

    public void searchInfo(String query) {
        browserService.open(infoUrlProvider.getUrl(query));
    }
}

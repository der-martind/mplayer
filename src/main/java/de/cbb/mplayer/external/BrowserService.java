package de.cbb.mplayer.external;

import com.sun.javafx.application.HostServicesDelegate;
import javafx.application.Application;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class BrowserService {

    private final Application application;

    public void open(final String url) {
        try {
            final HostServicesDelegate hostServices = HostServicesDelegate.getInstance(application);
            hostServices.showDocument(url);
        } catch (final Exception e) {
            log.warn("Opening url failed", e);
            throw new RuntimeException("Opening url failed: " + e);
        }
    }
}

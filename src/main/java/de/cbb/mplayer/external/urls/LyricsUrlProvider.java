package de.cbb.mplayer.external.urls;

import de.cbb.mplayer.config.ConfigProperties;
import de.cbb.mplayer.tunes.domain.Tune;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class LyricsUrlProvider {

    private final ConfigProperties properties;

    public String getUrl(final Tune tune) {
        return properties.getLyricsProvider()
                + "/"
                + removeMyIllegalCharacters(UrlBuilder.removeIllegalChars(tune.getArtist()))
                + "/"
                + removeMyIllegalCharacters(UrlBuilder.removeIllegalChars(tune.getTitle()))
                + ".html";
    }

    /*
     * Remove chars that are illegal in azlyrics url
     */
    private String removeMyIllegalCharacters(final String string) {
        return string.replaceAll(" ", "")
                .replaceAll("_", "")
                .replaceAll("\\.", "")
                .replaceAll("-", "");
    }
}

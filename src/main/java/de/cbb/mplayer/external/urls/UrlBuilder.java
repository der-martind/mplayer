package de.cbb.mplayer.external.urls;

public class UrlBuilder {

    public static String removeIllegalChars(final String string) {
        return string.toLowerCase()
                .trim()
                .replaceAll("\\+", "")
                .replaceAll("&", "")
                .replaceAll("ä", "ae")
                .replaceAll("ö", "oe")
                .replaceAll("ü", "ue")
                .replaceAll("\\(", "")
                .replaceAll("\\)", "")
                .replaceAll("´", "")
                .replaceAll("`", "")
                .replaceAll("'", "");
    }
}

package de.cbb.mplayer.external.urls;

import de.cbb.mplayer.config.ConfigProperties;
import de.cbb.mplayer.tunes.domain.Tune;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class VideoUrlProvider {

    private final ConfigProperties properties;

    public String getUrl(final Tune tune) {
        // https://www.youtube.com/results?search_query=move+your+body+my+darkest+days;
        return properties.getVideoProvider()
                + UrlBuilder.removeIllegalChars(tune.getArtist()).replaceAll(" ", "+")
                + "+"
                + UrlBuilder.removeIllegalChars(tune.getTitle()).replaceAll(" ", "+");
    }

    public String getUrl(final String query) {
        // https://www.youtube.com/results?search_query=darkest+days;
        return properties.getVideoProvider()
                + UrlBuilder.removeIllegalChars(query).replaceAll(" ", "+");
    }
}

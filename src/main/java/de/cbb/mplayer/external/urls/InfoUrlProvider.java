package de.cbb.mplayer.external.urls;

import de.cbb.mplayer.config.ConfigProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class InfoUrlProvider {

    private final ConfigProperties properties;

    public String getUrl(final String query) {
        return properties.getInfoProvider()
                + UrlBuilder.removeIllegalChars(query).replaceAll(" ", "+");
    }
}

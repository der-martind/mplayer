package de.cbb.mplayer.recommendations.view;

import de.cbb.mplayer.recommendations.domain.RecommendationItem;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.Callback;

public class RecommendationItemCellFactory
        implements Callback<ListView<RecommendationItem>, ListCell<RecommendationItem>> {

    private static final String IMG_INSTOCK = "/img/database-check-outline-custom-gray.png";

    @Override
    public ListCell<RecommendationItem> call(ListView<RecommendationItem> param) {
        return new ListCell<>() {
            @Override
            public void updateItem(RecommendationItem item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                if (empty || item == null) {
                    setGraphic(null);
                } else {
                    HBox node = new HBox();
                    node.setAlignment(Pos.CENTER_LEFT);
                    node.setSpacing(5.0);
                    node.getChildren().add(new Label(item.getArtist()));
                    if (item.isShowIcon()) {
                        node.getChildren().add(createImageView());
                    }
                    setGraphic(node);
                }
            }
        };
    }

    private Node createImageView() {
        ImageView imageView = new ImageView(IMG_INSTOCK);
        imageView.setFitHeight(16);
        imageView.setFitWidth(16);
        return imageView;
    }
}

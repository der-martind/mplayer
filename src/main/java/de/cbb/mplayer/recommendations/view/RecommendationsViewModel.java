package de.cbb.mplayer.recommendations.view;

import de.cbb.mplayer.external.ExternalResourceService;
import de.cbb.mplayer.recommendations.domain.RecommendationItem;
import de.cbb.mplayer.recommendations.domain.RecommendationMode;
import de.cbb.mplayer.recommendations.service.RecommendationService;
import de.cbb.mplayer.tunes.domain.Tune;
import de.cbb.mplayer.tunes.service.TuneService;
import de.saxsys.mvvmfx.ViewModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class RecommendationsViewModel implements ViewModel {

    private final RecommendationService recommService;
    private final ExternalResourceService externalResourceService;
    private final TuneService tuneService;

    @Getter
    private final ObjectProperty<RecommendationMode> recommendationModeProperty =
            new SimpleObjectProperty<>(RecommendationMode.RECOMM_SESSION);

    @Getter
    private final ObservableList<RecommendationItem> recommendations = FXCollections.observableArrayList();

    @Getter
    private final ObjectProperty<Tune> currentTuneProperty = new SimpleObjectProperty<>();

    public void initialize() {
        recommendationModeProperty.addListener(
                (observableValue, recommendationMode, newRecommendationMode) -> updateRecommendations());
        currentTuneProperty.addListener((observableValue, tune, t1) -> updateRecommendations());
    }

    public void searchArtist(final String artist) {
        externalResourceService.searchInfo(artist);
    }

    private void startSessionRecommendationThread() {
        new Thread(() -> {
                    final List<String> list = recommService.getSessionRecommendations(
                            currentTuneProperty.get().getArtist());
                    if (list.isEmpty()) {
                        log.debug("No session recommendations");
                    } else {
                        Platform.runLater(() -> recommendations.addAll(list.stream()
                                .map(a -> new RecommendationItem(false, a))
                                .toList()));
                    }
                })
                .start();
    }

    private void startCommunityRecommendationThread() {
        new Thread(() -> {
                    final List<String> list = recommService.getCommunityRecommendations(
                            currentTuneProperty.get().getArtist());
                    List<RecommendationItem> items = new ArrayList<>();
                    for (String artist : list) {
                        List<Tune> artistsInStock = tuneService.getByArtistAndFilters(artist, Collections.emptySet());
                        items.add(new RecommendationItem(!artistsInStock.isEmpty(), artist));
                    }
                    Platform.runLater(() -> recommendations.addAll(items));
                })
                .start();
    }

    private void updateRecommendations() {
        recommendations.clear();
        if (currentTuneProperty.get() != null) {
            if (recommendationModeProperty.get() == RecommendationMode.RECOMM_SESSION) {
                startSessionRecommendationThread();
            }
            if (recommendationModeProperty.get() == RecommendationMode.RECOMM_COMMUNITY) {
                startCommunityRecommendationThread();
            }
        }
    }
}

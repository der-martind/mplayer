package de.cbb.mplayer.recommendations.view;

import de.cbb.mplayer.recommendations.domain.RecommendationItem;
import de.cbb.mplayer.recommendations.domain.RecommendationMode;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import org.springframework.stereotype.Component;

@Component
public class RecommendationsView implements FxmlView<RecommendationsViewModel> {

    @FXML
    private ListView<RecommendationItem> listRecommendations;

    @FXML
    private ToggleButton rbSessionRecomm;

    @FXML
    private ToggleButton rbCommunityRecomm;

    @InjectViewModel
    private RecommendationsViewModel viewModel;

    public void initialize() {
        listRecommendations.setItems(viewModel.getRecommendations());
        listRecommendations.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() == 2
                    && viewModel.getRecommendationModeProperty().get().equals(RecommendationMode.RECOMM_COMMUNITY)) {
                viewModel.searchArtist(listRecommendations
                        .getSelectionModel()
                        .getSelectedItem()
                        .getArtist());
            }
        });
        final ToggleGroup tgRecommendations = new ToggleGroup();
        rbSessionRecomm.setToggleGroup(tgRecommendations);
        rbCommunityRecomm.setToggleGroup(tgRecommendations);
        if (viewModel.getRecommendationModeProperty().get().equals(RecommendationMode.RECOMM_COMMUNITY))
            tgRecommendations.selectToggle(rbCommunityRecomm);
        else tgRecommendations.selectToggle(rbSessionRecomm);

        tgRecommendations
                .selectedToggleProperty()
                .addListener((final ObservableValue<? extends Toggle> ov, final Toggle t, final Toggle t1) -> {
                    if (t1 == null) { // selected toggle selected again
                        return;
                    }
                    final ToggleButton chk = (ToggleButton) t1.getToggleGroup().getSelectedToggle();
                    if (chk == rbSessionRecomm) {
                        viewModel.getRecommendationModeProperty().set(RecommendationMode.RECOMM_SESSION);
                    }
                    if (chk == rbCommunityRecomm) {
                        viewModel.getRecommendationModeProperty().set(RecommendationMode.RECOMM_COMMUNITY);
                    }
                });

        listRecommendations.setCellFactory(new RecommendationItemCellFactory());
    }
}

package de.cbb.mplayer.recommendations;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import de.cbb.mplayer.common.listeners.DefaultListener;
import de.cbb.mplayer.perspectives.events.TuneSelectedEvent;
import de.cbb.mplayer.player.events.PlayTuneEvent;
import de.cbb.mplayer.recommendations.view.RecommendationsViewModel;
import org.springframework.stereotype.Component;

@Component
public class RecommendationsListener extends DefaultListener {

    private final RecommendationsViewModel viewModel;

    public RecommendationsListener(
            EventBus eventBus, DefaultErrorHandler defaultErrorHandler, final RecommendationsViewModel viewModel) {
        super(eventBus, defaultErrorHandler);
        this.viewModel = viewModel;
    }

    @Subscribe
    public void handleTunesSelectedEvent(final TuneSelectedEvent event) {
        // do nothing
    }

    @Subscribe
    public void handlePlayTuneEvent(final PlayTuneEvent event) {
        try {
            viewModel.getCurrentTuneProperty().set(event.getTune());
        } catch (final Exception ex) {
            defaultErrorHandler.handle("handlePlayTuneEvent", ex);
        }
    }
}

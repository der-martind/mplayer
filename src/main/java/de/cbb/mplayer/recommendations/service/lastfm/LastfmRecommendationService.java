package de.cbb.mplayer.recommendations.service.lastfm;

import de.cbb.mplayer.config.ConfigProperties;
import de.umass.lastfm.Artist;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class LastfmRecommendationService {

    private final ConfigProperties properties;
    private final LastfmConfiguration lastfmConfig;

    @Cacheable("community-recommendations")
    public List<String> getCommunityRecommendations(final String artist) {
        final List<String> list = new ArrayList<>();
        Collection<de.umass.lastfm.Artist> sim =
                Artist.getSimilar(artist, properties.getMaxRecommendations(), lastfmConfig.getApikey());
        for (Artist a : sim) {
            list.add(a.getName());
        }
        return list;
    }
}

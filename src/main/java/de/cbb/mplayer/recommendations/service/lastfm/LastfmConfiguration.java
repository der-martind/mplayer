package de.cbb.mplayer.recommendations.service.lastfm;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author der-martind
 */
@Configuration
@Data
@Slf4j
public class LastfmConfiguration {

    @Value("${lastfm.apikey}")
    private String apikey;

    @Value("${lastfm.shared-secret}")
    private String sharedSecret;
}

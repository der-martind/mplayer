package de.cbb.mplayer.recommendations.service;

import de.cbb.mplayer.charts.repo.ChartRepository;
import de.cbb.mplayer.config.ConfigProperties;
import de.cbb.mplayer.recommendations.service.lastfm.LastfmRecommendationService;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class RecommendationServiceImpl implements RecommendationService {

    private final ChartRepository chartRepository;
    private final ConfigProperties properties;
    private final LastfmRecommendationService recommendationService;

    @Cacheable("community-recommendations")
    public List<String> getCommunityRecommendations(final String artist) {
        List<String> list = new ArrayList<>();
        try {
            list = recommendationService.getCommunityRecommendations(artist);
        } catch (Exception ex) {
            log.error("Recommendation failed", ex);
            return list;
        }
        log.info("recommendations for " + artist + ": " + list.size());
        return list;
    }

    @Cacheable("session-recommendations")
    public List<String> getSessionRecommendations(final String artist) {
        return chartRepository.findDistinctArtistsByPlayed(artist, properties.getMaxRecommendations());
    }
}

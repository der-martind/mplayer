package de.cbb.mplayer.recommendations.service;

import java.util.List;

public interface RecommendationService {

    List<String> getSessionRecommendations(final String artist);

    List<String> getCommunityRecommendations(final String artist);
}

package de.cbb.mplayer.recommendations.domain;

public enum RecommendationMode {
    RECOMM_SESSION,
    RECOMM_COMMUNITY
}

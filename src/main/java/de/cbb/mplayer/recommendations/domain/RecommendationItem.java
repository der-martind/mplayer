package de.cbb.mplayer.recommendations.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RecommendationItem {

    private boolean showIcon;
    private String artist;

    @Override
    public String toString() {
        return artist;
    }
}

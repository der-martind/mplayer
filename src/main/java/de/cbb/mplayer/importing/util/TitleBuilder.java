package de.cbb.mplayer.importing.util;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TitleBuilder {

    private final List<String> unwantedTitleParts;

    public String build(String title) {
        if (title == null) return title;
        String result = title;
        for (String part : unwantedTitleParts) {
            result = result.replaceAll(part, "");
        }
        return result.trim();
    }
}

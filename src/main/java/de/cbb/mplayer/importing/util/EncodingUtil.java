package de.cbb.mplayer.importing.util;

import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.MultimediaInfo;
import java.io.File;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EncodingUtil {

    public static long getDurationInSecs(File source) {
        Encoder encoder = new Encoder();
        try {
            MultimediaInfo mi = encoder.getInfo(source);
            long millies = mi.getDuration();
            return millies / 1000;
        } catch (Exception e) {
            log.warn("getDurationInSecs failed: {}", e.toString());
        }
        return 0;
    }
}

package de.cbb.mplayer.importing.util;

import de.cbb.mplayer.config.ConfigProperties;
import de.cbb.mplayer.importing.mapping.ImportFileInfo;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

@Component
public class PathParser {

    /*
     * Path format when parsing for tune meta data
     */
    private final ConfigProperties properties;
    private final TitleBuilder titleBuilder;

    private String relativePath;

    private final List<Pattern> pathPatterns;

    public PathParser(final ConfigProperties properties) {
        this.properties = properties;
        pathPatterns = new ArrayList<>();
        for (final String format : properties.getImporting().getPathFormats()) {
            pathPatterns.add(Pattern.compile(format));
        }
        this.titleBuilder = new TitleBuilder(properties.getImporting().getUnwantedTitleParts());
    }

    public ImportFileInfo parse(final File file) {
        relativePath =
                file.getAbsolutePath().substring(properties.getMediaRoot().length() + 1);
        return new ImportFileInfo(getArtist(), getAlbum(), getTitle());
    }

    private String getArtist() {
        return getGroup("artist1");
    }

    private String getAlbum() {
        return getGroup("album");
    }

    private String getTitle() {
        return titleBuilder.build(getGroup("title"));
    }

    private String getGroup(final String group) {
        for (final Pattern pattern : pathPatterns) {
            final Matcher m1 = pattern.matcher(relativePath);
            if (m1.find()) {
                if (m1.namedGroups().containsKey(group)) {
                    return m1.group(group);
                } else {
                    return null;
                }
            }
        }
        return relativePath;
    }
}

package de.cbb.mplayer.importing.view;

import de.cbb.mplayer.common.ui.PointOnScreen;
import de.cbb.mplayer.discogs.view.DiscogsDialog;
import de.cbb.mplayer.tags.view.TagSelectionDialog;
import de.cbb.mplayer.tags.view.chips.ChipsPane;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.util.converter.NumberStringConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ImportingView implements FxmlView<ImportingViewModel> {

    @FXML
    private TextField tfArtist;

    @FXML
    private TextField tfAlbum;

    @FXML
    private TextField tfTitle;

    @FXML
    private TextField tfPath;

    @FXML
    private TextField tfYear;

    @FXML
    private TextField tfFilename2;

    @FXML
    private HBox ctnTags;

    @FXML
    private Button pbShowDiscogs;

    @FXML
    private Button pbSave;

    @FXML
    private Button pbReleaseYear;

    @InjectViewModel
    private ImportingViewModel viewModel;

    private final TagSelectionDialog tagSelectionDialog;

    public void initialize() {
        tfArtist.textProperty()
                .bindBidirectional(viewModel.getTunePropertyAdapter().getArtistProperty());
        tfAlbum.textProperty()
                .bindBidirectional(viewModel.getTunePropertyAdapter().getAlbumProperty());
        tfTitle.textProperty()
                .bindBidirectional(viewModel.getTunePropertyAdapter().getTitleProperty());
        // TODO: disable on folder import
        Bindings.bindBidirectional(
                tfYear.textProperty(),
                viewModel.getTunePropertyAdapter().getYearProperty(),
                new NumberStringConverter());
        tfPath.textProperty().bind(viewModel.getTunePropertyAdapter().getPathProperty());
        tfPath.setEditable(false);
        tfFilename2
                .textProperty()
                .bindBidirectional(viewModel.getTunePropertyAdapter().getFilename2Property());
        tfFilename2.setEditable(false);

        pbShowDiscogs.setOnAction(event -> showDiscogsDialog());
        pbSave.setOnAction(event -> viewModel.saveTune());
        pbReleaseYear.setOnAction(event -> fillReleaseYear());
        final ChipsPane pnTags = new ChipsPane(
                viewModel.getTagService(), viewModel.getTunePropertyAdapter().getTags(), tagSelectionDialog);
        ctnTags.getChildren().add(pnTags);
        tfYear.focusedProperty().addListener((observableValue, aBoolean, t1) -> {
            if (!t1) { // focus lost
                viewModel.updateDecateTag();
            }
        });
    }

    private void fillReleaseYear() {
        pbReleaseYear.getScene().setCursor(Cursor.WAIT);
        viewModel.fillReleaseYear();
        pbReleaseYear.getScene().setCursor(Cursor.DEFAULT);
        tfYear.requestFocus();
    }

    private void showDiscogsDialog() {
        DiscogsDialog.display(
                viewModel.getDiscogs(),
                new PointOnScreen(
                        pbShowDiscogs.getScene().getWindow().getX(),
                        pbShowDiscogs.getScene().getWindow().getY()));
    }
}

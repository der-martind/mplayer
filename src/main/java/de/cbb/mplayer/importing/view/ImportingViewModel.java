package de.cbb.mplayer.importing.view;

import de.cbb.mplayer.common.Constants;
import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import de.cbb.mplayer.common.ui.PointOnScreen;
import de.cbb.mplayer.common.ui.dialogs.FxmlInfoDialog;
import de.cbb.mplayer.detail.view.mapping.TunePropertyAdapter;
import de.cbb.mplayer.discogs.domain.Result;
import de.cbb.mplayer.discogs.domain.Root;
import de.cbb.mplayer.discogs.service.DiscogsService;
import de.cbb.mplayer.importing.mapping.ImportMapper;
import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tags.service.TagService;
import de.cbb.mplayer.tunes.domain.Tune;
import de.cbb.mplayer.tunes.service.TuneService;
import de.saxsys.mvvmfx.ViewModel;
import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javafx.scene.control.ButtonType;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class ImportingViewModel implements ViewModel {

    private final TuneService tuneService;
    private final DefaultErrorHandler defaultErrorHandler;

    @Getter
    private final TagService tagService;

    private final ImportMapper importMapper;
    private final DiscogsService discogsService;

    @Getter
    private final TunePropertyAdapter tunePropertyAdapter = new TunePropertyAdapter();

    private File importFile;
    private FxmlInfoDialog dlg;

    public void saveTune() {
        log.debug("saveTune");
        try {
            if (importFile.isDirectory()) {
                for (final File file : Objects.requireNonNull(importFile.listFiles())) {
                    final Tune tune = mapTune(file);
                    importMapper.afterDirectoryEdit(tune, file);
                    tuneService.save(tune);
                    // eventBus.post(new TuneImportedEvent(selectedTuneProperty.get())); TODO?
                }
            } else {
                tuneService.save(mapTune(importFile));
            }
        } catch (final Exception ex) {
            defaultErrorHandler.handle("saveTune ( " + importFile.getName() + ")", ex);
        }
        closeDialog();
    }

    public void showImportDialog(final File file, final PointOnScreen point) {
        this.importFile = file;
        tunePropertyAdapter.map2Adapter(importMapper.beforeEdit(importFile));
        dlg = new FxmlInfoDialog("Import", point, ImportingView.class);
        dlg.show();
    }

    public void fillReleaseYear() {
        Integer releaseYear;
        if (tunePropertyAdapter.getAlbumProperty().get().equalsIgnoreCase(Constants.DEFAULT_ALBUM))
            releaseYear = discogsService.getTrackReleaseYear(
                    tunePropertyAdapter.getArtistProperty().get(),
                    tunePropertyAdapter.getTitleProperty().get());
        else
            releaseYear = discogsService.getReleaseYear(
                    tunePropertyAdapter.getArtistProperty().get(),
                    tunePropertyAdapter.getAlbumProperty().get());
        if (releaseYear > 0 && !releaseYear.equals(Constants.DEFAULT_YEAR))
            tunePropertyAdapter.getYearProperty().set(releaseYear);
    }

    public void updateDecateTag() {
        int year = tunePropertyAdapter.getYearProperty().get();
        if (year > 0 && year != Constants.DEFAULT_YEAR) {
            Tag tag = tagService.findDecadeTag(year);
            if (tag != null) tunePropertyAdapter.getTags().add(tag);
        }
    }

    public List<Result> getDiscogs() {
        final Root r = discogsService.getReleases(
                tunePropertyAdapter.getArtistProperty().get(),
                tunePropertyAdapter.getAlbumProperty().get());
        if (r == null) return Collections.emptyList();
        return r.getResults();
    }

    private Tune mapTune(final File file) {
        final Tune tune = new Tune();
        tunePropertyAdapter.map2Entity(tune);
        importMapper.afterEdit(tune, file);
        return tune;
    }

    private void closeDialog() {
        // add dummy cancel button to close the dialog
        dlg.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL);
        dlg.close();
    }
}

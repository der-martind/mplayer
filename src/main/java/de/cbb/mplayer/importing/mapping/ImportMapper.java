package de.cbb.mplayer.importing.mapping;

import de.cbb.mplayer.common.Constants;
import de.cbb.mplayer.config.ConfigProperties;
import de.cbb.mplayer.importing.util.EncodingUtil;
import de.cbb.mplayer.importing.util.PathParser;
import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tags.service.TagService;
import de.cbb.mplayer.tunes.domain.Tune;
import java.io.File;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ImportMapper {

    private final ConfigProperties properties;
    private final PathParser pathParser;
    private final TagService tagService;

    public Tune beforeEdit(final File importFile) {
        final Tune tune = new Tune();
        tune.setPath(buildTunepath(importFile));
        final ImportFileInfo fileInfo = pathParser.parse(importFile);
        tune.setArtist(fileInfo.getArtist() == null ? Constants.DEFAULT_ARTIST : fileInfo.getArtist());
        tune.setAlbum(fileInfo.getAlbum() == null ? Constants.DEFAULT_ALBUM : fileInfo.getAlbum());
        if (!importFile.isDirectory()) {
            mapFile(fileInfo, tune, importFile);
        } else {
            mapFolder(tune);
        }
        tune.setYear2(Constants.DEFAULT_YEAR);
        return tune;
    }

    public void afterEdit(final Tune tune, final File importFile) {
        tune.setFilename2(importFile.getName());
        tune.setDuration(EncodingUtil.getDurationInSecs(importFile));
    }

    public void afterDirectoryEdit(final Tune tune, final File file) {
        afterEdit(tune, file);
        checkAlbumTag(tune);
        final ImportFileInfo fileInfo = pathParser.parse(file);
        tune.setTitle(fileInfo.getTitle() == null ? Constants.DEFAULT_TITLE : fileInfo.getTitle());
    }

    private void mapFolder(final Tune tune) {
        List<Tag> tags = tagService.findAllOrdered();
        tags.stream().filter(Tag::isImportFolderDefault).forEach(tune.getTags()::add);
    }

    private void mapFile(final ImportFileInfo fileInfo, final Tune tune, final File importFile) {
        tune.setTitle(fileInfo.getTitle() == null ? Constants.DEFAULT_TITLE : fileInfo.getTitle());
        tune.setFilename2(importFile.getName());
        List<Tag> tags = tagService.findAllOrdered();
        tags.stream().filter(Tag::isImportFileDefault).forEach(tune.getTags()::add);
    }

    private String buildTunepath(final File importFile) {
        if (importFile.isDirectory()) {
            return importFile.getPath().substring(properties.getMediaRoot().length() + 1);
        } else {
            return importFile
                    .getPath()
                    .substring(
                            properties.getMediaRoot().length() + 1,
                            importFile.getPath().lastIndexOf(Constants.FILE_SEPARATOR));
        }
    }

    private void checkAlbumTag(final Tune tune) {
        if (tune.getTags().stream()
                .filter(tag -> tag.getValue().equals("Album"))
                .findFirst()
                .isEmpty()) {
            final Tag tag = tagService.findByValue("Album");
            if (tag != null) {
                tune.getTags().add(tag);
            }
        }
    }
}

package de.cbb.mplayer.importing.mapping;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class ImportFileInfo {

    private final String artist;
    private final String album;
    private final String title;
}

package de.cbb.mplayer.common.utils;

import com.google.common.io.Files;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class ImageUtil {

    public static byte[] getImageFileContent(String imageFile) throws IOException {
        BufferedImage bImage = ImageIO.read(new File(imageFile));
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(bImage, Files.getFileExtension(imageFile), bos);
        return bos.toByteArray();
    }
}

package de.cbb.mplayer.common.utils;

import javafx.scene.paint.Color;

public class FxUtils {

    public static String toRGBCode(final Color color) {
        return String.format(
                "#%02X%02X%02X",
                (int) (color.getRed() * 255), (int) (color.getGreen() * 255), (int) (color.getBlue() * 255));
    }
}

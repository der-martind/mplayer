package de.cbb.mplayer.common.converters;

import com.google.common.base.Strings;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.util.StringConverter;

public class DurationConverter extends StringConverter<Number> {

    private static final String PATTERN_H_MM_SS = "%d:%02d:%02d";
    private static final String PATTERN_MM_SS = "%d:%02d";
    private static final String REGEX_DURATION = "^(?<hrs>\\d?):?(?<mins>\\d+):(?<secs>\\d{2})$";

    @Override
    public String toString(final Number duration) {
        if (duration == null) {
            return "0";
        }
        final java.time.Duration dur = java.time.Duration.ofSeconds(duration.longValue());
        final long hours = dur.toHours();
        final int minutes = dur.toMinutesPart();
        final int seconds = dur.toSecondsPart();

        if (hours > 0) {
            return String.format(PATTERN_H_MM_SS, hours, minutes, seconds);
        } else {
            return String.format(PATTERN_MM_SS, minutes, seconds);
        }
    }

    @Override
    public Number fromString(final String string) {
        final Pattern pattern = Pattern.compile(REGEX_DURATION);
        final Matcher m = pattern.matcher(string);
        if (m.find()) {
            final String hrs = m.group("hrs");
            final String mins = m.group("mins");
            final String secs = m.group("secs");
            return Integer.parseInt(secs)
                    + (Integer.parseInt(mins) * 60)
                    + ((Integer.parseInt(Strings.isNullOrEmpty(hrs) ? "0" : hrs)) * 3600);
        }
        return 0;
    }
}

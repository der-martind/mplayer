package de.cbb.mplayer.common.converters;

import de.cbb.mplayer.common.Constants;
import de.cbb.mplayer.config.ConfigProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PathConverter {

    private final ConfigProperties properties;

    public String toRelativePath(final String absolutePath) {
        return absolutePath.substring(properties.getMediaRoot().length() + 1);
    }

    public String toRelativeParentPath(final String absolutePath) {
        final String relativePath = toRelativePath(absolutePath);
        if (relativePath.lastIndexOf(Constants.FILE_SEPARATOR) < 0) {
            return "";
        }
        return relativePath.substring(0, relativePath.lastIndexOf(Constants.FILE_SEPARATOR));
    }
}

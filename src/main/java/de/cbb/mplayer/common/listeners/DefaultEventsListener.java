package de.cbb.mplayer.common.listeners;

import com.google.common.eventbus.EventBus;
import de.cbb.mplayer.common.errors.DefaultErrorHandler;

public final class DefaultEventsListener extends DefaultListener {

    public DefaultEventsListener(final EventBus eventBus, final DefaultErrorHandler defaultErrorHandler) {
        super(eventBus, defaultErrorHandler);
    }
}

package de.cbb.mplayer.common.listeners;

import com.google.common.eventbus.EventBus;
import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class DefaultListener {

    protected final EventBus eventBus;
    protected final DefaultErrorHandler defaultErrorHandler;

    @PostConstruct
    public void init() {
        eventBus.register(this);
    }

    public void registerForDefaultEvents() {
        new DefaultEventsListener(eventBus, defaultErrorHandler).init();
    }
}

package de.cbb.mplayer.common.errors;

import com.google.common.eventbus.EventBus;
import de.cbb.mplayer.main.events.NotificationEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class DefaultErrorHandler {

    private final EventBus eventBus;

    public void handle(final String method, final Exception ex) {
        log.error(method + " failed", ex);
        eventBus.post(new NotificationEvent(method + " failed:" + ex));
    }
}

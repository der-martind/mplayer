package de.cbb.mplayer.common.ui.controls;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class JumperBar extends VBox {

    private static final String[] LETTERS = {
        "0", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", /*"Q",*/ "R", "S", "T",
        "Th", "Ti", "U", "V", "W", "X", "Y", "Z"
    };

    public JumperBar(final EventHandler<ActionEvent> handler) {
        super();

        getStyleClass().add("jumper-bar");

        for (final String letter : LETTERS) {
            final Button pb = new Button(letter);
            pb.setPrefWidth(18);
            pb.setOnAction(handler);
            getChildren().add(pb);
        }
    }
}

package de.cbb.mplayer.common.ui.trees;

import javafx.scene.control.TreeItem;

/**
 * Class to simulate a parent TreeItem with children, so the expand icon is displayed, even if the
 * children are not loaded yet.
 */
public class ExpandableParentTreeItem extends TreeItem<TreeItemValue> {

    public ExpandableParentTreeItem(final String value) {
        super(new TreeItemTitleValue(value));
    }

    @Override
    public boolean isLeaf() {
        return false;
    }
}

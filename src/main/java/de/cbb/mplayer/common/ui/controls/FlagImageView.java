package de.cbb.mplayer.common.ui.controls;

import com.google.common.base.Strings;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Objects;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FlagImageView extends ImageView {

    private static final String BASE_PATH = "/img/countries/";
    private static final String MAP_FILE = "/country-list.csv";
    private static final String SEPARATOR = ",";

    private static HashMap<String, String> map;

    public FlagImageView(final String country) {
        super();
        if (FlagImageView.map == null) {
            initCountryMap();
        }
        final String countryCode = map.get(country);
        if (!Strings.isNullOrEmpty(countryCode)) {
            final String flagPath = getCountryFlagImage(country);
            try (final InputStream in = this.getClass().getResourceAsStream(flagPath)) {
                if (in != null) {
                    setImage(new Image(flagPath));
                } else {
                    log.warn("Flag image not found: " + flagPath);
                }
            } catch (final IOException e) {
                log.warn("Loading flag image failed", e);
            }
        } else {
            log.warn("Country code not found: " + country);
        }
    }

    private String getCountryFlagImage(final String country) {
        return BASE_PATH + map.get(country).toLowerCase() + ".png";
    }

    private static void initCountryMap() {
        String line;
        map = new HashMap<>();
        try (final BufferedReader br = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(FlagImageView.class.getResourceAsStream(MAP_FILE))))) {

            while ((line = br.readLine()) != null) {
                final String[] country = line.split(SEPARATOR);
                map.put(country[0], country[1]);
            }

        } catch (final IOException e) {
            log.warn("Reading country file failed", e);
        }
    }
}

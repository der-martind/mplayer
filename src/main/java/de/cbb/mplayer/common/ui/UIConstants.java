package de.cbb.mplayer.common.ui;

import java.util.Objects;

public class UIConstants {

    public static final String STYLESHEET = Objects.requireNonNull(UIConstants.class.getResource("/styles/healthy.css"))
            .toExternalForm();
}

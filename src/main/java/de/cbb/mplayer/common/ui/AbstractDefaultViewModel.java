package de.cbb.mplayer.common.ui;

import de.cbb.mplayer.tunes.domain.Tune;
import de.saxsys.mvvmfx.ViewModel;
import java.util.List;

public abstract class AbstractDefaultViewModel implements ViewModel {

    public abstract List<Tune> getSelectedTunes();
}

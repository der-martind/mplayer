package de.cbb.mplayer.common.ui.ctxmenu;

import de.cbb.mplayer.bulking.domain.BulkInfo;
import de.cbb.mplayer.bulking.view.BulkView;
import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import de.cbb.mplayer.common.ui.AbstractDefaultViewModel;
import de.cbb.mplayer.common.ui.PointOnScreen;
import de.cbb.mplayer.common.ui.dialogs.FxmlResultDialog;
import de.cbb.mplayer.detail.view.DetailViewModel;
import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tunes.domain.Tune;
import de.cbb.mplayer.tunes.service.TuneService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class EditAction implements ContextMenuAction {

    private final DetailViewModel detailViewModel;
    private final TuneService tuneService;
    protected final DefaultErrorHandler defaultErrorHandler;

    @Override
    public void execute(final AbstractDefaultViewModel viewModel, final PointOnScreen point) {
        if (viewModel.getSelectedTunes().size() == 1)
            detailViewModel.showDialog(viewModel.getSelectedTunes().get(0), point);
        else bulkTunes(viewModel.getSelectedTunes(), point);
    }

    private void bulkTunes(List<Tune> selectedTunes, final PointOnScreen point) {
        final BulkInfo bulkInfo = new FxmlResultDialog<BulkInfo>().display("Bulk", point, BulkView.class);
        if (bulkInfo != null) {
            for (final Tune tune : selectedTunes) {
                bulkTune(tune, bulkInfo);
            }
        }
    }

    private void bulkTune(final Tune tune, final BulkInfo bulkInfo) {
        boolean updated = false;
        if (bulkInfo.getPath().isPresent()) {
            tune.setPath(bulkInfo.getPath().get());
            updated = true;
        }
        if (bulkInfo.getArtist().isPresent()) {
            tune.setArtist(bulkInfo.getArtist().get());
            updated = true;
        }
        if (bulkInfo.getAlbum().isPresent()) {
            tune.setAlbum(bulkInfo.getAlbum().get());
            updated = true;
        }
        if (bulkInfo.getYear().isPresent()) {
            tune.setYear2(bulkInfo.getYear().get());
            updated = true;
        }
        if (!bulkInfo.getTags().isEmpty()) {
            for (final Tag tag : bulkInfo.getTags()) {
                if (!tune.containsTag(tag)) {
                    tune.getTags().add(tag);
                    updated = true;
                }
            }
        }
        if (!bulkInfo.getUntags().isEmpty()) {
            for (final Tag tag : bulkInfo.getUntags()) {
                if (tune.containsTag(tag)) {
                    tune.removeTag(tag);
                    updated = true;
                }
            }
        }
        if (updated) {
            if (!saveTune(tune)) {
                log.warn("Update failed on tune: " + tune);
            }
        }
    }

    private boolean saveTune(final Tune tune) {
        try {
            tuneService.save(tune);
        } catch (final Exception ex) {
            defaultErrorHandler.handle("Saving tune " + tune.getTid(), ex);
            return false;
        }
        return true;
    }
}

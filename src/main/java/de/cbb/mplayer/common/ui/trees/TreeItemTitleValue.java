package de.cbb.mplayer.common.ui.trees;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class TreeItemTitleValue implements TreeItemValue {

    private final String title;

    public String toString() {
        return title;
    }
}

package de.cbb.mplayer.common.ui.trees;

import com.google.common.eventbus.EventBus;
import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import de.cbb.mplayer.common.ui.AbstractDefaultViewModel;
import de.cbb.mplayer.session.service.EnqueueingService;
import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tags.service.TagService;
import de.cbb.mplayer.tunes.domain.Tune;
import de.cbb.mplayer.tunes.service.TuneService;
import java.util.List;
import javafx.scene.control.TreeItem;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public abstract class AbstractTreeViewModel extends AbstractDefaultViewModel {

    @Getter
    protected final TagService tagService;

    protected final TuneService tuneService;
    protected final EventBus eventBus;
    protected final EnqueueingService enqueueingService;
    protected final DefaultErrorHandler defaultErrorHandler;

    protected abstract TreeItem<TreeItemValue> getRoot();

    public void enqueueSelectedTunes() {
        enqueueingService.enqueue(getSelectedTunes());
    }

    public List<Tag> getTags() {
        return tagService.findAllOrdered();
    }

    public void update(final Tune tune) {
        final TreeItem<TreeItemValue> parent = getRoot();
        update(parent, tune);
    }

    public void onTabSelected() {
        getRoot().setExpanded(true);
    }

    private void update(final TreeItem<TreeItemValue> parent, final Tune tune) {
        for (final TreeItem<TreeItemValue> item : parent.getChildren()) {
            if (item.getValue() instanceof TreeItemTuneValue value) {
                if (value.getTune().getTid().equals(tune.getTid())) {
                    value.setTune(tune);
                }
            } else {
                update(item, tune);
            }
        }
    }
}

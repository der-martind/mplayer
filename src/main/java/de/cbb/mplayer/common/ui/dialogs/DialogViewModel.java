package de.cbb.mplayer.common.ui.dialogs;

public interface DialogViewModel<T> {

    T getResult();

    void reset();
}

package de.cbb.mplayer.common.ui.dialogs;

import de.cbb.mplayer.common.ui.PointOnScreen;
import de.cbb.mplayer.common.ui.UIConstants;
import de.saxsys.mvvmfx.FluentViewLoader;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.ViewModel;
import de.saxsys.mvvmfx.ViewTuple;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class FxmlResultDialog<T> {

    public T display(
            final String title,
            final PointOnScreen point,
            final Class<? extends de.saxsys.mvvmfx.FxmlView<? extends de.saxsys.mvvmfx.ViewModel>> viewClass) {
        final Stage window = initWindow(title, point);

        final ViewTuple<? extends FxmlView<? extends ViewModel>, ? extends ViewModel> tuple =
                FluentViewLoader.fxmlView(viewClass).load();
        final DialogViewModel<T> model = (DialogViewModel<T>) tuple.getViewModel();
        model.reset();
        final Parent root = tuple.getView();

        final Scene scene = new Scene(root);
        scene.getStylesheets().add(UIConstants.STYLESHEET);
        scene.addEventHandler(KeyEvent.KEY_PRESSED, t -> {
            if (t.getCode() == KeyCode.ESCAPE) {
                window.close();
            }
        });
        window.setScene(scene);
        window.showAndWait();

        return model.getResult();
    }

    private static Stage initWindow(final String title, final PointOnScreen point) {
        final Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.initStyle(StageStyle.UTILITY);
        window.setX(point.getX());
        window.setY(point.getY());
        return window;
    }
}

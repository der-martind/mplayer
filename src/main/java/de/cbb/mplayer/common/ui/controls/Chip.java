package de.cbb.mplayer.common.ui.controls;

import java.io.ByteArrayInputStream;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

public class Chip extends HBox {

    private static final int DERIVE_COLOR = 70;

    private HBox content;

    public Chip(final byte[] icon, final String color) {
        setAlignment(Pos.CENTER_LEFT);
        // l0
        Image img = new Image(new ByteArrayInputStream(icon));
        final ImageView l0 = new ImageView(img);
        l0.getStyleClass().add("chip0");
        l0.setStyle(String.format("-fx-background-color: %s;", color));
        getChildren().add(l0);
    }

    public Chip(final String value, final String color) {
        setAlignment(Pos.CENTER_LEFT);
        final String[] texts = value.split(":");
        // l0
        final Label l0 = new Label(texts[0]);
        l0.getStyleClass().add("chip0");
        l0.setStyle(String.format("-fx-background-color: %s;", color));
        getChildren().add(l0);
        // hbox
        content = new HBox();
        content.setAlignment(Pos.CENTER_LEFT);
        content.getStyleClass().add("chip1-readonly");
        getChildren().add(content);
        // l1
        if (texts.length > 1) {
            l0.setText(l0.getText() + ":");
            content.setStyle(String.format("-fx-background-color: derive(%s, %02d%%);", color, DERIVE_COLOR));
            final Label l1 = new Label(texts[1]);
            content.getChildren().add(l1);
        } else {
            content.setStyle(String.format("-fx-background-color: %s;", color));
        }
    }

    public Chip(final String value, final String color, final EventHandler<ActionEvent> handler) {
        this(value, color);
        content.getStyleClass().add("chip1-closeable");
        // btn
        final Button btn = new Button("x");
        btn.getStyleClass().add("chip-button");
        btn.setOnAction(handler);
        content.getChildren().add(btn);
    }
}

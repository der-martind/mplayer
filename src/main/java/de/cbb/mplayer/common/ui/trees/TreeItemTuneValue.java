package de.cbb.mplayer.common.ui.trees;

import de.cbb.mplayer.tunes.domain.Tune;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TreeItemTuneValue implements TreeItemValue {

    private Tune tune;

    public String toString() {
        return tune.toString();
    }
}

package de.cbb.mplayer.common.ui.trees;

import de.cbb.mplayer.common.ui.controls.Chip;
import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tunes.domain.Tune;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;

public class TaggedTreeItem extends HBox {

    private static final int MAX_CHIPVALUE_LENGTH = 7;

    public TaggedTreeItem(final Tune tune, final boolean limitSize) {
        setAlignment(Pos.CENTER_LEFT);

        final Label l = new Label(tune.toString());
        l.setTooltip(new Tooltip(tune.toString()));
        getChildren().add(l);

        final Region spacer = new Region();
        spacer.setPrefWidth(5);
        getChildren().add(spacer);

        tune.getTags().forEach(t -> {
            if (t.isVisible()) {
                String value = createTagValue(t, limitSize);
                Chip chip = t.getIcon() != null ? new Chip(t.getIcon(), t.getColor()) : new Chip(value, t.getColor());
                chip.setMinWidth(Region.USE_PREF_SIZE);
                getChildren().add(chip);
            }
        });
    }

    private String createTagValue(Tag t, boolean limitSize) {
        String value;
        if (limitSize)
            value = t.getValue()
                    .substring(0, Math.min(MAX_CHIPVALUE_LENGTH, t.getValue().length()));
        else value = t.getValue();
        return value;
    }
}

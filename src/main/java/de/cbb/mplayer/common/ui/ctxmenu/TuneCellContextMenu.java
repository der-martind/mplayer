package de.cbb.mplayer.common.ui.ctxmenu;

import de.cbb.mplayer.common.ui.AbstractDefaultViewModel;
import de.cbb.mplayer.common.ui.PointOnScreen;
import java.util.Map;
import javafx.event.ActionEvent;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;

public class TuneCellContextMenu extends ContextMenu {

    public static final String ACTION_EDIT = "Edit";

    public TuneCellContextMenu(
            final AbstractDefaultViewModel viewModel, final Map<String, ContextMenuAction> actionMap) {

        final MenuItem miEdit = new MenuItem(ACTION_EDIT);
        miEdit.setOnAction((final ActionEvent t) ->
                actionMap.get(ACTION_EDIT).execute(viewModel, new PointOnScreen(this.getX(), this.getY())));
        getItems().add(miEdit);
    }
}

package de.cbb.mplayer.common.ui.ctxmenu;

import de.cbb.mplayer.common.ui.AbstractDefaultViewModel;
import de.cbb.mplayer.common.ui.PointOnScreen;

public interface ContextMenuAction {

    void execute(final AbstractDefaultViewModel viewModel, final PointOnScreen point);
}

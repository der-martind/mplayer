package de.cbb.mplayer.common.ui.animation;

import eu.hansolo.medusa.Gauge;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;
import lombok.Getter;
import lombok.Setter;

public class GaugeTimeline {

    private final Timeline timeline;

    @Getter
    private Duration duration = Duration.ZERO;

    @Getter
    @Setter
    private double currentDuration;

    public GaugeTimeline(final Gauge gauge, final int divider) {
        timeline = new Timeline(new KeyFrame(Duration.millis(100), t -> {
            final Duration lduration = ((KeyFrame) t.getSource()).getTime();
            duration = duration.add(lduration);
            gauge.setValue(duration.toSeconds() / divider);
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
    }

    public void play() {
        timeline.play();
    }
}

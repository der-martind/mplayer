package de.cbb.mplayer.common.ui.trees;

import de.cbb.mplayer.common.ui.ctxmenu.ContextMenuAction;
import de.cbb.mplayer.common.ui.ctxmenu.TuneCellContextMenu;
import java.util.Map;
import javafx.scene.control.TreeCell;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TreeCellImpl extends TreeCell<TreeItemValue> {

    private final AbstractTreeViewModel viewModel;
    private final Map<String, ContextMenuAction> actionMap;

    @Override
    public void updateItem(final TreeItemValue item, final boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            if (item instanceof TreeItemTuneValue tuneValue) {
                setText("");
                setGraphic(new TaggedTreeItem(tuneValue.getTune(), false));
            } else {
                setText(getItem() == null ? "" : getItem().toString());
                setGraphic(getTreeItem().getGraphic());
            }

            setContextMenu(new TuneCellContextMenu(viewModel, actionMap));
        }
    }
}

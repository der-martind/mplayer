package de.cbb.mplayer.common.ui;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PointOnScreen {
    private double x, y;
}

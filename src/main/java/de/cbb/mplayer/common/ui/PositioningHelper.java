package de.cbb.mplayer.common.ui;

import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.geometry.Dimension2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.stage.Screen;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PositioningHelper {

    public static PointOnScreen getPointBelowParent(final Node node) {
        return getPointBelowParent(node, 0, 0);
    }

    public static PointOnScreen getPointBesidesParent(final Node node) {
        return getPointBesidesParent(node, 0, 0);
    }

    public static PointOnScreen getPointBesideParentWindow(final Node parent, final Dimension2D dimension) {
        Bounds parentBounds = parent.localToScreen(parent.getBoundsInLocal());
        ObservableList<Screen> screen = Screen.getScreensForRectangle(
                parentBounds.getMinX(), parentBounds.getMinY(), parentBounds.getWidth(), parentBounds.getHeight());
        if (screen.size() > 1) {
            log.warn("Multiple screens for rect");
        }
        Rectangle2D screenBounds = screen.get(0).getBounds();
        PointOnScreen point =
                new PointOnScreen(parentBounds.getMinX() + parentBounds.getWidth() + 15, parentBounds.getMinY());
        if (point.getX() + dimension.getWidth() >= screenBounds.getMaxX())
            point.setX(parentBounds.getMinX() - dimension.getWidth() - 25);
        if (point.getY() + dimension.getHeight() >= screenBounds.getMaxY())
            point.setY(screenBounds.getMaxY() - dimension.getHeight() - 50);
        return point;
    }

    public static PointOnScreen getPointBelowParent(final Node node, final double xInset, final double yInset) {
        Bounds bounds = node.localToScreen(node.getBoundsInLocal());
        return new PointOnScreen(bounds.getMinX() + xInset, bounds.getMaxY() + yInset);
    }

    public static PointOnScreen getPointBesidesParent(final Node node, final double xInset, final double yInset) {
        Bounds bounds = node.localToScreen(node.getBoundsInLocal());
        return new PointOnScreen(bounds.getMaxX() + xInset, bounds.getMinY() + yInset);
    }

    public static PointOnScreen getPointOnParent(final Node node, final double xInset, final double yInset) {
        Bounds bounds = node.localToScreen(node.getBoundsInLocal());
        return new PointOnScreen(bounds.getMinX() + xInset, bounds.getMinY() + yInset);
    }
}

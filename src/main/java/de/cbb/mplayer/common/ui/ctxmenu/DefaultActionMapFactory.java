package de.cbb.mplayer.common.ui.ctxmenu;

import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import de.cbb.mplayer.detail.view.DetailViewModel;
import de.cbb.mplayer.tunes.service.TuneService;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DefaultActionMapFactory {

    private final DetailViewModel detailViewModel;
    private final TuneService tuneService;
    protected final DefaultErrorHandler defaultErrorHandler;

    public Map<String, ContextMenuAction> createActionMap() {

        Map<String, ContextMenuAction> actionMap = new HashMap<>();
        actionMap.put(
                TuneCellContextMenu.ACTION_EDIT, new EditAction(detailViewModel, tuneService, defaultErrorHandler));
        return actionMap;
    }
}

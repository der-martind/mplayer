package de.cbb.mplayer.common.ui.dialogs;

import de.cbb.mplayer.common.ui.PointOnScreen;
import de.cbb.mplayer.common.ui.UIConstants;
import de.saxsys.mvvmfx.FluentViewLoader;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.ViewModel;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Dialog;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.StageStyle;

public class FxmlInfoDialog extends Dialog {

    public FxmlInfoDialog(
            final String title,
            final PointOnScreen point,
            final Class<? extends FxmlView<? extends ViewModel>> viewclass) {
        super();
        setTitle(title);
        initStyle(StageStyle.UTILITY);

        setX(point.getX());
        setY(point.getY());

        final Parent root = FluentViewLoader.fxmlView(viewclass).load().getView();

        getDialogPane().setContent(root);
        getDialogPane().getStylesheets().add(UIConstants.STYLESHEET);

        addCloseAction();
    }

    private void addCloseAction() {
        Scene scene = getDialogPane().getScene();
        scene.getWindow().setOnCloseRequest(event -> scene.getWindow().hide());
        scene.addEventHandler(KeyEvent.KEY_PRESSED, t -> {
            if (t.getCode() == KeyCode.ESCAPE) {
                close();
            }
        });
    }
}

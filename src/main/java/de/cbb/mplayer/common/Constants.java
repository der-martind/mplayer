package de.cbb.mplayer.common;

import java.time.format.DateTimeFormatter;

public class Constants {

    public static final String FILE_SEPARATOR = System.getProperty("file.separator");
    public static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static final String DEFAULT_ALBUM = "unknown";
    public static final String DEFAULT_ARTIST = "unknown";
    public static final String DEFAULT_TITLE = "unknown";
    public static final Integer DEFAULT_YEAR = 0;
}

package de.cbb.mplayer.shutdown;

import de.cbb.mplayer.screen.domain.Coords;
import de.cbb.mplayer.screen.service.ScreenService;
import de.cbb.mplayer.userprefs.service.UserPrefsService;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ShutdownHandler implements EventHandler<WindowEvent> {

    private final ScreenService screenService;
    private final UserPrefsService userPrefsService;

    @Override
    public void handle(final WindowEvent event) {
        updateUserPrefs();
    }

    private void updateUserPrefs() {
        final Coords coords = screenService.getCoordsOnScreen();
        userPrefsService.updateCoordsOnScreen(coords);
    }
}

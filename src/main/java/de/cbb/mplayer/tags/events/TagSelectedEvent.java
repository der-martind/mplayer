package de.cbb.mplayer.tags.events;

import de.cbb.mplayer.tags.domain.Tag;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class TagSelectedEvent {

    private final Tag selectedTag;
}

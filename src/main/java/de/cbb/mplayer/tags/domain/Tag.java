package de.cbb.mplayer.tags.domain;

import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    @NotNull
    private String value;

    @Column
    @NotNull
    private String color;

    @Column(nullable = false, columnDefinition = "BOOLEAN default false")
    private boolean visible = false;

    @Column(columnDefinition = "INTEGER default 0")
    private Integer selected = 0;

    @Column(nullable = false, columnDefinition = "BOOLEAN default false")
    private boolean importFolderDefault = false;

    @Column(nullable = false, columnDefinition = "BOOLEAN default false")
    private boolean importFileDefault = false;

    @Column(nullable = false, columnDefinition = "BOOLEAN default false")
    private boolean enqueueBlocked = false;

    @Column(nullable = false, columnDefinition = "BOOLEAN default false")
    private boolean showInPlayer = false;

    @Lob
    @Column(name = "icon", columnDefinition = "BINARY LARGE OBJECT")
    private byte[] icon;

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tag other = (Tag) obj;
        return Objects.equals(this.value, other.value);
    }
}

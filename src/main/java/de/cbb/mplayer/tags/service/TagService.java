package de.cbb.mplayer.tags.service;

import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tags.repo.TagRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class TagService {

    private final TagRepository tagRepository;

    public List<Tag> findTop() {
        return tagRepository.findAll(defaultSort());
    }

    public List<Tag> findAllButTop() {
        final List<Tag> list = tagRepository.findAll(defaultSort());
        list.removeAll(findTop());
        return list;
    }

    public List<Tag> findAllOrdered() {
        final List<Tag> list = findTop();
        list.addAll(findAllButTop());
        return list;
    }

    public Tag findDecadeTag(final Integer year) {
        if (year == null) {
            return null;
        }
        return tagRepository.findByValue(buildDecadeTagValue(year));
    }

    public String buildDecadeTagValue(final Integer year) {
        if (year == null || year == 0) {
            return null;
        }
        return year.toString().charAt(2) + "0s";
    }

    public Tag findByValue(final String value) {
        return tagRepository.findByValue(value);
    }

    public void save(final Tag tag) {
        tagRepository.save(tag);
        log.info("Tag saved: " + tag);
    }

    public void incrementSelected(Tag selectedTag) {
        log.debug("incrementSelected");
        selectedTag.setSelected(selectedTag.getSelected() + 1);
        tagRepository.save(selectedTag);
    }

    private Sort defaultSort() {
        return Sort.by("selected").descending().and(Sort.by("value").ascending());
    }
}

package de.cbb.mplayer.tags.view;

import de.cbb.mplayer.common.ui.controls.Chip;
import de.cbb.mplayer.tags.domain.Tag;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class TagCellFactory implements Callback<ListView<Tag>, ListCell<Tag>> {
    @Override
    public ListCell<Tag> call(final ListView<Tag> param) {
        return new ListCell<>() {
            @Override
            public void updateItem(final Tag tag, final boolean empty) {
                super.updateItem(tag, empty);
                if (empty) {
                    setText(null);
                    setGraphic(null);
                } else if (tag != null) {
                    setText(null);
                    setGraphic(new Chip(tag.getValue(), tag.getColor()));
                } else {
                    setText("null");
                    setGraphic(null);
                }
            }
        };
    }
}

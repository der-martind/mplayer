package de.cbb.mplayer.tags.view.adding;

import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import org.springframework.stereotype.Component;

@Component
public class AddingTagView implements FxmlView<AddingTagViewModel> {

    @FXML
    private TextField tfValue;

    @FXML
    private ColorPicker cbColor;

    @FXML
    private TextField tfIconFile;

    @FXML
    private Button pbChooseFile;

    @FXML
    private CheckBox cbVisible;

    @FXML
    private CheckBox cbEnqueueBlocked;

    @FXML
    private CheckBox cbShowInPlayer;

    @FXML
    private CheckBox cbImportFolderDefault;

    @FXML
    private CheckBox cbImportFileDefault;

    @FXML
    private Button pbSave;

    @InjectViewModel
    private AddingTagViewModel viewModel;

    public void initialize() {
        tfValue.textProperty().bindBidirectional(viewModel.getValue());
        cbColor.valueProperty().bindBidirectional(viewModel.getColor());
        tfIconFile.textProperty().bindBidirectional(viewModel.getIconFile());
        cbVisible.selectedProperty().bindBidirectional(viewModel.getVisible());
        cbEnqueueBlocked.selectedProperty().bindBidirectional(viewModel.getEnqueueBlocked());
        cbShowInPlayer.selectedProperty().bindBidirectional(viewModel.getShowInPlayer());
        cbImportFolderDefault.selectedProperty().bindBidirectional(viewModel.getImportFolderDefault());
        cbImportFileDefault.selectedProperty().bindBidirectional(viewModel.getImportFileDefault());
        pbChooseFile.setOnAction(event -> viewModel.selectIconFile(pbChooseFile));
        pbSave.setOnAction(event -> viewModel.saveTag());
        Platform.runLater(tfValue::requestFocus);
    }
}

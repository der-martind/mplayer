package de.cbb.mplayer.tags.view.chips;

import de.cbb.mplayer.common.ui.PositioningHelper;
import de.cbb.mplayer.common.ui.controls.Chip;
import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tags.service.TagService;
import de.cbb.mplayer.tags.view.TagSelectionDialog;
import javafx.collections.ObservableSet;
import javafx.collections.SetChangeListener;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.layout.FlowPane;

public class ChipsPane extends FlowPane {

    private final TagService tagService;
    private final ObservableSet<Tag> values;
    private final TagSelectionDialog tagSelectionDialog;

    public ChipsPane(
            final TagService tagService, final ObservableSet<Tag> values, final TagSelectionDialog tagSelectionDialog) {
        this.tagService = tagService;
        this.values = values;
        this.tagSelectionDialog = tagSelectionDialog;
        setOrientation(Orientation.HORIZONTAL);
        getStyleClass().add("chips-pane");
        setAlignment(Pos.TOP_LEFT);
        addContextMenu();
        values.addListener((SetChangeListener<? super Tag>) change -> onChangeTags());
        setupChildren();
    }

    private void onChangeTags() {
        setupChildren();
    }

    private void setupChildren() {
        getChildren().clear();
        for (final Tag tag : values) {
            addValue(tag);
        }
    }

    private void addContextMenu() {
        setOnMouseClicked((EventHandler<Event>) event -> {
            final Tag selectedTag = tagSelectionDialog.display(
                    tagService.findAllOrdered(),
                    PositioningHelper.getPointBesideParentWindow(this, tagSelectionDialog.getSize()));
            if (selectedTag != null) {
                values.add(selectedTag);
            }
        });
    }

    private void addValue(final Tag tag) {
        getChildren().add(new Chip(tag.getValue(), tag.getColor(), handler -> values.remove(tag)));
    }
}

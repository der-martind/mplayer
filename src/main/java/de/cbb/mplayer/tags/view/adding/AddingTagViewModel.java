package de.cbb.mplayer.tags.view.adding;

import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import de.cbb.mplayer.common.ui.PointOnScreen;
import de.cbb.mplayer.common.ui.dialogs.FxmlInfoDialog;
import de.cbb.mplayer.common.utils.FxUtils;
import de.cbb.mplayer.common.utils.ImageUtil;
import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tags.service.TagService;
import de.saxsys.mvvmfx.ViewModel;
import java.io.File;
import javafx.beans.property.*;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class AddingTagViewModel implements ViewModel {

    private final TagService tagService;
    private final DefaultErrorHandler defaultErrorHandler;

    @Getter
    private final StringProperty value = new SimpleStringProperty();

    @Getter
    private final ObjectProperty<Color> color = new SimpleObjectProperty<>();

    @Getter
    private final StringProperty iconFile = new SimpleStringProperty();

    @Getter
    private final BooleanProperty visible = new SimpleBooleanProperty();

    @Getter
    private final BooleanProperty enqueueBlocked = new SimpleBooleanProperty();

    @Getter
    private final BooleanProperty showInPlayer = new SimpleBooleanProperty();

    @Getter
    private final BooleanProperty importFolderDefault = new SimpleBooleanProperty();

    @Getter
    private final BooleanProperty importFileDefault = new SimpleBooleanProperty();

    private FxmlInfoDialog dlg;

    public void saveTag() {
        log.debug("saveTag");
        try {
            final Tag tag = new Tag();
            tag.setValue(value.get());
            tag.setColor(FxUtils.toRGBCode(color.get()));
            if (iconFile.get() != null) {
                tag.setIcon(ImageUtil.getImageFileContent(iconFile.get()));
            }
            tag.setVisible(visible.get());
            tag.setEnqueueBlocked(enqueueBlocked.get());
            tag.setShowInPlayer(showInPlayer.get());
            tag.setImportFolderDefault(importFolderDefault.get());
            tag.setImportFolderDefault(importFileDefault.get());
            tagService.save(tag);
        } catch (final Exception ex) {
            defaultErrorHandler.handle("saveTag (" + value.get() + ")", ex);
        }
        closeDialog();
    }

    public void showAddTagDialog(final PointOnScreen point) {
        dlg = new FxmlInfoDialog("Add tag", point, AddingTagView.class);
        dlg.show();
    }

    public void selectIconFile(final Node parent) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("*.png", "*.png"));
        final File file = fileChooser.showOpenDialog(parent.getScene().getWindow());
        if (file == null) {
            return;
        }
        iconFile.set(file.getAbsolutePath());
    }

    private void closeDialog() {
        // add dummy cancel button to close the dialog
        dlg.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL);
        dlg.close();
    }
}

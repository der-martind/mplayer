package de.cbb.mplayer.tags;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import de.cbb.mplayer.common.errors.DefaultErrorHandler;
import de.cbb.mplayer.common.listeners.DefaultListener;
import de.cbb.mplayer.tags.events.TagSelectedEvent;
import de.cbb.mplayer.tags.service.TagService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class TagEventListener extends DefaultListener {

    private final TagService tagService;

    public TagEventListener(
            final EventBus eventBus, final DefaultErrorHandler defaultErrorHandler, final TagService tagService) {
        super(eventBus, defaultErrorHandler);
        this.tagService = tagService;
    }

    @Subscribe
    public void handleTagSelectedEvent(final TagSelectedEvent ev) {
        try {
            tagService.incrementSelected(ev.getSelectedTag());
        } catch (final Exception ex) {
            log.error("handleTagSelectedEvent failed", ex);
        }
    }
}

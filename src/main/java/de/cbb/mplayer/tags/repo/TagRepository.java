package de.cbb.mplayer.tags.repo;

import de.cbb.mplayer.tags.domain.Tag;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag, Long> {

    List<Tag> findAll();

    Tag findByValue(String value);
}

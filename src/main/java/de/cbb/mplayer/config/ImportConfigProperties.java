package de.cbb.mplayer.config;

import java.util.List;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ImportConfigProperties {

    @NotBlank
    private List<String> supportedSuffices;

    @NotBlank
    private List<String> pathFormats;

    private List<String> unwantedTitleParts;
}

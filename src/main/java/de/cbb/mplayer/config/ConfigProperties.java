package de.cbb.mplayer.config;

import javax.validation.constraints.NotBlank;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "mplayer")
@Data
public class ConfigProperties {

    @NotBlank
    private String mediaRoot;

    @NotBlank
    private String lyricsProvider;

    @NotBlank
    private String videoProvider;

    @NotBlank
    private String infoProvider;

    private ImportConfigProperties importing;

    @NotBlank
    private Integer maxRecommendations;
}

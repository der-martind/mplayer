@echo off

set DBUSER=
set DBPASSWORD=
set FILE="%userprofile%\Documents\backup\%date:~-4%-%date:~3,2%-%date:~0,2%.mp3.sql"

mysql -u %DBUSER% --password=%DBPASSWORD% mp3test < %FILE%
echo imported from file %FILE%
pause
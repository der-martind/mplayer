-- Prepend Genre: to all genre tags
update tag set value = CONCAT('Genre:', value) where value not like 'Genre%' and value != 'Disliked'  and value != 'Liked' and value not like 'Lang%';
-- insert tune_tag entry for every tune that is liked
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,87 FROM tune where liked = 1;
-- insert tune_tag entry for every tune that is disliked
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,88 FROM tune where disliked = 1;
-- filename2 = filename + filetype
update tune set filename2 = CONCAT(filename, '.', filetype);
-- insert tune_tag entry for every tune's language
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,89 FROM tune where language = 'en';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,90 FROM tune where language = 'de';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,91 FROM tune where language = 'it';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,92 FROM tune where language = 'es';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,93 FROM tune where language = 'ar';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,94 FROM tune where language = 'fr';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,95 FROM tune where language = 'ru';
-- insert tune_tag entry for every tune's genre
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,27 /*Genre:blues*/ FROM tune WHERE genre = 'blues';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,32 /*Genre:country*/ FROM tune WHERE genre = 'country';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,4 /*Genre:dance*/ FROM tune WHERE genre = 'dance';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,16 /*Genre:electro*/ FROM tune WHERE genre = 'electro';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,40 /*Genre:flamenco*/ FROM tune WHERE genre = 'flamenco';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,46 /*Genre:folk*/ FROM tune WHERE genre = 'folk';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,5 /*Genre:hip-hop*/ FROM tune WHERE genre = 'Hip-hop';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,28 /*Genre:instrumental*/ FROM tune WHERE genre = 'instrumental';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,65 /*Genre:jazz*/ FROM tune WHERE genre = 'jazz';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,42 /*Genre:klassik*/ FROM tune WHERE genre = 'klassik';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,7 /*Genre:oldies*/ FROM tune WHERE genre = 'oldies';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,9 /*Genre:pop*/ FROM tune WHERE genre = 'pop';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,20 /*Genre:reggae*/ FROM tune WHERE genre = 'reggae';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,25 /*Genre:reggaeton*/ FROM tune WHERE genre = 'reggaeton';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,6 /*Genre:rnb*/ FROM tune WHERE genre = 'rnb';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,12 /*Genre:rock*/ FROM tune WHERE genre = 'rock';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,83 /*Genre:Volkstümlich*/ FROM tune WHERE genre = 'Volkstümlich';
-- insert tune_tag entry for every tune's genre2
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,17 /*Genre:a cappella*/ FROM tune WHERE genre2 = 'a cappella';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,2 /*Genre:alternative*/ FROM tune WHERE genre2 = 'alternative';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,10 /*Genre:ballads*/ FROM tune WHERE genre2 = 'ballads';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,27 /*Genre:blues*/ FROM tune WHERE genre2 = 'blues';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,53 /*Genre:bluesrock*/ FROM tune WHERE genre2 = 'bluesrock';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,69 /*Genre:christian*/ FROM tune WHERE genre2 = 'christian';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,14 /*Genre:classic*/ FROM tune WHERE genre2 = 'classic';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,32 /*Genre:country*/ FROM tune WHERE genre2 = 'country';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,15 /*Genre:crossover*/ FROM tune WHERE genre2 = 'crossover';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,4 /*Genre:dance*/ FROM tune WHERE genre2 = 'dance';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,37 /*Genre:dancehall*/ FROM tune WHERE genre2 = 'dancehall';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,16 /*Genre:electro*/ FROM tune WHERE genre2 = 'electro';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,13 /*Genre:funk*/ FROM tune WHERE genre2 = 'funk';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,24 /*Genre:garage*/ FROM tune WHERE genre2 = 'garage';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,29 /*Genre:gothic*/ FROM tune WHERE genre2 = 'gothic';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,1 /*Genre:grunge*/ FROM tune WHERE genre2 = 'grunge';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,31 /*Genre:hardrock*/ FROM tune WHERE genre2 = 'hardrock';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,41 /*Genre:indie*/ FROM tune WHERE genre2 = 'indie';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,36 /*Genre:industrial*/ FROM tune WHERE genre2 = 'industrial';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,28 /*Genre:instrumental*/ FROM tune WHERE genre2 = 'instrumental';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,61 /*Genre:latin*/ FROM tune WHERE genre2 = 'latin';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,22 /*Genre:metal*/ FROM tune WHERE genre2 = 'metal';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,38 /*Genre:motown*/ FROM tune WHERE genre2 = 'motown';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,86 /*Genre:musical*/ FROM tune WHERE genre2 = 'musical';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,21 /*Genre:neo soul*/ FROM tune WHERE genre2 = 'neo soul';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,30 /*Genre:OST*/ FROM tune WHERE genre2 = 'ost';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,9 /*Genre:oldies*/ FROM tune WHERE genre2 = 'oldies';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,19 /*Genre:poprock*/ FROM tune WHERE genre2 = 'poprock';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,63 /*Genre:post-grunge*/ FROM tune WHERE genre2 = 'post-grunge';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,49 /*Genre:post-hardcore*/ FROM tune WHERE genre2 = 'post-hardcore';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,82 /*Genre:post-punk*/ FROM tune WHERE genre2 = 'post-punk';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,23 /*Genre:progressive*/ FROM tune WHERE genre2 = 'progressive';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,18 /*Genre:punk*/ FROM tune WHERE genre2 = 'punk';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,25 /*Genre:reggaeton*/ FROM tune WHERE genre2 = 'reggaeton';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,6 /*Genre:rnb*/ FROM tune WHERE genre2 = 'rnb';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,43 /*Genre:schlager*/ FROM tune WHERE genre2 = 'schlager';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,34 /*Genre:singer-songwriter*/ FROM tune WHERE genre2 = 'Singer-songwriter';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,39 /*Genre:ska*/ FROM tune WHERE genre2 = 'ska';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,33 /*Genre:soca*/ FROM tune WHERE genre2 = 'soca';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,26 /*Genre:soul*/ FROM tune WHERE genre2 = 'soul';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,47 /*Genre:southern*/ FROM tune WHERE genre2 = 'southern';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,67 /*Genre:synth-rock*/ FROM tune WHERE genre2 = 'Synth-rock';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,44 /*Genre:trip-hop*/ FROM tune WHERE genre2 = 'trip-hop';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,60 /*Genre:virtuoso*/ FROM tune WHERE genre2 = 'virtuoso';
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,45 /*Genre:xmas*/ FROM tune WHERE genre2 = 'xmas';
-- set default year
update tune set year2 = 0 where year = 1900;
-- set full-album as tag
insert into tag(id,value,color) values(96,'Album','#333333');
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,96 FROM tune WHERE album != 'unknown' and album is not null;
-- insert decade tags
insert into tag(id,value,color) values(97,'60s','#333333');
insert into tag(id,value,color) values(98,'70s','#333333');
insert into tag(id,value,color) values(99,'80s','#333333');
insert into tag(id,value,color) values(100,'90s','#333333');
insert into tag(id,value,color) values(101,'00s','#333333');
insert into tag(id,value,color) values(102,'10s','#333333');
insert into tag(id,value,color) values(103,'20s','#333333');
-- insert tags for every tune's year
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,97 FROM tune WHERE year2 > 1959 and year2 < 1970;
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,98 FROM tune WHERE year2 > 1969 and year2 < 1980;
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,99 FROM tune WHERE year2 > 1979 and year2 < 1990;
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,100 FROM tune WHERE year2 > 1989 and year2 < 2000;
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,101 FROM tune WHERE year2 > 1999 and year2 < 2010;
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,102 FROM tune WHERE year2 > 2009 and year2 < 2020;
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,103 FROM tune WHERE year2 > 2019 and year2 < 2030;
-- insert Exhib tag
insert into tag(id,value,color) values(104,'Exhib','#333333');
-- insert Exhib tag to every tune in EXHIBITION
INSERT INTO tune_tag (tune_tid,tag_id) SELECT tid,104 FROM tune WHERE path like '%EXHIBITION%';
-- replace file separator
UPDATE tune set path = REPLACE(path, '/', '\\');

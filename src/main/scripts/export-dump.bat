@echo off

set DBUSER=
set DBPASSWORD=
set FILE="%userprofile%\Documents\backup\%date:~-4%-%date:~3,2%-%date:~0,2%.mp3.sql"

mysqldump -u %DBUSER% --password=%DBPASSWORD% mp3 >> %FILE%
echo dumped to file %FILE%
pause
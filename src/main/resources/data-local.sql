-- noinspection SqlNoDataSourceInspectionForFile

INSERT INTO tune (tid, artist, album, title, year2, path, filename2, duration, added, updated) values (101, '3DG', 'Explosions', 'Explosions', 2022,'3DG\Explosions', '01.mp3', 19, '2023-03-22','2023-03-22');
INSERT INTO tune (tid, artist, album, title, year2, path, filename2, duration, added, updated) values (102, '3DG', 'Explosions', 'High Road', 2022,'3DG\Explosions', '02.mp3', 16, '2023-03-22', '2023-03-22');
INSERT INTO tune (tid, artist, album, title, year2, path, filename2, duration, added, updated) values (103, '3DG', 'Human', 'Tell me why', 2022,'3DG\Human', '04.mp3', 192, '2020-01-01', '2023-03-22');
INSERT INTO tune (tid, artist, album, title, year2, path, filename2, duration, added, updated) values (104, 'Normandie', 'Inguz', 'Awakening', 2022,'Normandie\Inguz', '07.mp3', 101, '2023-03-22', '2023-03-22');

INSERT INTO chart (cid, tid, label, played) values (101, 101, 'Explosions', '2023-01-01');
INSERT INTO chart (cid, tid, label, played) values (102, 102, 'High Road', '2023-01-01');
INSERT INTO chart (cid, tid, label, played) values (103, 101, 'Explosions', '2023-02-01');
INSERT INTO chart (cid, tid, label, played) values (104, 104, 'Awakening', '2023-03-01');

INSERT INTO tag (id, value, color, visible, selected, import_folder_default, import_file_default, enqueue_blocked, show_in_player) values(101, 'Genre:rock', '#7a3802', false, 10, true, true, false, false);
INSERT INTO tag (id, value, color, visible, selected, import_folder_default, import_file_default, enqueue_blocked, show_in_player) values(102, 'Genre:alternative', '#7a3802', false, 5, false, false, false, false);
INSERT INTO tag (id, value, color, visible, selected, import_folder_default, import_file_default, enqueue_blocked, show_in_player) values(103, 'Genre:post-hardcore', '#7a3802', false, 4, false, false, false, false);
INSERT INTO tag (id, value, color, visible, selected, import_folder_default, import_file_default, enqueue_blocked, show_in_player) values(104, 'top', 'cornflowerblue', true, 1, false, false, false, false);
INSERT INTO tag (id, value, color, visible, selected, import_folder_default, import_file_default, enqueue_blocked, show_in_player) values(105, 'Tuning:EADGHe', '#e3af05', true, 0, false, false, false, false);
INSERT INTO tag (id, value, color, visible, selected, import_folder_default, import_file_default, enqueue_blocked, show_in_player, icon) values(106, 'Like', 'green', true, 12, false, false, false, true, FILE_READ('src/main/resources/img/thumb-up-custom.png'));
INSERT INTO tag (id, value, color, visible, selected, import_folder_default, import_file_default, enqueue_blocked, show_in_player) values(107, 'Dislike', 'red', true, 6, false, false, true, false);
INSERT INTO tag (id, value, color, visible, selected, import_folder_default, import_file_default, enqueue_blocked, show_in_player) values(108, 'Lang:en', '#333333', false, 2, true, true, false, false);
INSERT INTO tag (id, value, color, visible, selected, import_folder_default, import_file_default, enqueue_blocked, show_in_player) values(109, '10s', '#333333', false, 1, false, false, false, false);

INSERT INTO tune_tag (tune_tid,tag_id) values(101,101);
INSERT INTO tune_tag (tune_tid,tag_id) values(101,102);
INSERT INTO tune_tag (tune_tid,tag_id) values(101,104);
INSERT INTO tune_tag (tune_tid,tag_id) values(102,101);
INSERT INTO tune_tag (tune_tid,tag_id) values(102,102);
INSERT INTO tune_tag (tune_tid,tag_id) values(103,101);
INSERT INTO tune_tag (tune_tid,tag_id) values(103,102);
INSERT INTO tune_tag (tune_tid,tag_id) values(103,106);
INSERT INTO tune_tag (tune_tid,tag_id) values(104,101);
INSERT INTO tune_tag (tune_tid,tag_id) values(104,103);
INSERT INTO tune_tag (tune_tid,tag_id) values(104,109);

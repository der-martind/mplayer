package de.cbb.mplayer.bowl.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.google.common.eventbus.EventBus;
import de.cbb.mplayer.session.events.EnqueueTunesEvent;
import de.cbb.mplayer.session.service.EnqueueingService;
import de.cbb.mplayer.tags.domain.Tag;
import de.cbb.mplayer.tunes.domain.Tune;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class) // for JUnit 5
public class EnqueueingServiceTest {

    @InjectMocks
    private EnqueueingService enqueueingService;

    @Mock
    private EventBus eventBus;

    @Captor
    private ArgumentCaptor<EnqueueTunesEvent> eventCaptor;

    @BeforeEach
    public void setup() {}

    @Test
    public void testEnqueue_WithSingleValidTune() {
        final Tag tag1 = new Tag();
        tag1.setValue("Dummy1");
        final Tag tag2 = new Tag();
        tag2.setValue("Dummy2");
        final Tag tag3 = new Tag();
        tag3.setValue("Dislike");
        tag3.setEnqueueBlocked(true);

        final List<Tune> tunes = new ArrayList<>();
        final Tune validTune = new Tune();
        validTune.setTitle("valid");
        validTune.getTags().add(tag1);
        validTune.getTags().add(tag2);
        tunes.add(validTune);

        final Tune invalidTune = new Tune();
        invalidTune.setTitle("invalid");
        invalidTune.getTags().add(tag2);
        invalidTune.getTags().add(tag3);
        tunes.add(invalidTune);

        enqueueingService.enqueue(tunes);
        Mockito.verify(eventBus).post(eventCaptor.capture());
        final EnqueueTunesEvent event = eventCaptor.getValue();
        assertEquals(1, event.getTunes().size());
        assertEquals(validTune, event.getTunes().get(0));
    }
}

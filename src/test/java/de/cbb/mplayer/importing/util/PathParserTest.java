package de.cbb.mplayer.importing.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import de.cbb.mplayer.config.ConfigProperties;
import de.cbb.mplayer.config.ImportConfigProperties;
import de.cbb.mplayer.importing.mapping.ImportFileInfo;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class) // for JUnit 5
public class PathParserTest {

    List<String> pathFormats = Arrays.asList(
            // File
            "^(?<artist1>[^\\?*]*)\\\\(?<artist2>[^\\?*-]*)\\s*-\\s*(?<album>[^\\?*]*)\\\\(?<artist3>[^\\?*-]*)\\s*-\\s*(?<title>[^\\?*.]*).\\S*$",
            "^(?<artist1>[^\\?*]*)\\\\(?<artist2>[^\\?*-]*)\\s*-\\s*(?<album>[^\\?*]*)\\\\(?<title>[^\\?*.]*).\\S*$",
            "^(?<artist1>[^\\?*]*)\\\\(?<album>[^\\?*]*-)\\\\(?<artist3>[^\\?*]*)\\s*-\\s*(?<title>[^\\?*.]*).\\S*$",
            "^(?<artist1>[^\\?*]*)\\\\(?<album>[^\\?*]*-)\\\\(?<title>[^\\?*.]*).\\S*$",
            // Folder
            "^(?<artist1>[^\\?*]*)\\\\(?<artist2>[^\\?*-]*)\\s*-\\s*(?<album>[^\\?*]*)$",
            "^(?<artist1>[^\\?*]*)\\\\(?<album>[^\\?*]*)$",
            "^(?<artist1>[^\\?*]*)$");
    private PathParser pathParser;

    @Mock
    private ConfigProperties configProperties;

    @Mock
    private ImportConfigProperties importConfigProperties;

    @BeforeEach
    public void setup() {
        when(configProperties.getMediaRoot()).thenReturn("mp3");
        when(configProperties.getImporting()).thenReturn(importConfigProperties);
        when(importConfigProperties.getPathFormats()).thenReturn(pathFormats);
        pathParser = new PathParser(configProperties);
    }

    @Test
    public void testGetArtist_WithArtistNoAlbum() {
        testFolder("mp3\\MyArtist", "MyArtist", null);
    }

    @Test
    public void testGetArtist_WithArtistSlashAlbum() {
        testFolder("mp3\\MyArtist\\MyAlbum", "MyArtist", "MyAlbum");
    }

    @Test
    public void testGetArtist_WithArtistSlashArtistAlbum() {
        testFolder("mp3\\MyArtist\\MyArtist - MyAlbum", "MyArtist", "MyAlbum");
    }

    @Test
    public void testGetAlbum_WithArtistSlashAlbum() {
        testFolder("mp3\\MyArtist\\MyAlbum", "MyArtist", "MyAlbum");
    }

    @Test
    public void testGetAlbum_WithArtistSlashArtistAlbum() {
        testFolder("mp3\\MyArtist\\MyArtist - MyAlbum", "MyArtist", "MyAlbum");
    }

    @Test
    public void testGetAlbum_WithArtistNoAlbum() {
        testFolder("mp3\\MyArtist", "MyArtist", null);
    }

    @Test
    public void testGetArtist_WithArtistTitleFiletype() {
        testFile("mp3\\MyArtist\\MyArtist - MyAlbum\\MyArtist - Track01.mp3", "Track01");
    }

    @Test
    public void testGetArtist_WithPathTitleFiletype() {
        testFile("mp3\\MyArtist\\MyArtist - MyAlbum\\Track01.mp3", "Track01");
    }

    @Test
    public void testGetTitle_WithPathArtistTitleFiletype() {
        testFile("mp3\\MyArtist\\MyArtist - MyAlbum\\MyArtist - Track01.mp3", "Track01");
    }

    @Test
    public void testGetTitle_WithPathTitleFiletype() {
        testFile("mp3\\MyArtist\\MyArtist - MyAlbum\\Track01.mp3", "Track01");
    }

    @Test
    public void testGetTitle_WithPathArtistTitleExtensionFiletype() {
        testFile(
                "mp3\\MyArtist\\MyArtist - MyAlbum\\Trivium - The Shadow Of The Abattoir - OFFICIAL" + " AUDIO.mp3",
                "The Shadow Of The Abattoir - OFFICIAL AUDIO");
    }

    private void testFolder(final String path, final String expectedArtist, final String expectedAlbum) {
        final File file = Mockito.mock(File.class);
        when(file.getAbsolutePath()).thenReturn(path);
        final ImportFileInfo info = pathParser.parse(file);
        assertEquals(expectedArtist, info.getArtist());
        assertEquals(expectedAlbum, info.getAlbum());
    }

    private void testFile(final String path, final String expectedTitle) {
        final File file = Mockito.mock(File.class);
        when(file.getAbsolutePath()).thenReturn(path);
        final ImportFileInfo info = pathParser.parse(file);
        assertEquals(expectedTitle, info.getTitle());
    }
}
